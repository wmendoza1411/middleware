/*
 * -This class has the mediation funtion betwen the app movril request and the ProDoctivity Services (Rest)  (Back End).
 * -Instance the modal class "GetServiceDescription" and 
 *  make the Serialization for the Json component front ProDoctivity.
 * -Receive as parameter the pointer  for the clase Configurator whit the general values  for  the 
 * Middleware.
 */
package NVS_ClientBackEnd;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import sun.misc.BASE64Encoder;
//
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
//
// Serializacion / DesSerializacion Json Component
import com.google.gson.Gson;


// Instance the just Back End modal clase
import NVS_ModalBackEnd.GetInfo.PDmGetServiceInfo;



// Configurator class whit all parameter and condition for the middleware  services
import NVS_Configurator.PDM_Configurator;
import java.io.IOException;
import java.io.OutputStream;

       
/**
 *
 * @author Novatec wmendoza
 */
public class CRN_GetInfo {
    
    // Clase para el manejo del servicio de ServiceDescription
    // expuesto por Prodoctivity
    // Por ser un servicio que no es solicitado explicitamente desde la 
    // aplicacion movil los datos que provengan del Back End
    // se matendran en la clase modal
    
    PDM_Configurator    pdm_configurator;
    Gson                gson;
    PDmGetServiceInfo   pdmGetServiceInfo;
     
    // Constructor Class
    public void PDM_GetServiceDescription(){     }
      
    // Acces the services in the Back End
    public boolean getService(PDM_Configurator pdm_configurator) throws SecurityException, IOException{
        // This code shoult be change
        // The correct code should cosume the prodoctivity services
    
        try {
            
            URL url= new URL(pdm_configurator.getPdm_paramatermodal().
                                                        getUrl()+"service/info");//+info
            BASE64Encoder enc= new sun.misc.BASE64Encoder();
 
            String userCredentials = 
                    pdm_configurator.getPdm_paramatermodal().
                            getAutenticationuser() + ":" +
                                    pdm_configurator.getPdm_paramatermodal().
                                                        getAutenticationkey();
            
            String encodedAuthorization = enc.encode(userCredentials.getBytes());

            String basicAuth = "Basic " + encodedAuthorization; 
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty ("Authorization", basicAuth);
            
            // Get Configurator Values
            conn.setRequestProperty(pdm_configurator.getPdm_paramatermodal().
                    getBackendpar01value(), pdm_configurator.getPdm_paramatermodal()
                            .getBackendpar02value());
            
            // Get Protertys Values
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            conn.setRequestProperty("Accept", "application/json");

            // se instancian desde el configurador las clases para Serializar y 
            // de los Json.
            Gson gson                   = pdm_configurator.getGson();  

            conn.setUseCaches(false);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            
            // Exceute the Services.....
            if (conn.getResponseCode() != 200) {
                pdm_configurator.getPdm_login().setBl_statusbackend(false);
                return false;
            }
            //
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String valor="";
            String output="";
            
            while ((output = br.readLine()) != null) {
                    valor=output;}

            // Get the Json
            conn.disconnect();
            pdmGetServiceInfo = gson.fromJson(valor,PDmGetServiceInfo.class);
            pdm_configurator.setPdmmgetserviceinfo(pdmGetServiceInfo);
            
            return true;

        } catch (Exception e) {
             // Se Registra el error.
           try{
               pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
               pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
               pdm_configurator.DriverError();
            
           }catch(SecurityException | IOException e_){}
            this.pdmGetServiceInfo.setActive(false);
            this.pdm_configurator.setBl_onlineProd(false);  // prodoctivity is Off Line
            return false;
        }
    }

    public PDmGetServiceInfo getPdmGetServiceInfo() {
        return pdmGetServiceInfo;
    }

    public void setPdmGetServiceInfo(PDmGetServiceInfo pdmGetServiceInfo) {
        this.pdmGetServiceInfo = pdmGetServiceInfo;
    }
    
}
