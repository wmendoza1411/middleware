/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ClientBackEnd;

import NVS_Configurator.PDM_Configurator;
import NVS_ModalBackEnd.DocHandleNovosit.ResponseGetDocument;
import NVS_ModalBackEnd.GetInfo.PDmGetServiceInfo;
import NVS_ModalBackEnd.searchdocumentsrequestNovosit.Searchdocumentsrequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import sun.misc.BASE64Encoder;

/**
 *
 * @author wmendoza
 */
public class CRN_Searchdocumentsrequest {
   
     public boolean getSearchdocumentsrequest(PDM_Configurator pdm_configurator) throws SecurityException, IOException{
        // This code shoult be change
        // The correct code should cosume the prodoctivity services
      //  System.out.print("*****************bien1="+pdm_configurator.getPdm_paramatermodal().
                                                           // getUrl()+"info");
        try {
           
           
            
           pdm_configurator.getPdm_paramatermodal().
                   setUrl("http://34.228.64.13:80/CoordinatorFront/"
                           + "api/v1/prodoctivityapi/searchdocumentsrequest?"
                           + "searchQuery=1&inPublishedDocuments=true&inWorkflowDocuments="
                           + "false&inIndexedKeys=true&inNotes=false&rowCount=50");
          
            URL url= new URL(pdm_configurator.getPdm_paramatermodal().
                                                            getUrl());//+info
            BASE64Encoder enc= new sun.misc.BASE64Encoder();

            String userCredentials = 
                    pdm_configurator.getPdm_paramatermodal().
                            getAutenticationuser() + ":" +
                                    pdm_configurator.getPdm_paramatermodal().getAutenticationkey();
            
            String encodedAuthorization = enc.encode(userCredentials.getBytes());

            String basicAuth = "Basic " + encodedAuthorization; 
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty ("Authorization", basicAuth);
            
            // Get Configurator Values
            conn.setRequestProperty(pdm_configurator.getPdm_paramatermodal().
                    getBackendpar01value(), pdm_configurator.getPdm_paramatermodal()
                            .getBackendpar02value());
            
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            conn.setRequestProperty("Accept", "application/json");

            // se instancian desde el configurador las clases para Serializar y 
            // de los Json.
            Gson gson                   = pdm_configurator.getGson();
            GsonBuilder gsonBuilder     = pdm_configurator.getGsonBuilder();
            
     
            conn.setUseCaches(false);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            OutputStream os = conn.getOutputStream();
            os.write((gson.toJson(pdm_configurator.getSearchdocumentsrequest())).getBytes());
            os.flush();


            if (conn.getResponseCode() != 200) {
                pdm_configurator.getPdm_login().setBl_statusbackend(false);
               return false;
                }

            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String valor="";
            String output="";
            
            while ((output = br.readLine()) != null) {
                    valor=output;}

            conn.disconnect();
           Searchdocumentsrequest searchdocumentsrequest = gson.fromJson(valor,Searchdocumentsrequest.class);
           
            
           
            
            return true;

        } catch (Exception e) {
             // Se Registra el error.
           try{
               pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
               pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
               pdm_configurator.DriverError();
            
           }catch(SecurityException | IOException e_){}
        
            return false;
        }

    }
}
