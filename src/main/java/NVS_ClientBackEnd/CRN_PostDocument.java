/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ClientBackEnd;

import NVS_Configurator.PDM_Configurator;
import NVS_Error.PDM_HandlerError;
import NVS_ModalBackEnd.PostDocument.Data;
import NVS_ModalBackEnd.PostDocument.Keyword;
import NVS_ModalBackEnd.PostDocument.Record;
import NVS_ModalBackEnd.PostDocumentNovosit.PDM_PostDocumentRequest;
import NVS_ModalBackEnd.ProfileNovosit.PDmGetUserProfile;
import NVS_ModalFrontEnd.Autentication.PDM_Keyword;
import NVS_ModalFrontEnd.Document.PDM_Document;
import NVS_ModalFrontEnd.DocumentNovosit.PDM_KeywordData_Request;
import NVS_ModalFrontEnd.DocumentNovosit.PDM_Documents_Request;

//
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import javax.ws.rs.core.Response;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import sun.misc.BASE64Encoder;


/**
 *
 * @author wmendoza
 */
public class CRN_PostDocument {
    
    PDM_Configurator        pdm_configurator;
    PDM_Document            pdm_document;
    PDM_PostDocumentRequest pdm_postdocumentrequest;
    
    public CRN_PostDocument(){
    
    }
    
   
   
    public boolean SendDocumentProfile(PDM_Configurator pdm_configurator) throws SecurityException, IOException {
    
        // Get the Connfigurator Pointer 
        this.pdm_configurator    =   pdm_configurator;
        
        // Get Instance Modal pdm_document
        this.pdm_document        =   pdm_configurator.getPdm_document();
        
        try{
            
            pdm_configurator.getPdm_document().setBl_statusbackend(true);    
            URL url= new URL(pdm_configurator.getPdm_paramatermodal().getUrl()+"documents");
            BASE64Encoder enc= new sun.misc.BASE64Encoder();

            String userCredentials = 
                    pdm_configurator.getPdm_paramatermodal().
                            getAutenticationuser() + ":" +
                                    pdm_configurator.getPdm_paramatermodal().getAutenticationkey();
            userCredentials ="manager@mobile:"+pdm_configurator.getPdm_paramatermodal().getAutenticationkey();
            String encodedAuthorization = enc.encode(userCredentials.getBytes());

            String basicAuth = "Basic " + encodedAuthorization; 
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty ("Authorization", basicAuth);
            
            // Get Configurator Values
            conn.setRequestProperty(pdm_configurator.getPdm_paramatermodal().
                    getBackendpar01value(), pdm_configurator.getPdm_paramatermodal()
                            .getBackendpar02value());
            
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            conn.setRequestProperty("Accept", "application/json");

            // se instancian desde el configurador las clases para Serializar y 
            // de los Json.
            Gson gson                   = pdm_configurator.getGson();
            GsonBuilder gsonBuilder     = pdm_configurator.getGsonBuilder();
            
            // Get Pointer PDM_Document_Request from Configurator
            PDM_Documents_Request pdm_document_request = 
            pdm_configurator.getPdm_document_request();


            conn.setUseCaches(false);
            conn.setDoInput(true);
            conn.setDoOutput(true);

            OutputStream os = conn.getOutputStream();
          
            os.write((gson.toJson(pdm_configurator.getPdm_document_request())).getBytes());//obja
            os.flush();
 
            if (conn.getResponseCode() != 200) {
                pdm_configurator.getPdm_document().setBl_statusbackend(true);
                return false;
                
                }

            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String valor="";
            String output="";
            
            while ((output = br.readLine()) != null) {valor=output;
            
            }

            conn.disconnect();
            pdm_postdocumentrequest = gson.fromJson(valor,PDM_PostDocumentRequest.class);
                
        }
        
        catch (Exception e) { 
            // Register Error
            try{
               pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
               pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
               pdm_configurator.DriverError();
            
           }catch(SecurityException | IOException e_){}
            return false;
        }
        
        // Set to Configurator the modal pdm_postdocumentrequest
        pdm_configurator.setPdm_postdocumentrequest(pdm_postdocumentrequest);
        return true;   
    }
    
}
