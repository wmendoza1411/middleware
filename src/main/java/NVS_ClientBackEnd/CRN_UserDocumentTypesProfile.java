/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ClientBackEnd;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import sun.misc.BASE64Encoder;

import NVS_ModalFrontEnd.Autentication.PDM_Login;
import NVS_ModalBackEnd.Profile.PDmGetUserProfile;

// Serializacion / DesSerializacion Json Component
import com.google.gson.Gson;

// Configurator class whit all parameter and condition for the middleware  services
import NVS_Configurator.PDM_Configurator;

import NVS_ModalBackEnd.GetInfo.PDmGetServiceInfo;
import NVS_ModalBackEnd.LoginNovosit.Users;
import NVS_ModalBackEnd.keywordNovosit.UserDocumentType;

import NVS_ModalFrontEnd.Autentication.PDM_Handler;
import NVS_ModalFrontEnd.Autentication.PDM_Tipo;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;

/**
 *
 * @author cesar3r2
 */
public class CRN_UserDocumentTypesProfile {
    
    
    PDM_Configurator pdm_configurator;
    Gson gson;
    UserDocumentType userdocumenttype;
   
    public CRN_UserDocumentTypesProfile() { }
      
    //Consume Back End Profile Back End Services 
    public boolean getUserDocumentTypes(PDM_Configurator  pdm_configurator) throws SecurityException, IOException {
        
       try {
                  
            URL url= new URL("http://34.228.64.13/Site/Admin/AdminServiceJson."
                    + "svc/userDocumentTypes");
            
            BASE64Encoder enc= new sun.misc.BASE64Encoder();
            pdm_configurator.getPdm_paramatermodal().setAutenticationuser("manager@mobile");//falta agregarlo al configurador 

         
            String userCredentials = pdm_configurator.getPdm_paramatermodal().getAutenticationuser() + ":" +pdm_configurator.getPdm_paramatermodal().getAutenticationkey();
         
            String encodedAuthorization = enc.encode(userCredentials.getBytes());
 
            String basicAuth = "Basic " + encodedAuthorization; 
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty ("Authorization", basicAuth);
           
            conn.setRequestProperty(pdm_configurator.getPdm_paramatermodal().
                   getBackendpar01value(), pdm_configurator.getPdm_paramatermodal()
                          .getBackendpar02value());
            
            conn.setConnectTimeout(1000);
            conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");//este
            conn.setRequestProperty("Accept", "application/json");
   
            conn.setRequestProperty("Cookie", pdm_configurator.getCookies());
          
            // se instancian desde el configurador las clases para Serializar y 
            // de los Json.
            Gson gson= pdm_configurator.getGson();

            conn.setRequestProperty("Connection", "keep-alive");
            conn.setDoOutput(true);
            conn.setDoInput(true);
           

            if (conn.getResponseCode() != 200) {
                pdm_configurator.getPdm_login().setBl_statusbackend(false);
               return false;
            }
  
           InputStreamReader in = new InputStreamReader(conn.getInputStream(),"UTF-8");
            BufferedReader br = new BufferedReader(in);
            String valor="";
            String output="";
         
            while ((output = br.readLine()) != null) {valor=output;}
           
              List<UserDocumentType> userdocumenttypelist = gson.fromJson(valor, new TypeToken<List<UserDocumentType>>(){}.getType());
              pdm_configurator.setUserdocumenttype(userdocumenttypelist);
             
             return true;

        } catch (Exception e) {
             // Se Registra el error.
            try{
               pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
               pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
               pdm_configurator.DriverError();
            
           }catch(SecurityException | IOException e_){}
            return false;
        }
    }
    
}