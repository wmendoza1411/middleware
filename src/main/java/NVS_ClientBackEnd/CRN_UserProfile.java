/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ClientBackEnd;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import sun.misc.BASE64Encoder;

import NVS_ModalFrontEnd.Autentication.PDM_Login;
import NVS_ModalBackEnd.Profile.PDmGetUserProfile;

// Serializacion / DesSerializacion Json Component
import com.google.gson.Gson;

// Configurator class whit all parameter and condition for the middleware  services
import NVS_Configurator.PDM_Configurator;

import NVS_ModalBackEnd.GetInfo.PDmGetServiceInfo;
import NVS_ModalBackEnd.LoginNovosit.Users;

import NVS_ModalFrontEnd.Autentication.PDM_Handler;
import NVS_ModalFrontEnd.Autentication.PDM_Tipo;
import java.io.IOException;

import java.util.ArrayList;

/**
 *
 * @author cesar3r2
 */
public class CRN_UserProfile {
    
    
    PDM_Configurator pdm_configurator;
    Gson gson;
    Users users;
    
    PDmGetUserProfile pdmgetUseprofile;
    
    String st_mensaje           = "";
    String st_internal_error    = "";
    String st_user              = "";
    String st_key               = "";
    String st_dns               = "";
    String output               = "";
    
    PDM_Login pdm_login;
    
    public CRN_UserProfile() { }
    
    
    // Consume Back End Profile Back End Services 
    public boolean getUserProfile(PDM_Configurator  pdm_configurator) throws SecurityException, IOException {
        
       try {
         
              pdm_configurator.getPdm_login().setBl_statusbackend(true);
              
           
            URL url= new URL("http://34.228.64.13:80/Site/api/users/"+pdm_configurator.getPdm_login().getUsuario());
            BASE64Encoder enc= new sun.misc.BASE64Encoder();
            
           // pdm_configurator.getPdm_paramatermodal().setAutenticationuser(pdm_configurator.getPdm_paramatermodal().
               //             getAutenticationuser()+"@mobile");//falta agregarlo al 
            pdm_configurator.getPdm_paramatermodal().setAutenticationuser("manager@mobile");
         
           /* String userCredentials = 
                    pdm_configurator.getPdm_paramatermodal().
                            getAutenticationuser() + ":" +
                                    pdm_configurator.getPdm_paramatermodal().getAutenticationkey();*/
             String userCredentials = "manager@mobile:"+pdm_configurator.getPdm_paramatermodal().getAutenticationkey();
                   
            String encodedAuthorization = enc.encode(userCredentials.getBytes());
 
            String basicAuth = "Basic " + encodedAuthorization; 
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty ("Authorization", basicAuth);
           
             conn.setRequestProperty(pdm_configurator.getPdm_paramatermodal().
                    getBackendpar01value(), pdm_configurator.getPdm_paramatermodal()
                            .getBackendpar02value());
             
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            conn.setRequestProperty("Accept", "application/json");
           
 
            // se instancian desde el configurador las clases para Serializar y 
            // de los Json.
            Gson gson                   = pdm_configurator.getGson();

            conn.setUseCaches(false);
            conn.setDoInput(true);
            conn.setDoOutput(true);
           

            if (conn.getResponseCode() != 200) {
               
                pdm_configurator.getPdm_login().setBl_statusbackend(false);
                return false;
                
            }
            ////////////////////////////////////cookies

            String key;
            String cookies="";
            for (int i = 1; (key = conn.getHeaderFieldKey(i)) != null; i++) {  
               if(key.equals("Set-Cookie")){
               cookies=cookies+conn.getHeaderField(i)+";";
               }
            }
            //////////////////////////////////
            pdm_configurator.setCookies(cookies);
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String valor="";
            String output="";
         
            while ((output = br.readLine()) != null) {valor=output;}
            conn.disconnect();
         
           if(valor.equals("null")){
              
               pdm_configurator.getPdm_login().setBl_statusbackend(false);
               return false;
           }
        
            users = gson.fromJson(valor,Users.class);
            pdm_configurator.setUser(users);
          
            return true;

        } catch (Exception e) {
             // Se Registra el error.
            try{
               pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
               pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
               pdm_configurator.DriverError();
            
           }catch(SecurityException | IOException e_){}
            return false;
        }
    }
    
    // Procesa el login que llego del back end y lo transforma al 
    // login modal requerido por el front end
    private boolean ProcLogin(String output){
        
        // Des Serializa hacia la clase modal el json  que llega en la peticion 
        // desde el Back End
        this.pdmgetUseprofile = gson.fromJson(output,PDmGetUserProfile.class);
        
        String st_nomber = this.pdmgetUseprofile.getName();
        
        pdm_login.setRespuesta("1");
        pdm_login.setPerfil(null);  // Este perfil lo entrega Back End
        pdm_login.setMensaje("Login Aceptado");
        pdm_login.setInternal_error("");
        pdm_login.setBl_statusbackend(true);
        
        return true;
        
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }
    

    public String getSt_mensaje() {
        return st_mensaje;
    }

    public void setSt_mensaje(String st_mensaje) {
        this.st_mensaje = st_mensaje;
    }

    public String getSt_internal_error() {
        return st_internal_error;
    }

    public void setSt_internal_error(String st_internal_error) {
        this.st_internal_error = st_internal_error;
    }

    public String getSt_user() {
        return st_user;
    }

    public void setSt_user(String st_user) {
        this.st_user = st_user;
    }

    public String getSt_key() {
        return st_key;
    }

    public void setSt_key(String st_key) {
        this.st_key = st_key;
    }

    public PDM_Login getPdm_login() {
        return pdm_login;
    }

    public void setPdm_login(PDM_Login pdm_login) {
        this.pdm_login = pdm_login;
    }

    public String getSt_dns() {
        return st_dns;
    }

    public void setSt_dns(String st_dns) {
        this.st_dns = st_dns;
    }
    
}