/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ClientBackEnd;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import sun.misc.BASE64Encoder;

import NVS_ModalFrontEnd.Autentication.PDM_Join;

import NVS_Configurator.PDM_Configurator;
import java.io.IOException;

/**
 *
 * @author cesar3r2
 */
public class CRN_UserJointNovosit {
    
    String st_mensaje = "";
    String st_internal_error = "";
    String st_user  = "";
    String st_key   = "";
    String st_dns   = "";
    
    PDM_Join            pdm_join;
    PDM_Configurator    pdm_configurator; 
    
    
    public boolean getUserProfile (int it_par,PDM_Configurator pdm_configurator){
        
        this.pdm_configurator = pdm_configurator;
        this.setPdm_join(pdm_configurator.getPdm_join());
        
        this.pdm_join.setgeneralerror();
        this.pdm_join.setBl_statusbackend(true);
        
        pdm_configurator.getPdm_handlererror().setOrigen("20");
        pdm_configurator.getPdm_handlererror().setSclass("20");
        pdm_configurator.getPdm_handlererror().setTip("20");
        pdm_configurator.getPdm_handlererror().setCod("");
        pdm_configurator.getPdm_handlererror().setUsermessage(" Bad Response Front Back End  ");
        return false;
    }
    
    public boolean getUserProfile() {
        try {
            URL url = new URL("http://localhost:8389/api/v1/prodoctivityapi/users/profile");
            BASE64Encoder enc = new sun.misc.BASE64Encoder();
            
            String userCredentials = "manager@ProDoctivity Mobile:AAbb11";
            String encodedAuthorization = enc.encode( userCredentials.getBytes() );
            
            String basicAuth = "Basic "+ encodedAuthorization;
            
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty ("Authorization", basicAuth);
            conn.setRequestProperty("X-API-KEY", "1234237");
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            conn.setRequestProperty("Accept", "application/json");
            conn.setUseCaches(false);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            
            System.out.println("\nSending 'GET' request to URL: " + url);

            if (conn.getResponseCode() != 200) {
                pdm_configurator.getPdm_login().setBl_statusbackend(false);
                return false;
            }
            System.out.println("Response Code: " + conn.getResponseCode() + ". " +conn.getResponseMessage());
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
            while ((output = br.readLine()) != null) {
                System.out.println(output);
            }
            conn.disconnect();
            return true;

        } catch (Exception e) {
            try{
               pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
               pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
               pdm_configurator.DriverError();
            
           }catch(SecurityException | IOException e_){}
            return false;
        }
    }

    public String getSt_mensaje() {
        return st_mensaje;
    }

    public void setSt_mensaje(String st_mensaje) {
        this.st_mensaje = st_mensaje;
    }

    public String getSt_internal_error() {
        return st_internal_error;
    }

    public void setSt_internal_error(String st_internal_error) {
        this.st_internal_error = st_internal_error;
    }

    public String getSt_user() {
        return st_user;
    }

    public void setSt_user(String st_user) {
        this.st_user = st_user;
    }

    public String getSt_key() {
        return st_key;
    }

    public void setSt_key(String st_key) {
        this.st_key = st_key;
    }

    public PDM_Join getPdm_join() {
        return pdm_join;
    }

    public void setPdm_join(PDM_Join pdm_join) {
        this.pdm_join = pdm_join;
    }

    public String getSt_dns() {
        return st_dns;
    }

    public void setSt_dns(String st_dns) {
        this.st_dns = st_dns;
    }
    
}
