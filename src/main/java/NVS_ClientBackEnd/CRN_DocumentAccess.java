/*
 * -This class has the mediation funtion betwen the app movril request and the ProDoctivity Services (Rest)  (Back End).
 * -Instance the modal class "GetServiceDescription" and 
 *  make the Serialization for the Json component front ProDoctivity.
 * -Receive as parameter the pointer  for the clase Configurator whit the general values  for  the 
 * Middleware.
 */
package NVS_ClientBackEnd;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import sun.misc.BASE64Encoder;
//
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
//
// Serializacion / DesSerializacion Json Component
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

// Instance the just Back End modal clase
import NVS_ModalBackEnd.GetInfo.PDmGetServiceInfo;

// Instance the just Front End modal class
import NVS_ModalFrontEnd.Info.PDM_Info;

// Configurator class whit all parameter and condition for the middleware  services
import NVS_Configurator.PDM_Configurator;
import NVS_ModalBackEnd.DocumentAccessNovosit.DocumentAccess;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

        

/**
 *
 * @author Novatec wmendoza
 */
public class CRN_DocumentAccess {
    
    // Clase para el manejo del servicio de ServiceDescription
    // expuesto por Prodoctivity
    // Por ser un servicio que no es solicitado explicitamente desde la 
    // aplicacion movil los datos que provengan del Back End
    // se matendran en la clase modal
    
    PDM_Configurator pdm_configurator;
    Gson gson;
    GsonBuilder gsonbuilder;
    PDmGetServiceInfo  pdmGetServiceInfo;
    PDM_Info    pdm_info;
  
     
    // Constructor Class
    public void PDM_GetServiceDescription(){     }
    
    public boolean getServiceRS(PDM_Configurator pdm_configurator){
        
        this.pdm_configurator       = pdm_configurator;
        // this.gson                   = this.pdm_configurator.getGson();
        this.pdm_info               = this.pdm_configurator.getPdm_info();
        
        pdmGetServiceInfo           = new PDmGetServiceInfo();
        
        
        // Update WML 06052018 
        this.pdm_info.setSt_version(this.pdm_configurator.getPdm_paramatermodal().getVersion());
        this.pdm_info.setBl_statusbackend(true);
        
        return true;
    }
    
    public boolean getService(PDM_Configurator pdm_configurator) throws SecurityException, IOException{
        // This code shoult be change
        // The correct code should cosume the prodoctivity services
      
        try {
            
            //fijo
           pdm_configurator.getPdm_paramatermodal().setUrl(pdm_configurator.getPdm_paramatermodal().
                                                           getUrl()+"users/document-access");
          
            URL url= new URL(pdm_configurator.getPdm_paramatermodal().
                                                            getUrl());//+info
            BASE64Encoder enc= new sun.misc.BASE64Encoder();

            String userCredentials = 
                    pdm_configurator.getPdm_paramatermodal().
                            getAutenticationuser() + ":" +
                                    pdm_configurator.getPdm_paramatermodal().getAutenticationkey();
            
            String encodedAuthorization = enc.encode(userCredentials.getBytes());

            String basicAuth = "Basic " + encodedAuthorization; 
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty ("Authorization", basicAuth);
            
            // Get Configurator Values
            conn.setRequestProperty(pdm_configurator.getPdm_paramatermodal().
                    getBackendpar01value(), pdm_configurator.getPdm_paramatermodal()
                            .getBackendpar02value());
            
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            conn.setRequestProperty("Accept", "application/json");

            // se instancian desde el configurador las clases para Serializar y 
            // de los Json.
            Gson gson                   = pdm_configurator.getGson();
            GsonBuilder gsonBuilder     = pdm_configurator.getGsonBuilder();
            
            conn.setUseCaches(false);
            conn.setDoInput(true);
            conn.setDoOutput(true);


            if (conn.getResponseCode() != 200) {
                pdm_configurator.getPdm_login().setBl_statusbackend(false);
               return false;
                }

            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String valor="";
            String output="";
            
            while ((output = br.readLine()) != null) {
                    valor=output;}

            conn.disconnect();
          
           ArrayList<DocumentAccess> documentaccess= (ArrayList<DocumentAccess>) gson.fromJson(valor,
                    new TypeToken<ArrayList<DocumentAccess>>() {
                    }.getType());
            pdm_configurator.setDocumentaccess(documentaccess);
            
            
            return true;

        } catch (Exception e) {
             // Se Registra el error.
           try{
               pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
               pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
               pdm_configurator.DriverError();
            
           }catch(SecurityException | IOException e_){}
            return false;
        }

    }

    // Analiza el Json de Back End y Genera Jason de Respuesta para el
    // Front End
    // Como hoy (03/16/2018) no se tiene claro la logica de operacion contra
    // los servicios del Back End esta clase este metodo estara vacio.
    // Hay que aclarar si con este servicio
    // '/api/v{version}/prodoctivityapi/service/info': se debe utilizar con
    // la interaccion del Front End
    private void ProdService (){

    }

    public PDmGetServiceInfo getPdmGetServiceInfo() {
        return pdmGetServiceInfo;
    }

    public void setPdmGetServiceInfo(PDmGetServiceInfo pdmGetServiceInfo) {
        this.pdmGetServiceInfo = pdmGetServiceInfo;
    }
    
    
    
}
