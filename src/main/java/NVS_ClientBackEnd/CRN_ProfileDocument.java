/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ClientBackEnd;

import NVS_Configurator.PDM_Configurator;
import NVS_ModalBackEnd.DocHandleNovosit.ResponseGetDocument;
import NVS_ModalBackEnd.GetInfo.PDmGetServiceInfo;
import NVS_ModalBackEnd.ProfileDocument.ProfileDocument;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import sun.misc.BASE64Encoder;

/**
 *
 * @author wmendoza
 */
public class CRN_ProfileDocument {
   
     public boolean getProfileDocument(PDM_Configurator pdm_configurator){
        // This code shoult be change
        // The correct code should cosume the prodoctivity services
      
        try {
            
            System.out.print("***************** desde getProfileDocument");
          
           
             pdm_configurator.getPdm_login().setBl_statusbackend(true);
            
             URL url= new URL(pdm_configurator.getPdm_paramatermodal().
                                                            getUrl()+"users/profile?includeDocumentTypeAccessList=true");//+info
            BASE64Encoder enc= new sun.misc.BASE64Encoder();
 
            String userCredentials = 
                    pdm_configurator.getPdm_login().getUsuario() + ":" +
                                    pdm_configurator.getPdm_login().getClave();
            
            String encodedAuthorization = enc.encode(userCredentials.getBytes());
  
            String basicAuth = "Basic " + encodedAuthorization; 
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            
            conn.setRequestMethod("GET");
            conn.setRequestProperty ("Authorization", basicAuth);
           
            // Get Configurator Values
            conn.setRequestProperty(pdm_configurator.getPdm_paramatermodal().
                    getBackendpar01value(), pdm_configurator.getPdm_paramatermodal()
                            .getBackendpar02value());
            
            conn.setRequestProperty("Content-Type", "application/json");
           
 
            // se instancian desde el configurador las clases para Serializar y 
            // de los Json.
            Gson gson                   = pdm_configurator.getGson();
            GsonBuilder gsonBuilder     = pdm_configurator.getGsonBuilder();
           
   
            conn.setConnectTimeout(pdm_configurator.getIn_timeOut());
            conn.setUseCaches(false);
            conn.setDoInput(true);
            conn.setDoOutput(true);


            if (conn.getResponseCode() != 200) {
                
                pdm_configurator.getPdm_login().setBl_statusbackend(false);
                pdm_configurator.getPdm_error().setSt_error(conn.getResponseCode()+" "+conn.getResponseMessage());
                pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
                pdm_configurator.DriverError(); 
                return false;
                }

            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String valor="";
            String output="";
            
            while ((output = br.readLine()) != null) {valor=output;}

           conn.disconnect();
           ProfileDocument profiledocument = gson.fromJson(valor,ProfileDocument.class);
           pdm_configurator.setProfiledocument(profiledocument);
           
           
            return true;

        } catch (Exception e) {
             // Se Registra el error.
           // Register Error
           try{
               pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
               pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
               pdm_configurator.DriverError();
            
           }catch(SecurityException | IOException e_){}
         //   this.pdmGetServiceInfo.setActive(false);
          //  this.pdm_configurator.setBl_onlineProd(false);  // prodoctivity is Off Line
            return false;
        }

    }
}
