/*
 * Clase para la creacion y validacion del token JWT
 * Para los efectos la solicitud del token es con el nro. de telefono
 * Usurio y clave del usuario involucrado.
 * la clase devuelve el nuevo token, o una respuesta afirmativa si el token
 * pasa la verificación 03/10/2018
 
 */
package NVS_ModalFrontEnd.TokenDriver;

// 
import java.security.Key;
import io.jsonwebtoken.*;
import io.jsonwebtoken.impl.crypto.MacProvider;
import java.util.Date;

// 
import NVS_Configurator.PDM_Configurator;
import NVS_Error.PDM_HandlerError;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 *
 * @author Novatec
 */
public class PDM_JWT {
    
    String  st_token;
    boolean bl_statusbackend;
    String  st_idd;
    String  respuesta;
    private PDM_HandlerError handlererror;
    String mensaje;  
   
    
   
    // Constructor
    public void PDM_JWT() {}

    
    // Creacion de Token inicio de conversación y por cada peticion para proteger los tiempos 
    // de respuesta y darle tiempo al usuario en sus ejecuciones en el App Movil
    // La crecion del token involucra el usuario y la clave por que cada operacion
    // que se vaya a realizar contra el Back End requiere usuario y clave.
    public boolean CreateJWT(PDM_Configurator pdm_configurator_p,String par_llamada) throws SecurityException, IOException {
        
        // Genera un nuevo token con el Inicio de una conversacion Movil to Back End
        // se utilizaran  los valores generales del servicio que provee el configurador esto es:
        // Clave de encriptamiento general
        // Lapso de tiempo de vencimiento general
        // Configurador y Subject generales.
        // Nro. de telefono donde esta la app movil operando.
        PDM_Configurator pdm_configurator = pdm_configurator_p;
        try {
           
            long nowMillis = System.currentTimeMillis();
            String stInter   =  pdm_configurator.
                                getPdm_paramatermodal().getTimedurationtoken();
            long valend    =  Integer.valueOf(stInter);
            // Se indican los lapsos de tiempo de vencimiento de la transacción.
            
            Date now = new Date(nowMillis);
            Date exp = new Date(nowMillis + valend);
            
            // Se genera el Token
            String compactJws = Jwts.builder()
                                    .setId(pdm_configurator.getSt_id())
                                    .setSubject(pdm_configurator.getSt_subject())
                                    .setIssuedAt(now)
                                    .setExpiration(exp)
                                    .claim("llamada",par_llamada)
                                 //   .claim("user",pdm_configurator.getSt_login_user())
                                 //   .claim("key",pdm_configurator.getSt_login_key())
                                 //   .claim("userbe",pdm_configurator.getSt_login_user_be())
                                 //   .claim("keybe",pdm_configurator.getSt_login_key_be())
                                    .signWith(SignatureAlgorithm.HS256, pdm_configurator.getKey().
                                            getBytes(pdm_configurator.getPdm_paramatermodal().getEncripgetbyte())).
                                                compact();
            
            // Almacena el token generado y demas valores en propia clase con los metodos "set"
            setSt_token(compactJws);
            
            // Token Generado.
            return true;
        }
   
         catch (Exception e) {  // Error en la generacion del token
                                // Se le avisa al usuario 
            // Acciones para el registro de errores.
            pdm_configurator.getPdm_error().setSt_error(e.getMessage());
            pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
            pdm_configurator.DriverError();
            System.out.println("Error  " + e.getMessage());
            return false;
        }
    }
    
    // Metodo de Verificación del Token
    // por cada peticion que llega desde la App Movil, se verifica el token
    // y se comprueba su validez y vigencia.
    public boolean VerifyJWT(String compactJwt, 
                                        PDM_Configurator pdm_configurator_p) 
                                                throws UnsupportedEncodingException, SecurityException, IOException {
        // Configurador pasado como parametro
        PDM_Configurator pdm_configurator = pdm_configurator_p;
        
        try {
            Boolean response;
            
            // Valida sintacticamente el token que viene como parametro
            if (compactJwt == null || compactJwt.isEmpty()){
                
                return false;
            }
            String ls_byte = pdm_configurator.getPdm_paramatermodal().getEncripgetbyte();
            String ls_key  = pdm_configurator.getKey();
            // Se desencripta el Token
            Claims claims = Jwts.parser()         
                .setSigningKey(ls_key
                 .getBytes(ls_byte)) 
                       .parseClaimsJws(compactJwt) // Token Parametro
                         .getBody();
            
            // Se hacen las validaciones lógicas correpondientes
           
            String st_compare = (String)claims.getId();
            if (!(st_compare.equals(pdm_configurator.getSt_id()))){// ID
                    return false;}
             
            
            st_compare = (String)claims.getSubject();
            if (!(st_compare.equals(pdm_configurator.getSt_subject()))){// SUBJECT
                    return false;}
            
            // Get the all keys and user
            st_compare = (String)claims.get("key");
            if ((st_compare != "") && (st_compare != null)) {
                pdm_configurator.setSt_login_key(st_compare);}
            else{
                pdm_configurator.setSt_login_key("");
            }
            
            st_compare = (String)claims.get("user");
            if ((st_compare != "") && (st_compare != null)) {
                pdm_configurator.setSt_login_user(st_compare);}
            else{
                pdm_configurator.setSt_login_key("");
            }
                
            st_compare = (String)claims.get("keybe");
            if ((st_compare != "") && (st_compare != null)) {
                pdm_configurator.setSt_login_key_be(st_compare);}
            else{
                pdm_configurator.setSt_login_key_be("");
            }
                
            st_compare = (String)claims.get("userbe");
            if ((st_compare != "") && (st_compare != null)) {
                pdm_configurator.setSt_login_user_be(st_compare);}
            else{
                pdm_configurator.setSt_login_user_be("");
            }
                  
            return true; 
        } catch (SignatureException e) {
            // Se captura y controla la invalides del token
            
            // Acciones para el registro de errores.
            pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
            pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
            pdm_configurator.DriverError();
            
            return false;
        }
        catch (Exception e) {
            
            // Acciones para el registro de errores.
            pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
            pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
            pdm_configurator.DriverError();
            System.out.println("Error " + e.hashCode() + "  " + e.getMessage());
            return false;
            
        }
        
    }
    
    
    // Set y Get para a generacion del Json con el Token
    public String getSt_token() {return st_token;}

    public void setSt_token(String st_token) {this.st_token = st_token;}

    public boolean isBl_statusbackend() {
        return bl_statusbackend;
    }

    public void setBl_statusbackend(boolean bl_statusbackend) {
        this.bl_statusbackend = bl_statusbackend;
    }

    public String getSt_idd() {
        return st_idd;
    }

    public void setSt_idd(String st_idd) {
        this.st_idd = st_idd;
    }
    
    public void setgeneralerror(PDM_HandlerError Par_handlererror, String par_repuesta){
        
        if (handlererror==null){PDM_HandlerError handlererror = new PDM_HandlerError();}
        String ls_origen = Par_handlererror.getOrigen();
        getHandlererror().setOrigen(ls_origen);
        getHandlererror().setSclass(Par_handlererror.getSclass());
        getHandlererror().setTip(Par_handlererror.getTip());
        getHandlererror().setCod(Par_handlererror.getCod());
        getHandlererror().setUsermessage(Par_handlererror.getUsermessage());
        getHandlererror().setIntmessage(Par_handlererror.getIntmessage());
        setSt_respuesta(par_repuesta);
        
        return;
    }
    
    public  void setgeneralerror(String par_origen, 
                                            String par_class,
                                                String par_tipo,
                                                    String par_cod,
                                                        String par_messuser,
                                                            String par_messinter,
                                                                String par_repuesta){
        getHandlererror().setOrigen(par_origen);
        getHandlererror().setSclass(par_class);
        getHandlererror().setTip(par_tipo);
        getHandlererror().setCod(par_cod);
        getHandlererror().setUsermessage(par_messuser);
        getHandlererror().setIntmessage(par_messinter);
        setSt_respuesta(par_repuesta);
        
        return;
    }
    
    public  void setgeneralerror(){
        
        getHandlererror().setOrigen("00");
        getHandlererror().setSclass("");
        getHandlererror().setTip("");
        getHandlererror().setCod("");
        getHandlererror().setUsermessage("");
        getHandlererror().setIntmessage("");
        setSt_respuesta("00");
        
        return;
         
    }

    public  PDM_HandlerError getHandlererror() {
        if (handlererror==null){handlererror = new PDM_HandlerError();}
        return handlererror;
    }

    public void setHandlererror(PDM_HandlerError handlererror) {
        handlererror = handlererror;
    }

    public String getSt_respuesta() {
        return respuesta;
    }

    public void setSt_respuesta(String st_respuesta) {
        this.respuesta = st_respuesta;
    }
    
}
