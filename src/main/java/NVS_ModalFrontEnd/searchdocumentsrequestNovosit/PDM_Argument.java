/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ModalFrontEnd.searchdocumentsrequestNovosit;

/**
 *
 * @author Capas360
 */
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PDM_Argument {


private Boolean isNegative;

private PDM_LeftExpression leftExpression;

private PDM_RightExpression rightExpression;

private String source;

private String operator;

private String target;

private String targetFixedValue;

private String summaryFunction;

private PDM_Where where;

private List<String> involvedSources = null;

private String humanString;

public Boolean getIsNegative() {
return isNegative;
}

public void setIsNegative(Boolean isNegative) {
this.isNegative = isNegative;
}

public PDM_LeftExpression getLeftExpression() {
return leftExpression;
}

public void setLeftExpression(PDM_LeftExpression leftExpression) {
this.leftExpression = leftExpression;
}

public PDM_RightExpression getRightExpression() {
return rightExpression;
}

public void setRightExpression(PDM_RightExpression rightExpression) {
this.rightExpression = rightExpression;
}

public String getSource() {
return source;
}

public void setSource(String source) {
this.source = source;
}

public String getOperator() {
return operator;
}

public void setOperator(String operator) {
this.operator = operator;
}

public String getTarget() {
return target;
}

public void setTarget(String target) {
this.target = target;
}

public String getTargetFixedValue() {
return targetFixedValue;
}

public void setTargetFixedValue(String targetFixedValue) {
this.targetFixedValue = targetFixedValue;
}

public String getSummaryFunction() {
return summaryFunction;
}

public void setSummaryFunction(String summaryFunction) {
this.summaryFunction = summaryFunction;
}

public PDM_Where getWhere() {
return where;
}

public void setWhere(PDM_Where where) {
this.where = where;
}

public List<String> getInvolvedSources() {
return involvedSources;
}

public void setInvolvedSources(List<String> involvedSources) {
this.involvedSources = involvedSources;
}

public String getHumanString() {
return humanString;
}

public void setHumanString(String humanString) {
this.humanString = humanString;
}

}
