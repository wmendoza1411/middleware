/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ModalFrontEnd.DocHandleNovosit;

import NVS_ModalBackEnd.DocHandleNovosit.*;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PDM_KeywordMap {

private Integer dataType;
private Integer handle;
private String name;

public Integer getDataType() {
return dataType;
}

public void setDataType(Integer dataType) {
this.dataType = dataType;
}

public Integer getHandle() {
return handle;
}

public void setHandle(Integer handle) {
this.handle = handle;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

}