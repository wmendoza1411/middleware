/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ModalFrontEnd.DocHandleNovosit;

import NVS_ModalBackEnd.DocHandleNovosit.*;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

public class PDM_KeywordData {


private List<PDM_KeywordMap> keywordMap = null;

private List<PDM_Keyword> keywords = null;

private List<Object> recordMap = null;

private List<Object> records = null;

public List<PDM_KeywordMap> getKeywordMap() {
return keywordMap;
}

public void setKeywordMap(List<PDM_KeywordMap> keywordMap) {
this.keywordMap = keywordMap;
}

public List<PDM_Keyword> getKeywords() {
return keywords;
}

public void setKeywords(List<PDM_Keyword> keywords) {
this.keywords = keywords;
}

public List<Object> getRecordMap() {
return recordMap;
}

public void setRecordMap(List<Object> recordMap) {
this.recordMap = recordMap;
}

public List<Object> getRecords() {
return records;
}

public void setRecords(List<Object> records) {
this.records = records;
}

}