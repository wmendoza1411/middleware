/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ModalFrontEnd.DocHandleNovosit;

/**
 *
 * @author Capas360
 */
import NVS_Error.PDM_HandlerError;
import NVS_ModalBackEnd.DocHandleNovosit.*;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PDM_GetDocument {

private List<String> pagesBase64 = null;
private String contentType;
private Integer documentHandle;
private Integer documentTypeHandle;
private String lastUpdated;
private Integer version;
private Integer createdBy;
private String creationDate;
private String createdByName;
private String versionDate;
private String documentType;
private String reference;
private PDM_KeywordData keywordData;
private Object generationToken;
private List<Object> accesses = null;
private String respuesta;
private Object templateDocumentHandle;
private String internal_error= "";
private boolean bl_statusbackend;
private PDM_HandlerError handlererror;

    public String getInternal_error() {
        return internal_error;
    }

    public void setInternal_error(String internal_error) {
        this.internal_error = internal_error;
    }

    public boolean isBl_statusbackend() {
        return bl_statusbackend;
    }

    public void setBl_statusbackend(boolean bl_statusbackend) {
        this.bl_statusbackend = bl_statusbackend;
    }


    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

public List<String> getPagesBase64() {
return pagesBase64;
}

public void setPagesBase64(List<String> pagesBase64) {
this.pagesBase64 = pagesBase64;
}

public String getContentType() {
return contentType;
}

public void setContentType(String contentType) {
this.contentType = contentType;
}

public Integer getDocumentHandle() {
return documentHandle;
}

public void setDocumentHandle(Integer documentHandle) {
this.documentHandle = documentHandle;
}

public Integer getDocumentTypeHandle() {
return documentTypeHandle;
}

public void setDocumentTypeHandle(Integer documentTypeHandle) {
this.documentTypeHandle = documentTypeHandle;
}

public String getLastUpdated() {
return lastUpdated;
}

public void setLastUpdated(String lastUpdated) {
this.lastUpdated = lastUpdated;
}

public Integer getVersion() {
return version;
}

public void setVersion(Integer version) {
this.version = version;
}

public Integer getCreatedBy() {
return createdBy;
}

public void setCreatedBy(Integer createdBy) {
this.createdBy = createdBy;
}

public String getCreationDate() {
return creationDate;
}

public void setCreationDate(String creationDate) {
this.creationDate = creationDate;
}

public String getCreatedByName() {
return createdByName;
}

public void setCreatedByName(String createdByName) {
this.createdByName = createdByName;
}

public String getVersionDate() {
return versionDate;
}

public void setVersionDate(String versionDate) {
this.versionDate = versionDate;
}

public String getDocumentType() {
return documentType;
}

public void setDocumentType(String documentType) {
this.documentType = documentType;
}

public String getReference() {
return reference;
}

public void setReference(String reference) {
this.reference = reference;
}

public PDM_KeywordData getKeywordData() {
return keywordData;
}

public void setKeywordData(PDM_KeywordData keywordData) {
this.keywordData = keywordData;
}

public Object getGenerationToken() {
return generationToken;
}

public void setGenerationToken(Object generationToken) {
this.generationToken = generationToken;
}

public List<Object> getAccesses() {
return accesses;
}

public void setAccesses(List<Object> accesses) {
this.accesses = accesses;
}

public Object getTemplateDocumentHandle() {
return templateDocumentHandle;
}

public void setTemplateDocumentHandle(Object templateDocumentHandle) {
this.templateDocumentHandle = templateDocumentHandle;
}

public void resetGetDocument(){
                                                             
    /* this.identificador=null;
     this.tipo=null;
     this.cadena=null;
     this.peso=null;
     this.fecha=null;
     this.autor=null;
     this.telefono=null;
     this.mensaje=null;
     this.respuesta=null;
     this.keyword= null;*/
  
    }
     
     public void setgeneralerror(PDM_HandlerError Par_handlererror, String par_repuesta){
        
        if (handlererror==null){PDM_HandlerError handlererror = new PDM_HandlerError();}
        
        getHandlererror().setOrigen(Par_handlererror.getOrigen());
        getHandlererror().setSclass(Par_handlererror.getSclass());
        getHandlererror().setTip(Par_handlererror.getTip());
        getHandlererror().setCod(Par_handlererror.getCod());
        getHandlererror().setUsermessage(Par_handlererror.getUsermessage());
        getHandlererror().setIntmessage(Par_handlererror.getIntmessage());
        setRespuesta(par_repuesta);
        
        return;
        
    }
    
    public  void setgeneralerror(String par_origen, 
                                            String par_class,
                                                String par_tipo,
                                                    String par_cod,
                                                        String par_messuser,
                                                            String par_messinter,
                                                                String par_repuesta){
        getHandlererror().setOrigen(par_origen);
        getHandlererror().setSclass(par_class);
        getHandlererror().setTip(par_tipo);
        getHandlererror().setCod(par_cod);
        getHandlererror().setUsermessage(par_messuser);
        getHandlererror().setIntmessage(par_messinter);
        setRespuesta(par_repuesta);
        
        return;
    }
    
    public  void setgeneralerror(){
        
        getHandlererror().setOrigen("00");
        getHandlererror().setSclass("");
        getHandlererror().setTip("");
        getHandlererror().setCod("");
        getHandlererror().setUsermessage("");
        getHandlererror().setIntmessage("");
        
        return;
         
    }

    public  PDM_HandlerError getHandlererror() {
        if (handlererror==null){handlererror = new PDM_HandlerError();}
        return handlererror;
    }

    public void setHandlererror(PDM_HandlerError handlererror) {
        this.handlererror = handlererror;
    }


}
