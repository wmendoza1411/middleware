/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ModalFrontEnd.DocumentNovosit;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PDM_Keyword__Request {

@SerializedName("keywordHandle")
@Expose
private String keywordHandle;
@SerializedName("name")
@Expose
private String name;
@SerializedName("value")
@Expose
private String value;
@SerializedName("dataType")
@Expose
private String dataType;

public String getKeywordHandle() {
return keywordHandle;
}

public void setKeywordHandle(String keywordHandle) {
this.keywordHandle = keywordHandle;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public String getValue() {
return value;
}

public void setValue(String value) {
this.value = value;
}

public String getDataType() {
return dataType;
}

public void setDataType(String dataType) {
this.dataType = dataType;
}

}