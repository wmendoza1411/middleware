/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ModalFrontEnd.DocumentNovosit;


import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PDM_Documents_Request {

@SerializedName("documentType")
@Expose
private String documentType;
@SerializedName("documentData")
@Expose
private List<String> documentData = null;
@SerializedName("lastVersionDocHandle")
@Expose
private String lastVersionDocHandle;
@SerializedName("mimeType")
@Expose
private String mimeType;
@SerializedName("keywordData")
@Expose
private PDM_KeywordData_Request keywordData;

public String getDocumentType() {
return documentType;
}

public void setDocumentType(String documentType) {
this.documentType = documentType;
}

public List<String> getDocumentData() {
return documentData;
}

public void setDocumentData(List<String> documentData) {
this.documentData = documentData;
}

public String getLastVersionDocHandle() {
return lastVersionDocHandle;
}

public void setLastVersionDocHandle(String lastVersionDocHandle) {
this.lastVersionDocHandle = lastVersionDocHandle;
}

public String getMimeType() {
return mimeType;
}

public void setMimeType(String mimeType) {
this.mimeType = mimeType;
}

public PDM_KeywordData_Request getKeywordData() {
return keywordData;
}

public void setKeywordData(PDM_KeywordData_Request keywordData) {
this.keywordData = keywordData;
}

}