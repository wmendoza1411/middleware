/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ModalFrontEnd.DocumentNovosit;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PDM_Data_Request {

@SerializedName("keywords")
@Expose
private List<PDM_Keyword_Request> keywords = null;
@SerializedName("records")
@Expose
private List<PDM_Record_Request> records = null;

public List<PDM_Keyword_Request> getKeywords() {
return keywords;
}

public void setKeywords(List<PDM_Keyword_Request> keywords) {
this.keywords = keywords;
}

public List<PDM_Record_Request> getRecords() {
return records;
}

public void setRecords(List<PDM_Record_Request> records) {
this.records = records;
}

}