/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ModalFrontEnd.DocumentNovosit;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PDM_KeywordData_Request {

@SerializedName("originalDocumentHandle")
@Expose
private String originalDocumentHandle;
@SerializedName("data")
@Expose
private PDM_Data_Request data;
@SerializedName("skipKeywords")
@Expose
private Boolean skipKeywords;

public String getOriginalDocumentHandle() {
return originalDocumentHandle;
}

public void setOriginalDocumentHandle(String originalDocumentHandle) {
this.originalDocumentHandle = originalDocumentHandle;
}

public PDM_Data_Request getData() {
return data;
}

public void setData(PDM_Data_Request data) {
this.data = data;
}

public Boolean getSkipKeywords() {
return skipKeywords;
}

public void setSkipKeywords(Boolean skipKeywords) {
this.skipKeywords = skipKeywords;
}

}