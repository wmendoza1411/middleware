/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ModalFrontEnd.DocumentNovosit;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PDM_Record_Request  {

@SerializedName("name")
@Expose
private String name;
@SerializedName("recordHandle")
@Expose
private String recordHandle;
@SerializedName("keywords")
@Expose
private List<PDM_Keyword__Request> keywords = null;

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public String getRecordHandle() {
return recordHandle;
}

public void setRecordHandle(String recordHandle) {
this.recordHandle = recordHandle;
}

public List<PDM_Keyword__Request> getKeywords() {
return keywords;
}

public void setKeywords(List<PDM_Keyword__Request> keywords) {
this.keywords = keywords;
}

}