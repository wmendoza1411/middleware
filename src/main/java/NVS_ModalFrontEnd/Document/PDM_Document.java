/*
 */
package NVS_ModalFrontEnd.Document;

import NVS_Error.PDM_HandlerError;
import NVS_ModalFrontEnd.Autentication.PDM_Keyword;
import java.util.ArrayList;
import java.util.Objects;

import java.util.Objects;

/**
 *
 * @author Novatec wmendoza
 */
public class PDM_Document {
    
    String  identificador;
    String  tipo;
    String  cadena;
    String  peso;
    String  fecha;
    String  autor;
    String  telefono;
    String  documentType;
    String  cantidad;
    String  nro;
    Boolean ensamblar;
    
    private ArrayList<PDM_Keyword> keyword= null;
    private String token = "";
    private String internal_error = "";
    private boolean bl_statusbackend;
    String mensaje;  
    String respuesta;
    private PDM_HandlerError handlererror;

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }


    public ArrayList<PDM_Keyword> getKeyword() {
        return keyword;
    }

    public void setKeyword(ArrayList<PDM_Keyword> keyword) {
        this.keyword = keyword;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public PDM_Document() {
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String Tipo) {
        this.tipo = Tipo;
    }

    public String getCadena() {
        return cadena;
    }

    public void setCadena(String cadena) {
        this.cadena = cadena;
    }

    public String getPeso() {
        return peso;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }
    
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String Telefono) {
        this.telefono = Telefono;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getInternal_error() {
        return internal_error;
    }

    public void setInternal_error(String internal_error) {
        this.internal_error = internal_error;
    }

    public boolean isBl_statusbackend() {
        return bl_statusbackend;
    }

    public void setBl_statusbackend(boolean bl_statusbackend) {
        this.bl_statusbackend = bl_statusbackend;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getNro() {
        return nro;
    }

    public void setNro(String nro) {
        this.nro = nro;
    }

    public Boolean getEnsamblar() {
        return ensamblar;
    }

    public void setEnsamblar(Boolean ensamblar) {
        this.ensamblar = ensamblar;
    }
    
    
    
     public void resetDocument(){
                                                             
     this.identificador=null;
     this.tipo=null;
     this.cadena=null;
     this.peso=null;
     this.fecha=null;
     this.autor=null;
     this.telefono=null;
     this.mensaje=null;
     this.respuesta=null;
     this.keyword= null;
     this.nro = null;
     this.cantidad = null;
     this.ensamblar = false;
  
    }
     
     public void setgeneralerror(PDM_HandlerError Par_handlererror, String par_repuesta){
        
        if (handlererror==null){PDM_HandlerError handlererror = new PDM_HandlerError();}
        
        getHandlererror().setOrigen(Par_handlererror.getOrigen());
        getHandlererror().setSclass(Par_handlererror.getSclass());
        getHandlererror().setTip(Par_handlererror.getTip());
        getHandlererror().setCod(Par_handlererror.getCod());
        getHandlererror().setUsermessage(Par_handlererror.getUsermessage());
        getHandlererror().setIntmessage(Par_handlererror.getIntmessage());
        setRespuesta(par_repuesta);
        
        return;
        
    }
    
    public  void setgeneralerror(String par_origen, 
                                            String par_class,
                                                String par_tipo,
                                                    String par_cod,
                                                        String par_messuser,
                                                            String par_messinter,
                                                                String par_repuesta){
        getHandlererror().setOrigen(par_origen);
        getHandlererror().setSclass(par_class);
        getHandlererror().setTip(par_tipo);
        getHandlererror().setCod(par_cod);
        getHandlererror().setUsermessage(par_messuser);
        getHandlererror().setIntmessage(par_messinter);
        setRespuesta(par_repuesta);
        
        return;
    }
    
    public  void setgeneralerror(){
        
        getHandlererror().setOrigen("00");
        getHandlererror().setSclass("");
        getHandlererror().setTip("");
        getHandlererror().setCod("");
        getHandlererror().setUsermessage("");
        getHandlererror().setIntmessage("");
        
        return;
         
    }

    public  PDM_HandlerError getHandlererror() {
        if (handlererror==null){handlererror = new PDM_HandlerError();}
        return handlererror;
    }

    public void setHandlererror(PDM_HandlerError handlererror) {
        this.handlererror = handlererror;
    }

    
    
}

