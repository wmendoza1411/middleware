/*
*
* Clase que materializa al json del recurso login.....
* la misma mantendra los campos que objeto 
*/
package NVS_ModalFrontEnd.Autentication;

import java.util.ArrayList;

/**
 *
 * @author Novatec
 */
public class PDM_Tipo {
    
    private Integer id;
    private String tipo= "";
    private ArrayList<PDM_Keyword> keyword= null;

    public PDM_Tipo() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public ArrayList<PDM_Keyword> getKeyword() {
        return keyword;
    }

    public void setKeyword(ArrayList<PDM_Keyword> keyword) {
        this.keyword = keyword;
    }

   

    
   
    

  }
