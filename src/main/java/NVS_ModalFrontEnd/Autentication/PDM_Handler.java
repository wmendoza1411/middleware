/*
*
* Clase que materializa al json del recurso login.....
* la misma mantendra los campos que objeto 
*/
package NVS_ModalFrontEnd.Autentication;

import java.util.ArrayList;

/**
 *
 * @author Novatec
 */
public class PDM_Handler {
    
    private Integer id;
    private String area= "";
    private ArrayList<PDM_Tipo> tipo= null;

    public PDM_Handler() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public ArrayList<PDM_Tipo> getTipo() {
        return tipo;
    }

    public void setTipo(ArrayList<PDM_Tipo> tipo) {
        this.tipo = tipo;
    }
   
    

  }
