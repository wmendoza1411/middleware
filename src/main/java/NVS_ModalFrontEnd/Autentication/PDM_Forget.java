/*
*
* Clase que materializa al json del recurso Forget.....
* la misma mantendra los campos que objeto 
*/
package NVS_ModalFrontEnd.Autentication;

import NVS_Error.PDM_HandlerError;

/**
 *
 * @author wmendoza Veimen
 */
public class PDM_Forget {
    
    private String usuario          = "";
    private String token            = "";
    private String respuesta        = "";
    private String mensaje          = "";
    private String perfil           = "";
    private String email            = "";
    private String internal_error   = "";
    private boolean bl_statusbackend;
    private String idd              = "";
    private PDM_HandlerError        handlererror;

   
    
    public PDM_Forget(){}
    
    public void resetUser(){
        
        usuario          = "";
        token            = "";
        respuesta        = "";
        mensaje          = "";
        perfil           = "";
        email            = "";
        internal_error   = "";
        bl_statusbackend = false;
        idd              = "";
        
    }


    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    public String getInternal_error() {
        return internal_error;
    }

    public void setInternal_error(String internal_error) {
        this.internal_error = internal_error;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isBl_statusbackend() {
        return bl_statusbackend;
    }

    public void setBl_statusbackend(boolean bl_statusbackend) {
        this.bl_statusbackend = bl_statusbackend;
    }

    public String getIdd() {
        return idd;
    }

    public void setIdd(String idd) {
        this.idd = idd;
    }
    
     public void setgeneralerror(PDM_HandlerError Par_handlererror, String par_repuesta){
        
        if (handlererror==null){PDM_HandlerError handlererror = new PDM_HandlerError();}
        String ls_origen = Par_handlererror.getOrigen();
        getHandlererror().setOrigen(ls_origen);
        getHandlererror().setSclass(Par_handlererror.getSclass());
        getHandlererror().setTip(Par_handlererror.getTip());
        getHandlererror().setCod(Par_handlererror.getCod());
        getHandlererror().setUsermessage(Par_handlererror.getUsermessage());
        getHandlererror().setIntmessage(Par_handlererror.getIntmessage());
        setRespuesta(par_repuesta);
        
        return;
        
    }
    
    public  void setgeneralerror(String par_origen, 
                                            String par_class,
                                                String par_tipo,
                                                    String par_cod,
                                                        String par_messuser,
                                                            String par_messinter,
                                                                String par_repuesta){
        getHandlererror().setOrigen(par_origen);
        getHandlererror().setSclass(par_class);
        getHandlererror().setTip(par_tipo);
        getHandlererror().setCod(par_cod);
        getHandlererror().setUsermessage(par_messuser);
        getHandlererror().setIntmessage(par_messinter);
        setRespuesta(par_repuesta);
        
        return;
    }
    
    public  void setgeneralerror(){
        
        getHandlererror().setOrigen("00");
        getHandlererror().setSclass("");
        getHandlererror().setTip("");
        getHandlererror().setCod("");
        getHandlererror().setUsermessage("");
        getHandlererror().setIntmessage("");
        setRespuesta("00");
        
        return;
         
    }

    public  PDM_HandlerError getHandlererror() {
        if (handlererror==null){handlererror = new PDM_HandlerError();}
        return handlererror;
    }

    public void setHandlererror(PDM_HandlerError handlererror) {
        handlererror = handlererror;
    }
    
    
}
