/*
*
* Clase que materializa al json del recurso login.....
* la misma mantendra los campos que objeto 
*/
package NVS_ModalFrontEnd.Autentication;

import NVS_Error.PDM_HandlerError;
import java.util.ArrayList;

/**
 *
 * @author Novatec
 */
public class PDM_Login {
    
    private String usuario          = "";
    private String clave            = "";
    private String respuesta        = "";
    private String mensaje          = "";
    private ArrayList<PDM_Handler> perfil;
    private String token            = "";
    private String internal_error   = "";
    private boolean bl_statusbackend;
    private String idd              = "";
    private PDM_HandlerError handlererror;
    

    public PDM_Login(String usuario,
                        String clave,
                            String dns,
                                String respuesta,
                                    String mensaje,
                                        ArrayList<PDM_Handler> perfil,
                                            String telefono,
                                                String lap,
                                                    
                                                        String token, 
                                                            String internal_error) {
        this.usuario        = usuario;
        this.clave          = clave;
        
        this.respuesta      = respuesta;
        this.mensaje        = mensaje;
        this.perfil         = perfil;
        this.token          = token;
        
        
       
        this.internal_error = internal_error;
        
    }

    public PDM_Login() {}
    
    public void resetUSuario(){
                                                             
        this.usuario        = "";
        this.clave          = "";
        this.respuesta      = "";
        this.mensaje        = "";
        this.perfil         = null;
        this.token          = "";
        this.internal_error = "";
        this.idd            = "";
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public ArrayList<PDM_Handler> getPerfil() {
        return perfil;
    }

    public void setPerfil(ArrayList<PDM_Handler> perfil) {
        this.perfil = perfil;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


    public String getInternal_error() {
        return internal_error;
    }

    public void setInternal_error(String internal_error) {
        this.internal_error = internal_error;
    }

    public boolean isBl_statusbackend() {
        return bl_statusbackend;
    }

    public void setBl_statusbackend(boolean bl_statusbackend) {
        this.bl_statusbackend = bl_statusbackend;
    }

    public String getIdd() {
        return idd;
    }

    public void setIdd(String idd) {
        this.idd = idd;
    }
    
    public void setgeneralerror(PDM_HandlerError Par_handlererror, String par_repuesta){
        
        if (handlererror==null){PDM_HandlerError handlererror = new PDM_HandlerError();}
        
        getHandlererror().setOrigen(Par_handlererror.getOrigen());
        getHandlererror().setSclass(Par_handlererror.getSclass());
        getHandlererror().setTip(Par_handlererror.getTip());
        getHandlererror().setCod(Par_handlererror.getCod());
        getHandlererror().setUsermessage(Par_handlererror.getUsermessage());
        getHandlererror().setIntmessage(Par_handlererror.getIntmessage());
        setRespuesta(par_repuesta);
        
        return;
        
    }
    
    public  void setgeneralerror(String par_origen, 
                                            String par_class,
                                                String par_tipo,
                                                    String par_cod,
                                                        String par_messuser,
                                                            String par_messinter,
                                                                String par_repuesta){
        getHandlererror().setOrigen(par_origen);
        getHandlererror().setSclass(par_class);
        getHandlererror().setTip(par_tipo);
        getHandlererror().setCod(par_cod);
        getHandlererror().setUsermessage(par_messuser);
        getHandlererror().setIntmessage(par_messinter);
        setRespuesta(par_repuesta);
        
        return;
    }
    
    public  void setgeneralerror(){
        
        getHandlererror().setOrigen("00");
        getHandlererror().setSclass("");
        getHandlererror().setTip("");
        getHandlererror().setCod("");
        getHandlererror().setUsermessage("");
        getHandlererror().setIntmessage("");
        
        return;
         
    }

    public  PDM_HandlerError getHandlererror() {
        if (handlererror==null){handlererror = new PDM_HandlerError();}
        return handlererror;
    }

    public void setHandlererror(PDM_HandlerError handlererror) {
        handlererror = handlererror;
    }
   
}
