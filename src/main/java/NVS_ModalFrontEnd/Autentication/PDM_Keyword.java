/*
*
* Clase que materializa al json del recurso login.....
* la misma mantendra los campos que objeto 
*/
package NVS_ModalFrontEnd.Autentication;

import java.util.ArrayList;

/**
 *
 * @author Novatec
 */
public class PDM_Keyword {
    
    private Integer id;
    private String label= "";
    private String valor= "";
    private String dataType= null;

    public PDM_Keyword() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

   
   
    
   
    

  }
