/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ModalFrontEnd.ContextDefinitionNovositi;

import NVS_Error.PDM_HandlerError;
import NVS_ModalFrontEnd.Autentication.PDM_Keyword;
import java.util.ArrayList;

/**
 *
 * @author Capas360
 */
public class PDM_ContextDefinition {
    private String documentHandle;
    
    String mensaje;
    String respuesta;
    private ArrayList<PDM_Keyword> keyword= null;
    private PDM_HandlerError handlererror;

    public PDM_ContextDefinition() {
    }

    public String getDocumentHandle() {
        return documentHandle;
    }

    public void setDocumentHandle(String documentHandle) {
        this.documentHandle = documentHandle;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }
    
     public void setgeneralerror(PDM_HandlerError Par_handlererror, String par_repuesta){
        
        if (handlererror==null){PDM_HandlerError handlererror = new PDM_HandlerError();}
        
        getHandlererror().setOrigen(Par_handlererror.getOrigen());
        getHandlererror().setSclass(Par_handlererror.getSclass());
        getHandlererror().setTip(Par_handlererror.getTip());
        getHandlererror().setCod(Par_handlererror.getCod());
        getHandlererror().setUsermessage(Par_handlererror.getUsermessage());
        getHandlererror().setIntmessage(Par_handlererror.getIntmessage());
        setRespuesta(par_repuesta);
        
        return;
        
    }
    
    public  void setgeneralerror(String par_origen, 
                                            String par_class,
                                                String par_tipo,
                                                    String par_cod,
                                                        String par_messuser,
                                                            String par_messinter,
                                                                String par_repuesta){
        getHandlererror().setOrigen(par_origen);
        getHandlererror().setSclass(par_class);
        getHandlererror().setTip(par_tipo);
        getHandlererror().setCod(par_cod);
        getHandlererror().setUsermessage(par_messuser);
        getHandlererror().setIntmessage(par_messinter);
        setRespuesta(par_repuesta);
        
        return;
    }
    
    public  void setgeneralerror(){
        
        getHandlererror().setOrigen("00");
        getHandlererror().setSclass("");
        getHandlererror().setTip("");
        getHandlererror().setCod("");
        getHandlererror().setUsermessage("");
        getHandlererror().setIntmessage("");
        
        return;
         
    }

    public  PDM_HandlerError getHandlererror() {
        if (handlererror==null){handlererror = new PDM_HandlerError();}
        return handlererror;
    }

    public void setHandlererror(PDM_HandlerError handlererror) {
        this.handlererror = handlererror;
    }

    
    
    
}
