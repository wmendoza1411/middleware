/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_Middleware;

import NVS_ClientBackEnd.CRN_PostDocument;
import NVS_ClientBackEnd.CRN_DocHandle;
import NVS_ClientBackEnd.CRN_DocumentAccess;
import NVS_ClientBackEnd.CRN_GenerationExample;
import NVS_ClientBackEnd.CRN_Searchdocumentsrequest;
import NVS_ClientBackEnd.CRN_UserDocumentTypesProfile;
import NVS_ClientBackEnd.CRN_UserProfile;
import NVS_Configurator.PDM_Configurator;
import NVS_ModalBackEnd.DocumentAccessNovosit.DocumentAccess;

import NVS_ModalBackEnd.PostDocumentNovosit.PDM_PostDocumentRequest;
import NVS_ModalBackEnd.PostDocumentsGenerationRequest.Keywords;
import NVS_ModalBackEnd.keywordNovosit.Keyword;
import NVS_ModalBackEnd.keywordNovosit.UserDocumentType;
import NVS_ModalBackEnd.searchdocumentsrequestNovosit.Searchdocumentsrequest;
import NVS_ModalFrondEnd.DocumentAccessNovosit.PDM_DocumentAccess;

import NVS_ModalFrontEnd.Autentication.PDM_Keyword;
import NVS_ModalFrontEnd.Document.PDM_Document;
import NVS_ModalFrontEnd.DocumentNovosit.PDM_Data_Request;
import NVS_ModalFrontEnd.DocumentNovosit.PDM_Documents_Request;
import NVS_ModalFrontEnd.DocumentNovosit.PDM_KeywordData_Request;
import NVS_ModalFrontEnd.DocumentNovosit.PDM_Keyword_Request;
import NVS_ModalFrontEnd.DocumentNovosit.PDM_Keyword__Request;
import NVS_ModalFrontEnd.DocumentNovosit.PDM_Record_Request;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import com.itextpdf.text.Anchor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSmartCopy;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;


/**
 *
 * @author wmendoza
 */
public class NVS_MiddDocument {
    
    // Instance of modal and class Configurator
    PDM_Configurator                pdm_configurator;
    PDM_PostDocumentRequest         pdm_postdocumentRequest;
    List<String> enconde_general=new ArrayList<String>();
    
    // Constructor
    public void NVS_MiddDocument(){   }
    
    
    // Consume Back End Profile Send Document
    public boolean sendDocument(PDM_Configurator pdm_configurator) 
                                     throws NoSuchMethodException{
        
        // Get the Modal class Pointer from Configurator
        this.pdm_configurator       =   pdm_configurator;
        PDM_Document pdm_document   =   this.pdm_configurator.getPdm_document();
        Integer sw=0;
        String base64ImageDocumentoFinal="";
        
        
        File carpeta_configuracion=null;
        
        try{
            
            /////Rutas de carpetas contenedoras de archivos
            String Ruta_principal=pdm_document.getIdentificador();
            String Ruta_configuracion=pdm_document.getIdentificador()+"/configuracion";
            String Ruta_paginas=pdm_document.getIdentificador()+"/configuracion/paginas";
            ///////////////////////////////////////////////////////////     
            ///////////////////////////////////////Creacion de carpetas contenedoras de archivos      
            File carpeta_principal=new File(Ruta_principal);
            if(!carpeta_principal.exists()) 
                carpeta_principal.mkdir();

            carpeta_configuracion=new File(Ruta_configuracion);
            if(!carpeta_configuracion.exists()) 
                carpeta_configuracion.mkdir();

            File carpeta_paginas=new File(Ruta_paginas);
            if(!carpeta_paginas.exists()) 
                carpeta_paginas.mkdir();
         
            ////////////////////////////////////////////creacion de archivos
        
            ////////////////////////identificado del archivo y creacion
            String tipo=pdm_document.getTipo();
            String archivoJSON=pdm_document.getNro()+"."+tipo;
            String pagina_nueva=Ruta_paginas+"/"+archivoJSON;
            
            FileOutputStream imageOutFile = new FileOutputStream(pagina_nueva);
            byte[] imageByteArray = Base64.getDecoder().decode(pdm_document.getCadena());
            imageOutFile.write(imageByteArray);
            File file2=new File(pagina_nueva);
            imageOutFile.close();
                   
        
            ///////////////////////////////////////////encontrar cantidad de archivos ingresados con ese identificador
             String[] arrArchivosp = carpeta_paginas.list();
             int totalp = 0;
             totalp= (arrArchivosp.length);

            ///////////////validacion de numero mayor a cantidad en el json     
            if(Integer.parseInt(pdm_document.getNro())>
                        Integer.parseInt(pdm_document.getCantidad())){

                pdm_document.resetDocument();
                pdm_document.setgeneralerror(
                pdm_configurator.driver_err("10","20","30"," ","Error numero mayor a cantidad",""),"50");
                return false;
                         
            }
        
            ////////////////////////condicional si numero es igual a cantidad de archivos
            if(Integer.parseInt(pdm_document.getCantidad())==totalp){

                String[] arrArchivos = carpeta_paginas.list();
                Integer total= arrArchivos.length;

                Document documento = new Document();
                String formato_archivo =pdm_document.getTipo();
        
                   //validacion de formatos probados de tipo imagen  
                if(formato_archivo.equals("png") || 
                        formato_archivo.equals("jpeg") || 
                            formato_archivo.equals("jpg")){  
        
                    ///////////////creacion de documento para este caso
                    PdfWriter.getInstance(documento, 
                            new FileOutputStream(Ruta_configuracion+"/documento.pdf"));
                    documento.open();
                    for(int i=0; i<arrArchivos.length; ++i){
                       String base64ImageString="";

                       Image imagen = Image.getInstance(carpeta_paginas + "/" +arrArchivos[i]);
                       documento.add(imagen);
                       documento.newPage();
                       
                       File  file = new File(carpeta_paginas + "/" +arrArchivos[i]);
                       try (FileInputStream imageInFile = 
                                           new FileInputStream(file)) {//byte imageData[]
                   
                            byte imageData[] = new byte[(int) file.length()];

                            imageInFile.read(imageData);
                            base64ImageString = Base64.getEncoder().encodeToString(imageData);
                            System.out.print("(d)");
                            enconde_general.add(base64ImageString);
                             System.out.print("(e)");     
                        
                        } catch (FileNotFoundException e) {
                             // Register Error
                            pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
                            pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
                            pdm_configurator.DriverError();
            // Not respond with Json
                        } catch (IOException ioe) {
                             // Register Error
                            pdm_configurator.getPdm_error().setSt_error(ioe.hashCode() + "  " + ioe.getMessage());
                            pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
                            pdm_configurator.DriverError();
            // Not respond with Json
                        }

                    }  
                                 
                    ////////////////////////// se finaliza el documento
                    documento.close();

                }
                ///////////caso formato pdf 
                if(formato_archivo.equals("pdf")){  
        
                    Integer totalPages=0;
                    List<PdfReader> readers =  new ArrayList<PdfReader>();
                    /////////////////////////////////////////
                    for(int i=0; i<arrArchivos.length; ++i){
                    
                         PdfReader pdfReader=null;
                         pdfReader = new PdfReader(Ruta_paginas+"/"+arrArchivos[i]); 
                         readers.add(pdfReader);//se alamacena uno a uno los documentos del listado
                         totalPages = totalPages + pdfReader.getNumberOfPages();
                    }  
         
                     ///se especifica el nombre del documento
                     FileOutputStream imageOutFile2 = new 
                                FileOutputStream(Ruta_configuracion+"/documento.pdf");            
                     PdfWriter writer = PdfWriter.getInstance(documento, imageOutFile2);
                     documento.open();
                      //////////////////////////////////se crea el documento                       
                     PdfContentByte pageContentByte = writer.getDirectContent();

                     PdfImportedPage pdfImportedPage;
                     int currentPdfReaderPage = 1;
                     Iterator<PdfReader> iteratorPDFReader = readers.iterator();
                               
                    while (iteratorPDFReader.hasNext()) {
        
                        PdfReader pdfReader2 = iteratorPDFReader.next();
                        System.out.println("dentro:"+pdfReader2.getNumberOfPages());  
                        //Create page and add content.
                        while (currentPdfReaderPage <= pdfReader2.getNumberOfPages()) {

                            documento.newPage();
                            pdfImportedPage = writer.getImportedPage(
                            pdfReader2,currentPdfReaderPage);
                            pageContentByte.addTemplate(pdfImportedPage, 0, 0);
                            currentPdfReaderPage++;
                        }
                        currentPdfReaderPage = 1;
                    }       
                                
                    //Close document and outputStream.
                    imageOutFile2.flush();
                    documento.close();
                    imageOutFile2.close();
                    //////se finaliza el documento
                }
    
                /////////////////////////se crea un documento final en pdf con un nombre y ruta especifica dentro del directorio
                File  file_final = new File(Ruta_configuracion+"/documento.pdf");
        
                try (FileInputStream imageInFile_do = 
                    new FileInputStream(file_final)) {//byte imageData[]
                   
                // Reading a Image file from file system
                byte imageData[] = new byte[(int) file_final.length()];
                imageInFile_do.read(imageData);
                base64ImageDocumentoFinal = Base64.getEncoder().encodeToString(imageData); 
                sw=1;       
                imageInFile_do.close();
                 
                        
                } catch (FileNotFoundException e) {
			 // Register Error
                    pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
                    pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
                    pdm_configurator.DriverError();
            // Not respond with Json
                } catch (IOException ioe) {
			 // Register Error
                    pdm_configurator.getPdm_error().setSt_error(ioe.hashCode() + "  " + ioe.getMessage());
                    pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
                    pdm_configurator.DriverError();
            // Not respond with Json
                }
                /////////////////////////////////////////fin crea documento 

                /////////////////////////Inicio de los casos de ensamblar
                ///caso ensamblar igual a falso
                if(pdm_document.getEnsamblar()==false ){
    
                    if(!pdm_document.getTipo().equals("pdf")){//si es distinto 
                                                              // de pdf el formato es de tipo imagen 
                                                              // y se envia sin convertir en pdf
                        pdm_document.setCadena("");
                        pdm_document.setTipo("image/"+pdm_document.getTipo());

                    }else{//si es formato pdf se envia el ultimo archivo enviado
                          pdm_document.setTipo("application/pdf");

                    }
                    // Set the pdm_document Modal to Configurator     
                    pdm_configurator.setPdm_document(pdm_document);

                    ///////////////////////////////////////////////busqueda de keyword por Id
                    if(!getUserDocumentTypes(pdm_configurator)){
                    pdm_document.resetDocument();
                            pdm_document.setgeneralerror(
                            pdm_configurator.driver_err("10","20","30"," ","Error Permiso",""),"50");
                            return false;
                    }
                    //////////////////////////////////////////////////////

                    // Get Pointer CRN_PostDocument from Configurator
                    CRN_PostDocument crn_postdocument = pdm_configurator.getCrn_postdocument();

                    boolean lb_retursdp=false; 
                    // Set to Back End PDM_document
                    lb_retursdp = crn_postdocument.SendDocumentProfile(pdm_configurator);
                    if (lb_retursdp==true){
                        // The set to Back End is OK
                        pdm_document.setCadena("");
                        pdm_document.setgeneralerror(pdm_configurator.driver_err(),"00");
                        pdm_document.setMensaje(String.valueOf(pdm_configurator.getPdm_postdocumentrequest().getDocumentHandle()));
                        pdm_configurator.setPdm_document(pdm_document);

                        try{    
                            //una vez enviado correctamente todos los archivos se eliminar todos los directorios generados
                            File carpeta_principal_borrar=new File(Ruta_principal);
                            if (carpeta_principal_borrar.exists()){ 
                                    carpeta_principal_borrar.delete(); 
                            }
                        }catch(Exception e){
                         // Register Error
                            pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
                            pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
                            pdm_configurator.DriverError();
            // Not respond with Json
                        }
                            return true;
                    }
                }
   
                //////////////caso ensamblar igual a verdadero
                if(pdm_document.getEnsamblar()==true ){
                    ///////////////////////////////////////en este caso se envia un solo archivo en formato pdf
                    enconde_general=new ArrayList<String>();

                    pdm_document.setTipo("application/pdf");    
                    pdm_document.setCadena(base64ImageDocumentoFinal);

                    pdm_configurator.setPdm_document(pdm_document);

                    ///////////////////////////////////////////////busqueda de keyword por Id
                    if(!getUserDocumentTypes(pdm_configurator)){

                    pdm_document.resetDocument();
                            pdm_document.setgeneralerror(
                            pdm_configurator.driver_err("10","20","30"," ","Error Permiso",""),"50");
                            return false;
                    }
                    //////////////////////////////////////////////////////

                    // Get Pointer CRN_PostDocument from Configurator
                   CRN_PostDocument crn_postdocument = pdm_configurator.getCrn_postdocument();

                    boolean lb_retursdp=false; 

                    // Set to Back End PDM_document
                    lb_retursdp = crn_postdocument.SendDocumentProfile(pdm_configurator);
                    if (lb_retursdp==true){

                        // The set to Back End is OK
                        pdm_document.setCadena("");
                        pdm_document.setgeneralerror(pdm_configurator.driver_err(),"00");
                        pdm_document.setMensaje(String.valueOf(pdm_configurator.getPdm_postdocumentrequest().getDocumentHandle()));
                        pdm_configurator.setPdm_document(pdm_document);

                        try{    
                            File carpeta_principal_borrar=new File(Ruta_principal);
                            if (carpeta_principal_borrar.exists()){ 
                                carpeta_principal_borrar.delete();

                            }
                        }catch(Exception e){
                        
                             // Register Error
                            pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
                            pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
                            pdm_configurator.DriverError();
            // Not respond with Json
                        }

                        return true;
                    }
                }
   
            }                  

        }
        catch(Exception e){
            
             // Register Error
             try{
               pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
               pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
               pdm_configurator.DriverError();
               
               pdm_document.resetDocument();
               pdm_document.setgeneralerror(
              pdm_configurator.driver_err("10","20","30"," ","Error en formato de documento",""),"50");
             }catch(Exception e_){}
            // Not respond with Json
            
            
           return false;
        }
        
        pdm_document.setCadena("");
        pdm_document.setgeneralerror(pdm_configurator.driver_err(),"00");
        pdm_document.setMensaje("Documento "+pdm_document.getNro()+" recibido");
        pdm_configurator.setPdm_document(pdm_document);
      
        return true;
    }
    
    
     // Consume Back End Profile Send Document
    public boolean getUserDocumentTypes(PDM_Configurator pdm_configurator) 
                                     throws NoSuchMethodException, SecurityException, IOException{
    
        PDM_Document pdm_document   =   pdm_configurator.getPdm_document();
       
        try{
          //crn_getinfo.getServiceRS(pdm_configurator);
       
            CRN_UserProfile crn_userProfile = pdm_configurator.getPdm_userprofile();
            this.pdm_configurator.getPdm_login().setUsuario(this.pdm_configurator.getPdm_paramatermodal().getAutenticationuser()); 
            if(!crn_userProfile.getUserProfile(this.pdm_configurator)){
              
               return false;
            }
                 
            CRN_UserDocumentTypesProfile crn_userdocumenttypesprofile = pdm_configurator.getCrn_userdocumenttypesprofile();
            crn_userdocumenttypesprofile.getUserDocumentTypes(pdm_configurator);
           
       
           
            PDM_Document pdm_document_e=pdm_configurator.getPdm_document();
          
            List<UserDocumentType> userdocumenttypelist =pdm_configurator.getUserdocumenttype();
           
            for(UserDocumentType userdocumenttype:userdocumenttypelist){
                for(Keyword keywords:userdocumenttype.getKeywords()){
                    Integer contador=0;
                    while(contador<pdm_document_e.getKeyword().size()){
                   
                        if(keywords.getDefinition().getId()==
                                    pdm_document_e.getKeyword().get(contador)
                                               .getId()){
                             pdm_document_e.getKeyword().get(contador).setLabel(keywords.getProperties().getName());
                             pdm_document_e.getKeyword().get(contador).setDataType(keywords.getProperties().getDataType());

                        }
            
                        contador++;
                    }
                }
            }
           
            pdm_configurator.setPdm_document(pdm_document_e);   
       
            //////////////////////////////Armar Request document
       
                /////////////////////////////////////////////////nuevo area con el formato establecido en documentancion
            PDM_Documents_Request obja=new PDM_Documents_Request();
            List<String> documentData=new ArrayList<String>();
            
            if(enconde_general.size()==0)
                documentData.add(pdm_configurator.getPdm_document().getCadena());
             else
                documentData=enconde_general;    
            
                
            obja.setDocumentType(pdm_configurator.getPdm_document().getDocumentType());//4
           
            obja.setDocumentData(documentData);
            obja.setLastVersionDocHandle("0");
            obja.setMimeType(pdm_configurator.getPdm_document().getTipo());
            
            PDM_KeywordData_Request objb=new PDM_KeywordData_Request();
            objb.setOriginalDocumentHandle("1");
            
            PDM_Data_Request objc=new PDM_Data_Request();
           
            List<PDM_Keyword_Request> list_pdmt_keyword=new ArrayList<PDM_Keyword_Request>();
            
            for(PDM_Keyword pdm_keyword:pdm_configurator.getPdm_document().getKeyword()){
                 
                PDM_Keyword_Request objd=new PDM_Keyword_Request();
                objd.setKeywordHandle(String.valueOf(pdm_keyword.getId()));//5
                objd.setName(pdm_keyword.getLabel());
                objd.setValue(pdm_keyword.getValor());
                objd.setDataType(pdm_keyword.getDataType());
             
                list_pdmt_keyword.add(objd);
            }
           
            List<PDM_Keyword__Request> list_pdmt_keyword_=new ArrayList<PDM_Keyword__Request>();
            List<PDM_Record_Request> list_pdmt_record=new ArrayList<PDM_Record_Request>();
            objc.setKeywords(list_pdmt_keyword);
            objc.setRecords(list_pdmt_record);
            
            objb.setData(objc);
            objb.setSkipKeywords(false);
            obja.setKeywordData(objb);
            
            pdm_configurator.setPdm_document_request(obja);
          
        }
        
        catch(Exception e){
            // Register Error
           try {
                 // Register Error
                 
                 pdm_configurator.getSearchdocumentsrequest().setgeneralerror(
                 pdm_configurator.driver_err("10","30","20"," ","context Definition",e.getMessage()),"50");
                 // Register Error
                 pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
                 pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
                 pdm_configurator.DriverError();
                 return false;
            } catch (IllegalAccessException ex) {
                 Logger.getLogger(NVS_MiddDocument.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalArgumentException ex) {
                 Logger.getLogger(NVS_MiddDocument.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                 Logger.getLogger(NVS_MiddDocument.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                 Logger.getLogger(NVS_MiddDocument.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                 Logger.getLogger(NVS_MiddDocument.class.getName()).log(Level.SEVERE, null, ex);
            }
            return false;
        }
        
        return true;
    }    
    
}
