/*
 * Cod. de errores: 
 * SEGU010:  Segurity.
 * BAEN010:  BankEnd UNANSWERED.
 * REST010:  Error Server Rest
 * MIDE010:  Error Middleware.
 * xx origen, xx tipo, xx class, xxxxxxx Codigo, User Mensaje, Internal Error
 */
package NVS_Middleware;

import NVS_ClientBackEnd.CRN_ProfileDocument;
import NVS_ClientBackEnd.CRN_UserDocumentTypesProfile;
import NVS_ClientBackEnd.CRN_UserProfile;

import NVS_ModalFrontEnd.Autentication.PDM_Login;
import NVS_ModalFrontEnd.Autentication.PDM_Join;
import NVS_ModalFrontEnd.Autentication.PDM_Forget;

import NVS_Configurator.PDM_Configurator;
import NVS_ModalBackEnd.LoginNovosit.Role;

import NVS_ModalBackEnd.LoginNovosit.Users;
import NVS_ModalBackEnd.PostDocumentsGenerationRequest.Keywords;
import NVS_ModalBackEnd.ProfileDocument.DocumentTypeAccess;
import NVS_ModalBackEnd.ProfileDocument.ProfileDocument;
import NVS_ModalBackEnd.keywordNovosit.Access;
import NVS_ModalBackEnd.keywordNovosit.Keyword;
import NVS_ModalBackEnd.keywordNovosit.UserDocumentType;
import NVS_ModalFrontEnd.Autentication.PDM_Handler;
import NVS_ModalFrontEnd.Autentication.PDM_Keyword;
import NVS_ModalFrontEnd.Autentication.PDM_Tipo;
import com.google.gson.Gson;
import java.io.IOException;


import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Response;


/**
 *
 * @author wmendoza
 */
public class NVS_MiddAutentication {
   
    PDM_Configurator    pdm_configurator;
    PDM_Login           pdm_login;
    PDM_Join            pdm_join;
    PDM_Forget          pdm_forget;
    Users               users;
    
    String st_user = "";
    String st_key  = "";

    public String getSt_user() {return st_user;}

    public void setSt_user(String st_user) {this.st_user = st_user;}

    public String getSt_key() {return st_key;}

    public void setSt_key(String st_key) {this.st_key = st_key;}
    
    
    // Midleware Joint Fron End (Vs) Back End
    public boolean join (PDM_Configurator pdm_configurator) throws SecurityException, IOException{
        try{
        
            this.pdm_configurator   = pdm_configurator;
            this.pdm_login          = pdm_configurator.getPdm_login();

            try{
                // Set error
                pdm_login.resetUSuario();
                pdm_login.setgeneralerror(pdm_configurator.driver_err("10","20","10","BAEN010","Error Autentication ",""),"50");
            }catch (Exception e) {return false;}
           
            // User and key (Fron End) to Configurator
            this.pdm_configurator.setSt_login_key(pdm_login.getClave());
            this.pdm_configurator.setSt_login_user(pdm_login.getUsuario());

            pdm_login.setClave("");
            pdm_login.setUsuario("");

            return true;
        }
        catch (Exception e) {
               
            // Register Error
            pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
            pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
            pdm_configurator.DriverError();
     
            // Not respond with Json
            return false;
        } 
    }
    
    // Midleware Forget Fron End (Vs) Back End
    public boolean forget (PDM_Configurator pdm_configurator) throws SecurityException, IOException{
        try{
        
            this.pdm_configurator   = pdm_configurator;
            this.pdm_forget         = pdm_configurator.getPdm_forget();

            try{
                // Set error
                this.pdm_forget.resetUser();
                this.pdm_forget.setgeneralerror(pdm_configurator.driver_err(
                                        "10","20","10","","Error Autentication ",""),"50");
            }catch (Exception e) {
                 // Register Error
                 // Register Error
               pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
               pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
               pdm_configurator.DriverError();
            // Not respond with Json
                return false;}
           
            // User and key (Fron End) to Configurator
            this.pdm_configurator.setSt_login_key(pdm_login.getClave());
            this.pdm_configurator.setSt_login_user(pdm_login.getUsuario());

            pdm_login.setClave("");
            pdm_login.setUsuario("");

            return true;
        }
        catch (Exception e) {
            // Register Error
             // Register Error
               pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
               pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
               pdm_configurator.DriverError();
            // Not respond with Json
            return false;
        } 
    }
    
    
    // Methodo for intermediation  Front End / Back End Login
    public boolean login (PDM_Configurator pdm_configurator) throws SecurityException, IOException {
        
        try{
        
            this.pdm_configurator   = pdm_configurator;
            this.pdm_login          = pdm_configurator.getPdm_login();

            try{
               CRN_UserProfile crn_userProfile = this.pdm_configurator.getPdm_userprofile();
                
                //  Get Login front Back End
                if (!crn_userProfile.getUserProfile(this.pdm_configurator)) { // Error, login BackEnd Not OK                   
                    // build the json answer with the handlererror and login classes
                    pdm_login.resetUSuario();
                    pdm_login.setgeneralerror(pdm_configurator.driver_err("10","20","10","BAEN010","Error Autentication ",""),"50");
                    return false;
                }
               
                
                //  Get Login front Back End
                CRN_ProfileDocument crn_profiledocument = this.pdm_configurator.getCrn_profiledocument();
                
                 if (!crn_profiledocument.getProfileDocument(this.pdm_configurator)) { // Error, login BackEnd Not OK                   
                    // build the json answer with the handlererror and login classes
                   
                    pdm_login.setgeneralerror(pdm_configurator.driver_err("10","20","10","BAEN010","Error autentication ",""),"50");
                    return false;
                }
                
                 // Get modal Fron Back End
                 
                 
                CRN_UserDocumentTypesProfile crn_userdocumenttypesprofile = this.pdm_configurator.getCrn_userdocumenttypesprofile();
               
                //  Get Login front Back End
               
                if (!crn_userdocumenttypesprofile.getUserDocumentTypes(this.pdm_configurator)) { // Error, login BackEnd Not OK                   
                    // build the json answer with the handlererror and login classes
                   
                    pdm_login.setgeneralerror(pdm_configurator.driver_err("10","20","10","BAEN010","Error Autentication ",""),"50");
                    return false;
                }
               
                // Get modal Fron Back End
              
                //////////////////////////////////////////////////
               
                // the fun begins................
                loginMiddileware();
                
                
                this.pdm_configurator.setPdm_login(pdm_login);
                
                // User and key (Fron End) to Configurator
                this.pdm_configurator.setSt_login_key(pdm_login.getClave());
                this.pdm_configurator.setSt_login_user(pdm_login.getUsuario());
               
                return true;
                // Set error
                //pdm_login.resetUSuario();
               // pdm_login.setgeneralerror(pdm_configurator.driver_err("10","20","10","","Error Autentication ",""),"50");
            }catch (Exception e) {
                
                 // Register Error
               pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
               pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
               pdm_configurator.DriverError();
            // Not respond with Json
                
                return false;}
        }
        catch (Exception e) {
               
            // Register Error
               pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
               pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
               pdm_configurator.DriverError();
            // Not respond with Json
            return false;
        } 
    }
    
    // Move user login front Modal Back End to Modal Fron End
    private boolean loginMiddileware(){
        
      
       List<UserDocumentType> userdocumenttypelistafinal=new ArrayList<UserDocumentType>();
       List<UserDocumentType> userdocumenttype=pdm_configurator.getUserdocumenttype();
       
       //Buscar Keyword por Roles del usuario
       Users usuario= pdm_configurator.getUser();
       ProfileDocument profiledocument=pdm_configurator.getProfiledocument();
      List<DocumentTypeAccess> documentTypeAccesses=new ArrayList<DocumentTypeAccess>();
       
       for(UserDocumentType userdocumenttyper:userdocumenttype){
            Integer sw=0;
            List<Access> listaccess=userdocumenttyper.getAccesses();
            List<Role>  listarolesusuario=usuario.getRoles();
            for(Access listaaccessoperacion:listaccess)
            for(Role listarolesusuariooperacion:listarolesusuario){
            if(listaaccessoperacion.getRole().getName().equals(listarolesusuariooperacion.getName()) && !listarolesusuariooperacion.getName().equals(""))
           
            documentTypeAccesses=profiledocument.getDocumentTypeAccesses();
           for(DocumentTypeAccess list_documentTypeAccesses:documentTypeAccesses){
           if(list_documentTypeAccesses.getDocumentTypeHandle()==userdocumenttyper.getId())
                sw=1;
           }
//falta verificar acceso
            }
            if(sw==1 && userdocumenttyper.getKeywords().size()>0)
            userdocumenttypelistafinal.add(userdocumenttyper);
        }
        ArrayList<PDM_Handler> list_userdocumenttype=new ArrayList<PDM_Handler>();
        for(UserDocumentType userdocumenttypef:userdocumenttypelistafinal){
        ArrayList<PDM_Tipo> list_pdm_tipo=new ArrayList<PDM_Tipo>();
                         
        PDM_Handler obj=new PDM_Handler();
        PDM_Tipo pdm_tipo=new PDM_Tipo();
                         
        obj.setId(userdocumenttypef.getBusinessLine().getId());
        obj.setArea(userdocumenttypef.getBusinessLine().getDescription());      
                        
        pdm_tipo.setId(userdocumenttypef.getId());
        pdm_tipo.setTipo(userdocumenttypef.getName());        
        
        //Listado de Keyword
        ArrayList<PDM_Keyword> list_pdm_keyword=new ArrayList<PDM_Keyword>();
        for(Keyword list_Keywords:userdocumenttypef.getKeywords()){
            PDM_Keyword pdm_keyword=new PDM_Keyword();

            pdm_keyword.setId(list_Keywords.getDefinition().getId());
            pdm_keyword.setDataType(list_Keywords.getProperties().getDataType());
            pdm_keyword.setLabel(list_Keywords.getProperties().getLabel());

            list_pdm_keyword.add(pdm_keyword);
        }
        pdm_tipo.setKeyword(list_pdm_keyword);
        list_pdm_tipo.add(pdm_tipo);
        obj.setTipo(list_pdm_tipo);
                         
        //agrupar Keyword
        ArrayList<PDM_Handler> list_userdocumenttypeaux=new ArrayList<PDM_Handler>();
        Integer swAgrupar=0;
        for(PDM_Handler userdocumenttypeorden:list_userdocumenttype){

            if(userdocumenttypeorden.getId()==obj.getId()){
            userdocumenttypeorden.getTipo().add(obj.getTipo().get(0));
            swAgrupar=1;
            }

            list_userdocumenttypeaux.add(userdocumenttypeorden);
        }
        if(swAgrupar==1)
        list_userdocumenttype=list_userdocumenttypeaux;
                         
        if(swAgrupar==0)
        list_userdocumenttype.add(obj);
                    
        }
                   
               
        this.pdm_login=pdm_login;
        this.pdm_login.setUsuario(pdm_login.getUsuario());
        this.pdm_login.setPerfil(list_userdocumenttype);
        
                
        
        return true;
    }
    
}
    
