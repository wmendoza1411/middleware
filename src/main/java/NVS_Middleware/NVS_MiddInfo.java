/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_Middleware;

// Modal Front End
import NVS_ClientBackEnd.CRN_GetInfo;
// Modal Back  End
import NVS_ModalBackEnd.GetInfo.PDmGetServiceInfo;


import NVS_ModalFrontEnd.Info.PDM_Info;
import NVS_Configurator.PDM_Configurator;
import java.io.IOException;

/**
 *
 * @author wmendoza
 */
public class NVS_MiddInfo {
    
    // Modal Fron End
    PDM_Info            pdm_info;
    // Modal Back End
    PDmGetServiceInfo   pdmgetserviceinfo;
    
    PDM_Configurator    pdm_configurator;
    
    CRN_GetInfo         crn_getinfo;
    
    
    // Consume Service With Configurator 
    public boolean isOnlineBackEnd(PDM_Configurator  pdm_configurator) 
                                        throws NoSuchMethodException, SecurityException, IOException{
        try{
            this.pdm_configurator = pdm_configurator;
            pdm_info = this.pdm_configurator.getPdm_info();
            // Class Consume Services Back End
            crn_getinfo = new CRN_GetInfo() ;
            // Consume services with modal class
            if (info()) { // Class Consume the Services in the Back End
                
                            // If ok...Move Information from Modal Back End to Modal Front End
                pdmgetserviceinfo =  crn_getinfo.getPdmGetServiceInfo();
                
                // Analyza the values
                if (pdmgetserviceinfo.getActive()){ // If active
                    pdm_info.setName(pdmgetserviceinfo.getName());
                    pdm_info.setSt_version(pdmgetserviceinfo.getVersion());
                    pdm_info.setBl_statusbackend(true);
                    pdm_configurator.getPdm_info().setgeneralerror(
                                            pdm_configurator.driver_err(),"00");
                    
                    return true;
                }else{
                    pdm_info.setBl_statusbackend(false);
                    pdm_configurator.getPdm_info().setgeneralerror(
                                            pdm_configurator.driver_err(),"50");
                    return false;
                }
                
            }else{
                
                pdm_info.setBl_statusbackend(false);
                pdm_configurator.getPdm_info().setgeneralerror(
                                            pdm_configurator.driver_err(),"50");
                return false; // If operation is NOT ok...
            }
        }
        catch(Exception e){
            // Register Error
            pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
            pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
            pdm_configurator.DriverError();
            
            return false;
        }
        
    }

    public PDM_Info getPdm_info() {return pdm_info;}

    public void setPdm_info(PDM_Info pdm_info) {this.pdm_info = pdm_info;}
    
    // Consume Back End Profile Send Document
    public boolean info() throws NoSuchMethodException, SecurityException, IOException{
        
        try{
            // Call Class Back End
            
          return crn_getinfo.getService(pdm_configurator);
        }
        
        catch(Exception e){
            // Register Error
            pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
            pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
            pdm_configurator.DriverError();
            
            return false;
        }
        
    }

}
