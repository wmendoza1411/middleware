/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_Middleware;

import NVS_ClientBackEnd.CRN_GetInfo;
import NVS_ClientBackEnd.CRN_PostDocument;
import NVS_Configurator.PDM_Configurator;

import NVS_ModalBackEnd.PostDocumentNovosit.PDM_PostDocumentRequest;

import NVS_ModalFrontEnd.Autentication.PDM_Keyword;
import NVS_ModalFrontEnd.Document.PDM_Document;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.util.Base64;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

/**
 *
 * @author wmendoza
 */
public class NVS_MiddJwtServer {
    
    // Instance of modal and class Configurator
    PDM_Configurator                pdm_configurator;
    PDM_PostDocumentRequest         pdm_postdocumentRequest;
    CRN_GetInfo crn_getinfo         = new CRN_GetInfo() ;
    
    // Constructor
    public void NVS_MiddDocument(){   }
    
    
    // Consume Back End Profile Send Document
    public boolean info(PDM_Configurator pdm_configurator) 
                                                    throws NoSuchMethodException, SecurityException, IOException{
        
        // Get the Modal class Pointer from Configurator
        this.pdm_configurator       =   pdm_configurator;
        //PDM_Document pdm_document   =   this.pdm_configurator.getPdm_document();
        
        
        try{
          //crn_getinfo.getServiceRS(pdm_configurator);
          crn_getinfo.getService(pdm_configurator);
          //setPdmmgetserviceinfo
          pdm_configurator.getPdm_info().setName(pdm_configurator.getPdmmgetserviceinfo().getName());
          pdm_configurator.getPdm_info().setgeneralerror(pdm_configurator.driver_err(),"00");
         
        }
        
        catch(Exception e){
            // Register Error
            pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
            pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
            pdm_configurator.DriverError();
            
            return false;
        }
    return true;
    }

}
