/*
* Clase para el manejo de los errores
* Esta clase debe desarrollarse, por ahora solamente aceptara los valores de los 
* diferentes Catch que produscan errores.
*/
package NVS_Error;

import java.io.File;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.XMLFormatter;

/**
 *
 * @author wmendoza
 */
public class PDM_Error {
    
    String st_error         = "";
    String st_localizacion  = "";
    String st_servicio      = "";
    String st_fecha         = "";
    String st_hora          = "";
    String st_telefono      = "";

    public PDM_Error() throws SecurityException, IOException {
        
//         Se configura Logger de errorres
         ConfigLogError();
        
    }

    public String getSt_error() {
        return st_error;
    }

    public void setSt_error(String st_error) {
        this.st_error = st_error;
    }

    public String getSt_localizacion() {
        return st_localizacion;
    }

    public void setSt_localizacion(String st_localizacion) {
        this.st_localizacion = st_localizacion;
    }

    public String getSt_servicio() {
        return st_servicio;
    }

    public void setSt_servicio(String st_servicio) {
        this.st_servicio = st_servicio;
    }

    public String getSt_fecha() {
        return st_fecha;
    }

    public void setSt_fecha(String st_fecha) {
        this.st_fecha = st_fecha;
    }

    public String getSt_hora() {
        return st_hora;
    }

    public void setSt_hora(String st_hora) {
        this.st_hora = st_hora;
    }

    public String getSt_telefono() {
        return st_telefono;
    }

    public void setSt_telefono(String st_telefono) {
        this.st_telefono = st_telefono;
    }
    
    // Metodo para la captura de errores.
    public void GetError () {
        
    }
    
//    
//    // Error Confirator 
    public void ConfigLogError()
                        throws SecurityException, IOException {
        try{
        // creando manejador de archivo
        
       File carpeta_principal=new File(".");
            if(!carpeta_principal.exists()) 
                carpeta_principal.mkdir();
            
            File carpeta_principal2=new File("./log");
            if(!carpeta_principal2.exists()) 
                carpeta_principal2.mkdir();
            
        FileHandler fh = new FileHandler("./log/app%g.log",10485760,3,true);
        fh.setLevel(Level.ALL); // se captura all level error
        fh.setFormatter(new XMLFormatter()); //formatter

        // agregar el manejador de archivo al log
        Logger.global.addHandler(fh);

        // el manejador de consola se agrega automaticamente, solo
        // cambiamos el nivel de detalle a desplegar
        Logger.global.getHandlers()[0].setLevel(Level.SEVERE);

        // se establece el nivel predeterminado global
        Logger.global.setLevel(Level.INFO);
        }catch(Exception e){System.out.print("error="+e.getCause()+"la causa ="+e.getMessage()+" es loca="+e.getLocalizedMessage());}
        
    }
    
    public void LoggerError() {
        try {

            Logger.getLogger(getClass().getName()).log(
                Level.ALL, "Mensaje informativo...");

        } catch (Exception err) {

            Logger.getLogger(getClass().getName()).log(
                Level.SEVERE, "Mensaje crítico...", err);

        }
    }
}

