/*
 * Funtion for driver error 
 */
package NVS_Error;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author wmendoza
 */
public class PDM_HandlerError {
    
    private String intmessage = "";

    private String usermessage = "";

    private String cod = "";

    private String sclass = "";

    private String tip = "";

    private String origen = "";
    
    
    public PDM_HandlerError(){
        
    }

    public String getIntmessage ()
    {
        return intmessage;
    }

    public void setIntmessage (String intmessage)
    {
        this.intmessage = intmessage;
    }

    public String getUsermessage ()
    {
        return usermessage;
    }

    public void setUsermessage (String usermessage)
    {
        this.usermessage = usermessage;
    }

    public String getCod ()
    {
        return cod;
    }

    public void setCod (String cod)
    {
        this.cod = cod;
    }

    public String getSclass ()
    {
        return sclass;
    }

    public void setSclass (String sclass)
    {
        this.sclass = sclass;
    }

    public String getTip ()
    {
        return tip;
    }

    public void setTip (String tip)
    {
        this.tip = tip;
    }

    public String getOrigen ()
    {
        return origen;
    }

    public void setOrigen (String origen)
    {
        this.origen = origen;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [intmessage = "+intmessage+", usermessage = "+usermessage+", cod = "+cod+", class = "+sclass+", tip = "+tip+", origen = "+origen+"]";
    }
}
   
