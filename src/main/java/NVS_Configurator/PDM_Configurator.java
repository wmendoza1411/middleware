/*
 * Clase encargada de mantener la configuración general del servicio
 * Se instancian las clases que aplican en el servicio 
 * cualquier valor general del servicio se incluye o modifica en esta clase
 */


package NVS_Configurator;

// Clases que soportan los recursos del  Servicio de Autenticación
import NVS_ClientBackEnd.CRN_GetInfo;
import NVS_ModalFrontEnd.Autentication.PDM_Join;
import NVS_ModalFrontEnd.Autentication.PDM_Login;
import NVS_ModalFrontEnd.Autentication.PDM_Forget;
import NVS_ModalFrontEnd.Autentication.PDM_Autentication;

// Clase que configura el cliente rest para el consumo de los servicios
// de ProDoctivity.
import NVS_ClientBackEnd.CRN_UserProfile;
import NVS_ClientBackEnd.CRN_UserJointNovosit;
import NVS_ClientBackEnd.CRN_UserForgetNovosit;
import NVS_ClientBackEnd.CRN_PostDocument;
import NVS_ClientBackEnd.CRN_GenerationExample;
import NVS_ClientBackEnd.CRN_ProfileDocument;
import NVS_ClientBackEnd.CRN_UserDocumentTypesProfile;

// Clase de generacion y validación del token de seguridad.
import NVS_ModalFrontEnd.TokenDriver.PDM_JWT;

// Clase de manejo de errores, el cofigurador hara el manejo de los errores
// de manera que se aprovechara su apuntador para dicha tarea
import NVS_Error.PDM_Error;
import NVS_Error.PDM_HandlerError;
import NVS_ModalFrontEnd.Autentication.PDM_Handler;
import NVS_ModalFrontEnd.Document.PDM_Document;
import NVS_ModalFrontEnd.DocumentNovosit.PDM_Documents_Request;
import NVS_ModalFrontEnd.Info.PDM_Info;
import NVS_ModalFrontEnd.ContextDefinitionNovositi.PDM_ContextDefinition;
import NVS_ModalFrontEnd.DocHandleNovosit.PDM_GetDocument;
import NVS_ModalFrontEnd.GenerationExampleNovositi.PDM_GenerationExample;
import NVS_ModalFrontEnd.searchdocumentsrequestNovosit.PDM_Searchdocumentsrequest;

// Modal Back End
import NVS_ModalBackEnd.PostDocumentNovosit.PDM_PostDocumentRequest;
import NVS_ModalBackEnd.ContextDefinition.ContextDefinition;
import NVS_ModalBackEnd.DocHandleNovosit.ResponseGetDocument;
import NVS_ModalBackEnd.DocumentAccessNovosit.DocumentAccess;
import NVS_ModalBackEnd.GenerationExample.GenerationExample;
import NVS_ModalBackEnd.GetInfo.PDmGetServiceInfo;
import NVS_ModalFrondEnd.DocumentAccessNovosit.PDM_DocumentAccess;

// Clases utilitarias de consumo general en el servicio.
import io.jsonwebtoken.*;
import io.jsonwebtoken.impl.crypto.MacProvider;
import java.security.Key;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

// Clases para la Serializacion / Deserializacion 
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

// Reflexion
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.*;
import java.net.HttpURLConnection;
import NVS_Configurator.PDM_ParameterModal;

import NVS_Middleware.NVS_MiddJwtServer;
import NVS_Middleware.NVS_MiddAutentication;
import NVS_Middleware.NVS_MiddDocument;
import NVS_Middleware.NVS_MiddInfo;
import NVS_ModalBackEnd.LoginNovosit.Users;
import NVS_ModalBackEnd.ProfileDocument.ProfileDocument;
import NVS_ModalBackEnd.keywordNovosit.UserDocumentType;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import sun.misc.BASE64Encoder;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.RollingFileAppender;



/**
 *
 * @author wmendoza
 */
public class PDM_Configurator {
    
    Gson                    gson;
    GsonBuilder             gsonBuilder;
    
    PDM_Login               pdm_login;
    PDM_Join                pdm_join;
    PDM_Forget              pdm_forget;
    PDM_Document            pdm_document;
   

   
    
    PDM_ParameterModal      pdm_paramatermodal;
    
    PDM_Handler             pdm_handler;

    public PDM_Handler getPdm_handler() {return pdm_handler;}

    public void setPdm_handler(PDM_Handler pdm_handler) {
        this.pdm_handler = pdm_handler;
    }
    
    PDM_JWT                 pdm_jwt;
    PDM_Info                pdm_info;
    PDmGetServiceInfo       pdmmgetserviceinfo;

   
    CRN_UserProfile         pdm_userprofile;
    CRN_UserForgetNovosit   pdm_userforgetnovisit;
    CRN_UserJointNovosit    pdm_userjointnovosit;
    CRN_UserDocumentTypesProfile crn_userdocumenttypesprofile;
    CRN_ProfileDocument          crn_profiledocument;

   

    
    CRN_GetInfo             crn_getinfo;
    
    PDM_Error               pdm_error;
    PDM_HandlerError        pdm_handlererror;
    
    CRN_PostDocument        crn_postdocument;
    
    PDM_PostDocumentRequest pdm_postdocumentrequest;
    PDM_Documents_Request   pdm_document_request;
    PDM_GetDocument         getdocument;
    PDM_GetDocument         pdmgetdocument;
    PDM_ContextDefinition   pdm_contextdefinition;
    PDM_GenerationExample   pdm_generationexample;
    
    ContextDefinition       contextdefinition;
    GenerationExample       generationexample;
    Users                   User;
    List<UserDocumentType>  userdocumenttype;

   
    PDM_Searchdocumentsrequest searchdocumentsrequest;
    List<DocumentAccess>       documentaccess;
    List<PDM_DocumentAccess>   PDM_documentaccess;
    ResponseGetDocument        responsegetdocument; 
    ProfileDocument            profiledocument; 

    
 

    
    String                  key;        // Clave de Encriptacion
    
    String                  st_inline;
    String                  st_id;
    String                  st_subject;

    String                  st_error;
    String                  st_versionProDoc;
    int                     in_timeOut;
    
    
    String                  st_url;
    

    
    // Credenciales
    String                  st_credencialesInfo;
    
    // On Line Back End
    boolean                 bl_onlineProd;
    
    // User and Key appmovil
    String                  st_login_user;
    String                  st_login_key;
    
    // User and key Back End
    String                  st_login_user_be;
    String                  st_login_key_be;
    
    // Driver Error 
    String                  st_err_origen;
    String                  st_err_class;
    String                  st_err_tipo;
    String                  st_err_nro;
    String                  st_err_msg;
    String                  st_err_meg_int;
    
    String                  ls_Class_name;
    String                  ls_serversuspend;
    String                  ls_message_serversuspend;
    String                  ls_json;
    
    NVS_MiddDocument        nvs_midddocument;
    NVS_MiddJwtServer       nvs_middjwtserver;
    NVS_MiddAutentication   nvs_middautentication;
    NVS_MiddInfo            nvs_middinfo;
    
    
    String Cookies="";
    
    Logger                  logger;
    PatternLayout           layout;
    RollingFileAppender     fileAppender;

    // Constructor with  general value set 
    // 04/03/2018
    public boolean pdm_configurator() throws IOException {
        
        
        if (!findconfiguration() || 
               this.pdm_paramatermodal.getServersuspend().equals("1") ) {return false;}
        
        // Se establece la clave de encriptación 
        this.key                = this.pdm_paramatermodal.getEncripkey();
        
        //The JWT signature algorithm we will be using to sign the token
        //SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS512;
        
        this.st_id              = this.pdm_paramatermodal.getEncripid();
        this.st_subject         = this.pdm_paramatermodal.getEncripsujeto();
        
        // timeout transaction Back End
        this.in_timeOut         = Integer.valueOf(this.pdm_paramatermodal.getTimout());
        this.st_url             = this.pdm_paramatermodal.getUrl();
        
        return true;
                                            
    }

    public Users getUser() {
        return User;
    }

    public void setUser(Users User) {
        this.User = User;
    }

    public NVS_MiddAutentication getNvs_middautentication() {
        if (nvs_middautentication==null){
            nvs_middautentication = new NVS_MiddAutentication();
        }
        return nvs_middautentication;
    }

    public void setNvs_middautentication(NVS_MiddAutentication nvs_middautentication) {
        this.nvs_middautentication = nvs_middautentication;
    }

    public NVS_MiddInfo getNvs_middinfo() {
        if ( nvs_middinfo==null){
            nvs_middinfo = new NVS_MiddInfo(); 
        }
        return nvs_middinfo;
    }

    public void setNvs_middinfo(NVS_MiddInfo nvs_middinfo) {
        this.nvs_middinfo = nvs_middinfo;
    }
    
    public ResponseGetDocument getResponsegetdocument() {
        return responsegetdocument;
    }

    public void setResponsegetdocument(ResponseGetDocument responsegetdocument) {
        this.responsegetdocument = responsegetdocument;
    }
    
     public PDM_ContextDefinition getPdm_contextdefinition() {
        return pdm_contextdefinition;
    }

    public void setPdm_contextdefinition(PDM_ContextDefinition pdm_contextdefinition) {
        this.pdm_contextdefinition = pdm_contextdefinition;
    }

    public PDM_GenerationExample getPdm_generationexample() {
        return pdm_generationexample;
    }

    public void setPdm_generationexample(PDM_GenerationExample pdm_generationexample) {
        this.pdm_generationexample = pdm_generationexample;
    }

    public ContextDefinition getContextdefinition() {
        return contextdefinition;
    }

    public String getCookies() {
        return Cookies;
    }

    public void setCookies(String Cookies) {
        this.Cookies = Cookies;
    }

    public void setContextdefinition(ContextDefinition contextdefinition) {
        this.contextdefinition = contextdefinition;
    }

    public GenerationExample getGenerationexample() {
        return generationexample;
    }

    public void setGenerationexample(GenerationExample generationexample) {
        this.generationexample = generationexample;
    }
    
    public ProfileDocument getProfiledocument() {
        return profiledocument;
    }

    public void setProfiledocument(ProfileDocument profiledocument) {
        this.profiledocument = profiledocument;
    }

   
    public PDM_GetDocument getPdmgetdocument() {
        return pdmgetdocument;
    }

    public void setPdmgetdocument(PDM_GetDocument pdmgetdocument) {
        this.pdmgetdocument = pdmgetdocument;
    }
    
    public List<PDM_DocumentAccess> getPDM_documentaccess() {
        return PDM_documentaccess;
    }

    public void setPDM_documentaccess(List<PDM_DocumentAccess> PDM_documentaccess) {
        this.PDM_documentaccess = PDM_documentaccess;
    }

    public List<DocumentAccess> getDocumentaccess() {
        return documentaccess;
    }

    public void setDocumentaccess(List<DocumentAccess> documentaccess) {
        this.documentaccess = documentaccess;
    }

    // Valores generales

    public  List<UserDocumentType> getUserdocumenttype() {
        return userdocumenttype;
    }

    public void setUserdocumenttype( List<UserDocumentType> userdocumenttype) {
        this.userdocumenttype = userdocumenttype;
    }

    
    public PDM_Searchdocumentsrequest getSearchdocumentsrequest() {
        return searchdocumentsrequest;
    }

    public void setSearchdocumentsrequest(PDM_Searchdocumentsrequest searchdocumentsrequest) {
        this.searchdocumentsrequest = searchdocumentsrequest;
    }
    
    
    public NVS_MiddJwtServer getNvs_middjwtserver() {
      if (nvs_middjwtserver==null){
            nvs_middjwtserver = new NVS_MiddJwtServer();
        }
        return nvs_middjwtserver;
    }

    public void setNvs_middjwtserver(NVS_MiddJwtServer nvs_middjwtserver) {
        this.nvs_middjwtserver = nvs_middjwtserver;
    }
    
    public NVS_MiddDocument getNvs_midddocument() {
        if (nvs_midddocument==null){
            nvs_midddocument = new NVS_MiddDocument();
        }
        return nvs_midddocument;
    }

    public void setNvs_midddocument(NVS_MiddDocument nvs_midddocument) {
        this.nvs_midddocument = nvs_midddocument;
    }
    
           
    public PDM_Documents_Request getPdm_document_request() {
        if (pdm_document_request == null){
            pdm_document_request = new PDM_Documents_Request();
        }
        return pdm_document_request;
    }

    public void setPdm_document_request(PDM_Documents_Request pdm_document_request) {
        this.pdm_document_request = pdm_document_request;
    }
    

    public PDM_PostDocumentRequest getPdm_postdocumentrequest() {
        if (pdm_postdocumentrequest==null){
            pdm_postdocumentrequest = new PDM_PostDocumentRequest();
        }
        return pdm_postdocumentrequest;
    }

    public void setPdm_postdocumentrequest(PDM_PostDocumentRequest pdm_postdocumentrequest) {
        this.pdm_postdocumentrequest = pdm_postdocumentrequest;
    }
    
    

    public CRN_PostDocument getCrn_postdocument() {
        if (crn_postdocument==null){
            crn_postdocument = new CRN_PostDocument();
        }
        return crn_postdocument;
    }

    public void setCrn_postdocument(CRN_PostDocument crn_postdocument) {
        this.crn_postdocument = crn_postdocument;
    }
    
    
  
    public Gson getGson() {
        if (this.gson == null) {this.gson = new Gson ();}return gson;}

    public void setGson(Gson gson) {this.gson = gson;}

    public GsonBuilder getGsonBuilder() {
        if (this.gsonBuilder == null) {this.gsonBuilder = new GsonBuilder();}
        return gsonBuilder;
    }

    public void setGsonBuilder(GsonBuilder gsonBuilder) {this.gsonBuilder = gsonBuilder;}

    // Set Get Clases servicio Autenticación
    public PDM_Login getPdm_login() {
        if (this.pdm_login == null){this.pdm_login = new PDM_Login();}
        return pdm_login;
    }

    public void setPdm_login(PDM_Login pdm_login) {this.pdm_login = pdm_login;}

    public PDM_Join getPdm_join() {
        if (this.pdm_join == null){
            this.pdm_join = new PDM_Join();
        }
        return pdm_join;
    }

    public CRN_GetInfo getCrn_getinfo() {
        if (this.crn_getinfo == null) {
            crn_getinfo = new CRN_GetInfo();
        }
        return crn_getinfo;
    }

    public void setCrn_getinfo(CRN_GetInfo crn_getinfo) {
        this.crn_getinfo = crn_getinfo;
    }

    public void setPdm_join(PDM_Join pdm_join) {this.pdm_join = pdm_join;}

    public PDM_Forget getPdm_forget() {
        if (this.pdm_forget == null){
            this.pdm_forget = new PDM_Forget();
        }
        return pdm_forget;
    }

    public void setPdm_forget(PDM_Forget pdm_forget) {this.pdm_forget = pdm_forget;}

    // Se instancia los valores generales de la clase de seguridad
    public PDM_JWT getPdm_jwt() {
        if (pdm_jwt==null) {   
            this.pdm_jwt = new PDM_JWT();
        }
        return pdm_jwt;
    }

    public void setPdm_jwt(PDM_JWT pdm_jwt) {this.pdm_jwt = pdm_jwt;}

    public String getKey() {
        
        key = this.pdm_paramatermodal.getEncripkey();
        return key;}

    public void setKey(String key) {this.key = key;}
    
    public String getSt_inline() {return st_inline;}

    public void setSt_inline(String st_inline) {this.st_inline = st_inline;}

    public String getSt_id() {
        st_id = this.pdm_paramatermodal.getEncripid();
        return st_id;}

    public void setSt_id(String st_id) {this.st_id = st_id;}

    public String getSt_subject() {
        st_subject = this.pdm_paramatermodal.getEncripsujeto();
        return st_subject;}

    public void setSt_subject(String st_subject) {this.st_subject = st_subject;}

//    public void setSt_time2(String st_time2) {this.st_time2 = st_time2;}
    
     public PDM_Document getPdm_document() {
        return pdm_document;
    }

    public void setPdm_document(PDM_Document pdm_document) {
        this.pdm_document = pdm_document;
    }
    
       public PDM_GetDocument getGetdocument() {
        return getdocument;
    }

    public void setGetdocument(PDM_GetDocument getdocument) {
        this.getdocument = getdocument;
    }

    public CRN_UserProfile getPdm_userprofile() {
        if (pdm_userprofile == null){
            pdm_userprofile = new CRN_UserProfile();}
        return pdm_userprofile;}

    public void setPdm_userprofile(CRN_UserProfile pdm_userprofile) {
        this.pdm_userprofile = pdm_userprofile;
    }

    public PDM_Error getPdm_error() throws SecurityException, IOException {
        if (this.pdm_error == null){
            this.pdm_error = new PDM_Error();
        }
        return pdm_error;
    }

    public void setPdm_error(PDM_Error pdm_error) {this.pdm_error = pdm_error;}
    
    
    public CRN_UserForgetNovosit getPdm_userforgetnovisit() {
        if (pdm_userforgetnovisit == null){
            pdm_userforgetnovisit = new CRN_UserForgetNovosit();
        }
        return pdm_userforgetnovisit;
    }

    public void setPdm_userforgetnovisit(CRN_UserForgetNovosit pdm_userforgetnovisit) {
        this.pdm_userforgetnovisit = pdm_userforgetnovisit;
    }

    public CRN_UserDocumentTypesProfile getCrn_userdocumenttypesprofile() {
         if (crn_userdocumenttypesprofile == null){
            crn_userdocumenttypesprofile = new CRN_UserDocumentTypesProfile();
        }
        return crn_userdocumenttypesprofile;
    }

    public void setCrn_userdocumenttypesprofile(CRN_UserDocumentTypesProfile crn_userdocumenttypesprofile) {
        this.crn_userdocumenttypesprofile = crn_userdocumenttypesprofile;
    }
    
     public CRN_ProfileDocument getCrn_profiledocument() {
         if (this.crn_profiledocument == null){
            this.crn_profiledocument = new CRN_ProfileDocument();
        }
         
        return crn_profiledocument;
    }

    public void setCrn_profiledocument(CRN_ProfileDocument crn_profiledocument) {
        this.crn_profiledocument = crn_profiledocument;
    }
    
    

    public CRN_UserJointNovosit getPdm_userjointnovosit() {
        if (pdm_userjointnovosit == null){
            pdm_userjointnovosit = new CRN_UserJointNovosit();
        }
        return pdm_userjointnovosit;
    }

    public void setPdm_userjointnovosit(CRN_UserJointNovosit pdm_userjointnovosit) {
        this.pdm_userjointnovosit = pdm_userjointnovosit;
    }

    public String getSt_url() {
        return st_url;
    }

    public void setSt_url(String st_url) {
        this.st_url = st_url;
    }


    public String getSt_error() {
        if (st_error==null){st_error="";}
        return st_error;
    }

    public void setSt_error(String st_error) {
        this.st_error = st_error;
    }

    public boolean isBl_onlineProd() {
        return bl_onlineProd;
    }

    public void setBl_onlineProd(boolean bl_onlineProd) {
        this.bl_onlineProd = bl_onlineProd;
    }

    public String getSt_versionProDoc() {
        if (st_versionProDoc==null){st_versionProDoc = "";}
        return st_versionProDoc;
    }

    public void setSt_versionProDoc(String st_versionProDoc) {
        this.st_versionProDoc = st_versionProDoc;
    }

    public int getIn_timeOut() {
        in_timeOut = Integer.valueOf(this.pdm_paramatermodal.getTimout());
        return in_timeOut;}

    public void setIn_timeOut(int in_timeOut) {this.in_timeOut = in_timeOut;}

    public PDM_Info getPdm_info() {
        if (pdm_info == null){pdm_info = new PDM_Info();}
        return pdm_info;
    }

    public void setPdm_info(PDM_Info pdm_info) {
        this.pdm_info = pdm_info;
    }
 public PDmGetServiceInfo getPdmmgetserviceinfo() {
      if (pdmmgetserviceinfo == null){pdmmgetserviceinfo = new PDmGetServiceInfo();}
        return pdmmgetserviceinfo;
    }

    public void setPdmmgetserviceinfo(PDmGetServiceInfo pdmmgetserviceinfo) {
        this.pdmmgetserviceinfo = pdmmgetserviceinfo;
    }
    public String getSt_login_user() {
        if (st_login_user==null){st_login_user ="";}
        return st_login_user;
    }

    public void setSt_login_user(String st_login_user) {
        this.st_login_user = st_login_user;
    }

    public String getSt_login_key() {
        if (st_login_key==null) {st_login_key="";}return st_login_key;}

    public void setSt_login_key(String st_login_key) {
        this.st_login_key = st_login_key;}

    public String getSt_login_user_be() {
        if (st_login_user_be == null) { st_login_user_be="";};
        return this.st_login_user_be;
    }

    public void setSt_login_user_be(String st_login_user_be) {
        this.st_login_user_be = st_login_user_be;
    }

    public String getSt_login_key_be() {
        if(st_login_key_be==null){st_login_key_be="";}
        return st_login_key_be;
    }

    public void setSt_login_key_be(String st_login_key_be) {
        this.st_login_key_be = st_login_key_be;
    }
    
    // Driver Error..... (2) When there is NOT error in the transaction
    public PDM_HandlerError driver_err(){
        
            if (pdm_handlererror == null){
                pdm_handlererror = new PDM_HandlerError();}
            
            
            pdm_handlererror.setOrigen("");
            pdm_handlererror.setCod("");
            pdm_handlererror.setSclass("");
            pdm_handlererror.setTip("");
            pdm_handlererror.setUsermessage("");
            pdm_handlererror.setIntmessage("");

            return pdm_handlererror;
        
    }
    
    // Driver Error..... (1) When there is error in the transaction
    public PDM_HandlerError driver_err(
                                String par_origen,
                                    String par_clase,
                                        String par_tipo,
                                            String par_codigo,
                                                String par_message_user,
                                                    String par_message_interno)
                     throws NoSuchMethodException, 
                                IllegalAccessException, 
                                    IllegalArgumentException,
                                        InvocationTargetException, 
                                            ClassNotFoundException, 
                                                InstantiationException 
                                                                                            {
        try {
            
            if (pdm_handlererror == null){pdm_handlererror = new PDM_HandlerError();}
            
            pdm_handlererror.setOrigen(par_origen==null?"":par_origen);
            pdm_handlererror.setCod(par_codigo==null?"":par_codigo);
            pdm_handlererror.setSclass(par_clase==null?"":par_clase);
            pdm_handlererror.setTip(par_tipo==null?"":par_tipo);
            pdm_handlererror.setUsermessage(par_message_user==null?"":par_message_user);
            pdm_handlererror.setIntmessage(par_message_interno==null?"":par_message_interno);
            
            String ls_loca = "";
            
            ls_loca =  "Error de Aplicaitivo " + par_origen + " " +
                                    par_clase  + " " +
                                        par_tipo  + " " +
                                            par_codigo  + " " +
                                                par_message_user  + " " +
                                                    par_message_interno;
            
            
            
            DriverError(ls_loca);
            
            
            return pdm_handlererror;
        
        }
        
        catch (Exception e) { 
            
            return pdm_handlererror; }
    }
    
    public PDM_HandlerError getPdm_handlererror() {
        return pdm_handlererror==null?pdm_handlererror = new PDM_HandlerError():pdm_handlererror;}

    public void setPdm_handlererror(PDM_HandlerError pdm_handlererror) {
        this.pdm_handlererror = pdm_handlererror;}
    
    //  Request Manager Data Services
    public boolean findconfiguration() throws MalformedURLException, IOException{
        
        try{
            String uri = "http://66.45.236.108:9090/Config/parameter/configurator";
            URL url = new URL(uri);

            BASE64Encoder enc = new sun.misc.BASE64Encoder();
            String  basicAuth;
            basicAuth = "Basic " + enc.encode("Aladdin:open sesame".getBytes());

            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty ("authorization", basicAuth);

            conn.setRequestProperty("Content-Type", "application/json");
           
            if (conn.getResponseCode() == 200) {
            
               BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
               String inputLine;
               StringBuffer response = new StringBuffer();    
               while ((inputLine = in.readLine()) != null) {response.append(inputLine);}
               in.close();
               this.pdm_paramatermodal = gson.fromJson(response.toString(), PDM_ParameterModal.class);
               
               setparameter(response.toString());
                    
               return true;
                
            }else{
                
                if (!getparameter()){
                    pdm_paramatermodal = new PDM_ParameterModal();
                    this.pdm_paramatermodal.
                            setMensajeserversuspend("Server suspended until new notification");
                    return false;
                }else{
                    this.pdm_paramatermodal = gson.fromJson(this.ls_json, PDM_ParameterModal.class);
                    if (this.pdm_paramatermodal.getFailover().equals("1")){
                        return true;
                    }
                }
                return false;
            }
        }
        
        catch (MalformedURLException e) {
            String basicAuth = e.getMessage();
            System.out.println(basicAuth);
            return false;
        } 
        
        catch (IOException e) {
            String basicAuth = e.getMessage();
            System.out.println(basicAuth);
            return false;
        } 
        
        
        catch (Exception e) {
            String basicAuth = e.getMessage();
            System.out.println(basicAuth);
            return false;
        }   

    }
    
    // Get all parameter
    public boolean getparameter() throws IOException, ClassNotFoundException{
        
        try {
            FileInputStream input       = new  FileInputStream("PDM_Manager.txt");
            ObjectInputStream ois       = new ObjectInputStream(input);
            ls_json                     = (String) ois.readObject();
            return true;
        }
        catch(IOException e){return false;}
        catch(ClassNotFoundException e){return false;}
    }
            
    public boolean setparameter (String par_Json) throws FileNotFoundException, IOException {
        try{
            FileOutputStream fileoutput = new FileOutputStream("PDM_Manager.txt");
            ObjectOutputStream objectoutput = new ObjectOutputStream(fileoutput);
            ls_json = par_Json;
            objectoutput.writeObject(ls_json);
            return true;
        }
        
        catch(FileNotFoundException e){return false;}
        catch(IOException e){return false;}
    }

    public PDM_ParameterModal getPdm_paramatermodal() {
        return pdm_paramatermodal;
    }

    public void setPdm_paramatermodal(PDM_ParameterModal pdm_paramatermodal) {
        this.pdm_paramatermodal = pdm_paramatermodal;
    }
    
    public String getLs_serversuspend() {
        return ls_serversuspend;
    }

    public void setLs_serversuspend(String ls_serversuspend) {
        this.ls_serversuspend = ls_serversuspend;
    }

    public String getLs_message_serversuspend() {
        return ls_message_serversuspend;
    }

    public void setLs_message_serversuspend(String ls_message_serversuspend) {
        this.ls_message_serversuspend = ls_message_serversuspend;
    }
    

    // Metodo genera el Log de Errores
    public void DriverError () throws SecurityException, IOException {
        
        try{
            // creates pattern layout
            PatternLayout layout = new PatternLayout();
            String conversionPattern = "%-7p %d [%t] %c %x - %m%n";
            layout.setConversionPattern(conversionPattern);

            // creates console appender
            ConsoleAppender consoleAppender = new ConsoleAppender();
            consoleAppender.setLayout(layout);
            consoleAppender.activateOptions();

            // creates file appender
            FileAppender fileAppender = new FileAppender();
            
            fileAppender.setFile(getPdm_paramatermodal().getPathlog());
            fileAppender.setAppend(true);
            fileAppender.setLayout(layout);
            fileAppender.activateOptions();

            // configures the root logger
            Logger rootLogger = Logger.getRootLogger();
            rootLogger.setLevel(Level.DEBUG);
            rootLogger.addAppender(consoleAppender);
            rootLogger.addAppender(fileAppender);

            // creates a custom logger and log messages
            Logger logger = Logger.getLogger(PDM_Configurator.class);
            logger.debug(">>> " + this.getPdm_error().getSt_error() + "  " +
                                    this.getPdm_error().getSt_localizacion());
        }
        catch(Exception e){
            
            System.out.println(">>> " + this.getPdm_error().getSt_error() + "  " +
                                    this.getPdm_error().getSt_localizacion());
            
        }
        
    }
    
    public void DriverError (String lsString) throws SecurityException, IOException {
        
        try{
            // creates pattern layout
            PatternLayout layout = new PatternLayout();
            String conversionPattern = "%-7p %d [%t] %c %x - %m%n";
            layout.setConversionPattern(conversionPattern);

            // creates console appender
            ConsoleAppender consoleAppender = new ConsoleAppender();
            consoleAppender.setLayout(layout);
            consoleAppender.activateOptions();

            // creates file appender
            FileAppender fileAppender = new FileAppender();
            fileAppender.setFile(getPdm_paramatermodal().getPathlog());
            
           
            fileAppender.setAppend(true);
            fileAppender.setLayout(layout);
            fileAppender.activateOptions();

            // configures the root logger
            Logger rootLogger = Logger.getRootLogger();
            rootLogger.setLevel(Level.DEBUG);
            rootLogger.addAppender(consoleAppender);
            rootLogger.addAppender(fileAppender);

            // creates a custom logger and log messages
            Logger logger = Logger.getLogger(PDM_Configurator.class);
            logger.debug(">>> " + lsString );
        }
        catch(Exception e){
            
            System.out.println(">>> " + lsString);
            
        }
       
    }
    
    
}

