/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_Configurator;

/**
 *
 * @author wmendoza
 */
public class PDM_ParameterModal {
    
    
    private String backendpar01;
    private String backendpar01value;
    private String backendpar02;
    private String backendpar02value;
    private String backendpar03;
    private String backendpar03value;
    private String autenticationuser;
    private String autenticationkey;
    
    private String fechahoracambio;
    
    private String encripkey;
    
    private String encripsujeto;
    
    private String encripid;
    
    private String encripgetbyte;
    
    private String nombrebackend;
    
    private String timout;
    
    private String url;
    
    private String url02;
    
    private String nrobackend;
    
    private String asincronico;
    
    private String reintentos;
    
    private String emailnotification;
     
    private String horarecuperacion; 
    
    private String timescheduler;
    
    private String mensaje;
    
    private String version;
    
    private String organizacion;
    
    private String db_conection;
    
    private String db_user;
    
    private String db_key;
    
    private String serversuspend;
    
    private String mensajeserversuspend;
    
    private String agente;
    
    private String timedurationtoken;
    
    private String manager;
    
    private String failover;
    
    private String ftpdocurl;

    private String ftpdocuser;

    private String ftpdockey;
    
    private String pathlog;
    
    
    public String getPathlog() {
        return pathlog;
    }

    public void setPathlog(String pathlog) {
        this.pathlog = pathlog;
    }
    
    public String getUrl02() {
        return url02;
    }

    public void setUrl02(String url02) {
        this.url02 = url02;
    }
    
    public String getAutenticationkey ()
    {
        return autenticationkey;
    }

    public void setAutenticationkey (String autenticationkey)
    {
        this.autenticationkey = autenticationkey;
    }

    public String getFechahoracambio ()
    {
        return fechahoracambio;
    }

    public void setFechahoracambio (String fechahoracambio)
    {
        this.fechahoracambio = fechahoracambio;
    }

    public String getEncripkey ()
    {
        return encripkey;
    }

    public void setEncripkey (String encripkey)
    {
        this.encripkey = encripkey;
    }

    public String getNombrebackend ()
    {
        return nombrebackend;
    }

    public void setNombrebackend (String nombrebackend)
    {
        this.nombrebackend = nombrebackend;
    }

    public String getTimout ()
    {
        return timout;
    }

    public void setTimout (String timout)
    {
        this.timout = timout;
    }

    public String getUrl ()
    {
        return url;
    }

    public void setUrl (String url)
    {
        this.url = url;
    }

    public String getNrobackend ()
    {
        return nrobackend;
    }

    public void setNrobackend (String nrobackend)
    {
        this.nrobackend = nrobackend;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getAsincronico() {
        return asincronico;
    }

    public void setAsincronico(String asincronico) {
        this.asincronico = asincronico;
    }

    public String getReintentos() {
        return reintentos;
    }

    public void setReintentos(String reintentos) {
        this.reintentos = reintentos;
    }

    public String getHorarecuperacion() {
        return horarecuperacion;
    }

    public void setHorarecuperacion(String horarecuperacion) {
        this.horarecuperacion = horarecuperacion;
    }

    public String getTimescheduler() {
        return timescheduler;
    }

    public void setTimescheduler(String timescheduler) {
        this.timescheduler = timescheduler;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getOrganizacion() {
        return organizacion;
    }

    public void setOrganizacion(String organizacion) {
        this.organizacion = organizacion;
    }

    public String getDb_conection() {
        return db_conection;
    }

    public void setDb_conection(String db_conection) {
        this.db_conection = db_conection;
    }

    public String getDb_user() {
        return db_user;
    }

    public void setDb_user(String db_user) {
        this.db_user = db_user;
    }

    public String getDb_key() {
        return db_key;
    }

    public void setDb_key(String db_key) {
        this.db_key = db_key;
    }

    public String getServersuspend() {
        return serversuspend;
    }

    public void setServersuspend(String serversuspend) {
        this.serversuspend = serversuspend;
    }

    public String getAgente() {
        return agente;
    }

    public void setAgente(String agente) {
        this.agente = agente;
    }

    public String getMensajeserversuspend() {
        return mensajeserversuspend;
    }

    public void setMensajeserversuspend(String mensajeserversuspend) {
        this.mensajeserversuspend = mensajeserversuspend;
    }

    public String getTimedurationtoken() {
        return timedurationtoken;
    }

    public void setTimedurationtoken(String timedurationtoken) {
        this.timedurationtoken = timedurationtoken;
    }

    public String getEncripsujeto() {
        return encripsujeto;
    }

    public void setEncripsujeto(String encripsujeto) {
        this.encripsujeto = encripsujeto;
    }

    public String getEncripid() {
        return encripid;
    }

    public void setEncripid(String encripid) {
        this.encripid = encripid;
    }

    public String getBackendpar01() {
        return backendpar01;
    }

    public void setBackendpar01(String backendpar01) {
        this.backendpar01 = backendpar01;
    }

    public String getBackendpar01value() {
        return backendpar01value;
    }

    public void setBackendpar01value(String backendpar01value) {
        this.backendpar01value = backendpar01value;
    }

    public String getBackendpar02() {
        return backendpar02;
    }

    public void setBackendpar02(String backendpar02) {
        this.backendpar02 = backendpar02;
    }

    public String getBackendpar02value() {
        return backendpar02value;
    }

    public void setBackendpar02value(String backendpar02value) {
        this.backendpar02value = backendpar02value;
    }

    public String getBackendpar03() {
        return backendpar03;
    }

    public void setBackendpar03(String backendpar03) {
        this.backendpar03 = backendpar03;
    }

    public String getBackendpar03value() {
        return backendpar03value;
    }

    public void setBackendpar03value(String backendpar03value) {
        this.backendpar03value = backendpar03value;
    }

    public String getAutenticationuser() {
        return autenticationuser;
    }

    public void setAutenticationuser(String autenticationuser) {
        this.autenticationuser = autenticationuser;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public String getFailover() {
        return failover;
    }

    public void setFailover(String failover) {
        this.failover = failover;
    }

    public String getEmailnotification() {
        return emailnotification;
    }

    public void setEmailnotification(String emailnotification) {
        this.emailnotification = emailnotification;
    }

    public String getEncripgetbyte() {
        return encripgetbyte;
    }

    public void setEncripgetbyte(String encripgetbyte) {
        this.encripgetbyte = encripgetbyte;
    }

    public String getFtpdocurl() {
        return ftpdocurl;
    }

    public void setFtpdocurl(String ftpdocurl) {
        this.ftpdocurl = ftpdocurl;
    }

    public String getFtpdocuser() {
        return ftpdocuser;
    }

    public void setFtpdocuser(String ftpdocuser) {
        this.ftpdocuser = ftpdocuser;
    }

    public String getFtpdockey() {
        return ftpdockey;
    }

    public void setFtpdockey(String ftpdockey) {
        this.ftpdockey = ftpdockey;
    }
    
    
    
    public void reset(){
        
        // FTP Document
        this.setFtpdockey("");
        this.setFtpdocurl("");
        this.setFtpdocuser("");
        
        // Cod. Encriptation
        this.setEncripgetbyte("");
        
        // email Notification
        this.setEmailnotification("");
        
        // Failover
        this.setFailover("");
        
        this.setManager("");
        
        // URL Version BanckEnd
        this.setVersion("");
        // Organization Contract Services
        this.setOrganizacion("");
        
        // Back End URL
        this.setUrl("");
        // Back End Conection key
        this.setAutenticationkey("");
        // Back End Conection key
        this.setAutenticationuser("");
        
        // Back End Parameter
        this.setBackendpar01("");
        this.setBackendpar01value("");
        this.setBackendpar02("");
        this.setBackendpar02value("");
        this.setBackendpar03("");
        this.setBackendpar03value("");
        
        // Encriptation KEY JWT App Movil
        this.setEncripkey("");
        // Encriptation ID JWT App Movil
        this.setEncripid("");
        // Encriptation Sujeto JWT App Movil
        this.setEncripsujeto("");
        
        // Back End Name
        this.setNombrebackend("");
        // Update this Manager (Time Date)
        this.setFechahoracambio("");
        // In case had every Back End
        this.setNrobackend("");
        // TimeOut for Request Back End
        this.setTimout("");
        // token time duration
        this.setTimedurationtoken("");
        
        this.setMensaje("");
        // The Middleare had 2 operating Way 1 Asincrinic 0 Sincronic
        this.setAsincronico("");
        // Attempt to send transactions
        this.setReintentos("");
        
        // Email notification sending
        this.setEmailnotification("");
        
        //
        this.setHorarecuperacion("");
        // Interval time for the run cheduler
        this.setTimescheduler("");
        // DB Conection String  
        this.setDb_conection("");
        // DB User Key
        this.setDb_key("");
        // DB Usr Login
        this.setDb_user("");
        // Indicator for accept or not transactions
        this.setServersuspend("");
        // Name for the URL requirements 
        this.setAgente("");
        // Message Suspend Services
        this.setMensajeserversuspend("");
        
    }

    @Override
    public String toString()
    {
        return "ClassPojo [autenticationkey = "+autenticationkey+", fechahoracambio = "+fechahoracambio+", encripkey = "+encripkey+", nombrebackend = "+nombrebackend+", timout = "+timout+", url = "+url+", nrobackend = "+nrobackend+"]";
    }
    
}
