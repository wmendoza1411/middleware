/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ModalBackEnd.LoginNovosit;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Role {

@SerializedName("roleId")
@Expose
private Integer roleId;
@SerializedName("name")
@Expose
private String name;
@SerializedName("active")
@Expose
private Boolean active;
@SerializedName("activeDirectoryIntegration")
@Expose
private Boolean activeDirectoryIntegration;
@SerializedName("usersCount")
@Expose
private Integer usersCount;
@SerializedName("documentTypeAccesses")
@Expose
private List<Object> documentTypeAccesses = null;
@SerializedName("documentTypeAccessesCount")
@Expose
private Integer documentTypeAccessesCount;

public Integer getRoleId() {
return roleId;
}

public void setRoleId(Integer roleId) {
this.roleId = roleId;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public Boolean getActive() {
return active;
}

public void setActive(Boolean active) {
this.active = active;
}

public Boolean getActiveDirectoryIntegration() {
return activeDirectoryIntegration;
}

public void setActiveDirectoryIntegration(Boolean activeDirectoryIntegration) {
this.activeDirectoryIntegration = activeDirectoryIntegration;
}

public Integer getUsersCount() {
return usersCount;
}

public void setUsersCount(Integer usersCount) {
this.usersCount = usersCount;
}

public List<Object> getDocumentTypeAccesses() {
return documentTypeAccesses;
}

public void setDocumentTypeAccesses(List<Object> documentTypeAccesses) {
this.documentTypeAccesses = documentTypeAccesses;
}

public Integer getDocumentTypeAccessesCount() {
return documentTypeAccessesCount;
}

public void setDocumentTypeAccessesCount(Integer documentTypeAccessesCount) {
this.documentTypeAccessesCount = documentTypeAccessesCount;
}

}