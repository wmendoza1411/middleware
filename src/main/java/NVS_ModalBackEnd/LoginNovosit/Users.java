/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ModalBackEnd.LoginNovosit;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Users {

@SerializedName("userId")
@Expose
private Integer userId;
@SerializedName("userName")
@Expose
private String userName;
@SerializedName("name")
@Expose
private String name;
@SerializedName("lastName")
@Expose
private String lastName;
@SerializedName("identificationType")
@Expose
private String identificationType;
@SerializedName("identification")
@Expose
private String identification;
@SerializedName("position")
@Expose
private String position;
@SerializedName("email")
@Expose
private String email;
@SerializedName("phone")
@Expose
private String phone;
@SerializedName("phone2")
@Expose
private String phone2;
@SerializedName("validityPeriodType")
@Expose
private String validityPeriodType;
@SerializedName("hasValidityPeriod")
@Expose
private Boolean hasValidityPeriod;
@SerializedName("officeId")
@Expose
private Integer officeId;
@SerializedName("hasAllOffices")
@Expose
private Boolean hasAllOffices;
@SerializedName("hasActiveDirectoryIntegration")
@Expose
private Boolean hasActiveDirectoryIntegration;
@SerializedName("active")
@Expose
private Boolean active;
@SerializedName("roles")
@Expose
private List<Role> roles = null;
@SerializedName("hasSpecificStation")
@Expose
private Boolean hasSpecificStation;
@SerializedName("hasSpecificSchedule")
@Expose
private Boolean hasSpecificSchedule;
@SerializedName("lastLoginDate")
@Expose
private String lastLoginDate;
@SerializedName("startHour")
@Expose
private Integer startHour;
@SerializedName("endHour")
@Expose
private Integer endHour;
@SerializedName("properties")
@Expose
private List<Property> properties = null;
@SerializedName("stations")
@Expose
private List<Object> stations = null;
@SerializedName("locale")
@Expose
private String locale;
@SerializedName("company")
@Expose
private String company;

public Integer getUserId() {
return userId;
}

public void setUserId(Integer userId) {
this.userId = userId;
}

public String getUserName() {
return userName;
}

public void setUserName(String userName) {
this.userName = userName;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public String getLastName() {
return lastName;
}

public void setLastName(String lastName) {
this.lastName = lastName;
}

public String getIdentificationType() {
return identificationType;
}

public void setIdentificationType(String identificationType) {
this.identificationType = identificationType;
}

public String getIdentification() {
return identification;
}

public void setIdentification(String identification) {
this.identification = identification;
}

public String getPosition() {
return position;
}

public void setPosition(String position) {
this.position = position;
}

public String getEmail() {
return email;
}

public void setEmail(String email) {
this.email = email;
}

public String getPhone() {
return phone;
}

public void setPhone(String phone) {
this.phone = phone;
}

public String getPhone2() {
return phone2;
}

public void setPhone2(String phone2) {
this.phone2 = phone2;
}

public String getValidityPeriodType() {
return validityPeriodType;
}

public void setValidityPeriodType(String validityPeriodType) {
this.validityPeriodType = validityPeriodType;
}

public Boolean getHasValidityPeriod() {
return hasValidityPeriod;
}

public void setHasValidityPeriod(Boolean hasValidityPeriod) {
this.hasValidityPeriod = hasValidityPeriod;
}

public Integer getOfficeId() {
return officeId;
}

public void setOfficeId(Integer officeId) {
this.officeId = officeId;
}

public Boolean getHasAllOffices() {
return hasAllOffices;
}

public void setHasAllOffices(Boolean hasAllOffices) {
this.hasAllOffices = hasAllOffices;
}

public Boolean getHasActiveDirectoryIntegration() {
return hasActiveDirectoryIntegration;
}

public void setHasActiveDirectoryIntegration(Boolean hasActiveDirectoryIntegration) {
this.hasActiveDirectoryIntegration = hasActiveDirectoryIntegration;
}

public Boolean getActive() {
return active;
}

public void setActive(Boolean active) {
this.active = active;
}

public List<Role> getRoles() {
return roles;
}

public void setRoles(List<Role> roles) {
this.roles = roles;
}

public Boolean getHasSpecificStation() {
return hasSpecificStation;
}

public void setHasSpecificStation(Boolean hasSpecificStation) {
this.hasSpecificStation = hasSpecificStation;
}

public Boolean getHasSpecificSchedule() {
return hasSpecificSchedule;
}

public void setHasSpecificSchedule(Boolean hasSpecificSchedule) {
this.hasSpecificSchedule = hasSpecificSchedule;
}

public String getLastLoginDate() {
return lastLoginDate;
}

public void setLastLoginDate(String lastLoginDate) {
this.lastLoginDate = lastLoginDate;
}

public Integer getStartHour() {
return startHour;
}

public void setStartHour(Integer startHour) {
this.startHour = startHour;
}

public Integer getEndHour() {
return endHour;
}

public void setEndHour(Integer endHour) {
this.endHour = endHour;
}

public List<Property> getProperties() {
return properties;
}

public void setProperties(List<Property> properties) {
this.properties = properties;
}

public List<Object> getStations() {
return stations;
}

public void setStations(List<Object> stations) {
this.stations = stations;
}

public String getLocale() {
return locale;
}

public void setLocale(String locale) {
this.locale = locale;
}

public String getCompany() {
return company;
}

public void setCompany(String company) {
this.company = company;
}

}