/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ModalBackEnd.DocHandleNovosit;

/**
 *
 * @author Capas360
 */
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseGetDocument {

@SerializedName("pagesBase64")
@Expose
private List<String> pagesBase64 = null;
@SerializedName("contentType")
@Expose
private String contentType;
@SerializedName("documentHandle")
@Expose
private Integer documentHandle;
@SerializedName("documentTypeHandle")
@Expose
private Integer documentTypeHandle;
@SerializedName("lastUpdated")
@Expose
private String lastUpdated;
@SerializedName("version")
@Expose
private Integer version;
@SerializedName("createdBy")
@Expose
private Integer createdBy;
@SerializedName("creationDate")
@Expose
private String creationDate;
@SerializedName("createdByName")
@Expose
private String createdByName;
@SerializedName("versionDate")
@Expose
private String versionDate;
@SerializedName("documentType")
@Expose
private String documentType;
@SerializedName("reference")
@Expose
private String reference;
@SerializedName("keywordData")
@Expose
private KeywordData keywordData;
@SerializedName("generationToken")
@Expose
private Object generationToken;
@SerializedName("accesses")
@Expose
private List<Object> accesses = null;
@SerializedName("templateDocumentHandle")
@Expose
private Object templateDocumentHandle;

public List<String> getPagesBase64() {
return pagesBase64;
}

public void setPagesBase64(List<String> pagesBase64) {
this.pagesBase64 = pagesBase64;
}

public String getContentType() {
return contentType;
}

public void setContentType(String contentType) {
this.contentType = contentType;
}

public Integer getDocumentHandle() {
return documentHandle;
}

public void setDocumentHandle(Integer documentHandle) {
this.documentHandle = documentHandle;
}

public Integer getDocumentTypeHandle() {
return documentTypeHandle;
}

public void setDocumentTypeHandle(Integer documentTypeHandle) {
this.documentTypeHandle = documentTypeHandle;
}

public String getLastUpdated() {
return lastUpdated;
}

public void setLastUpdated(String lastUpdated) {
this.lastUpdated = lastUpdated;
}

public Integer getVersion() {
return version;
}

public void setVersion(Integer version) {
this.version = version;
}

public Integer getCreatedBy() {
return createdBy;
}

public void setCreatedBy(Integer createdBy) {
this.createdBy = createdBy;
}

public String getCreationDate() {
return creationDate;
}

public void setCreationDate(String creationDate) {
this.creationDate = creationDate;
}

public String getCreatedByName() {
return createdByName;
}

public void setCreatedByName(String createdByName) {
this.createdByName = createdByName;
}

public String getVersionDate() {
return versionDate;
}

public void setVersionDate(String versionDate) {
this.versionDate = versionDate;
}

public String getDocumentType() {
return documentType;
}

public void setDocumentType(String documentType) {
this.documentType = documentType;
}

public String getReference() {
return reference;
}

public void setReference(String reference) {
this.reference = reference;
}

public KeywordData getKeywordData() {
return keywordData;
}

public void setKeywordData(KeywordData keywordData) {
this.keywordData = keywordData;
}

public Object getGenerationToken() {
return generationToken;
}

public void setGenerationToken(Object generationToken) {
this.generationToken = generationToken;
}

public List<Object> getAccesses() {
return accesses;
}

public void setAccesses(List<Object> accesses) {
this.accesses = accesses;
}

public Object getTemplateDocumentHandle() {
return templateDocumentHandle;
}

public void setTemplateDocumentHandle(Object templateDocumentHandle) {
this.templateDocumentHandle = templateDocumentHandle;
}

}
