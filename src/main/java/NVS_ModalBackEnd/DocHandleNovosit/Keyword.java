/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ModalBackEnd.DocHandleNovosit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Keyword {

@SerializedName("handle")
@Expose
private Integer handle;
@SerializedName("order")
@Expose
private Integer order;
@SerializedName("value")
@Expose
private String value;

public Integer getHandle() {
return handle;
}

public void setHandle(Integer handle) {
this.handle = handle;
}

public Integer getOrder() {
return order;
}

public void setOrder(Integer order) {
this.order = order;
}

public String getValue() {
return value;
}

public void setValue(String value) {
this.value = value;
}

}