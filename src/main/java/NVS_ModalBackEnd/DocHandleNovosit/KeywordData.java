/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ModalBackEnd.DocHandleNovosit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

public class KeywordData {

@SerializedName("keywordMap")
@Expose
private List<KeywordMap> keywordMap = null;
@SerializedName("keywords")
@Expose
private List<Keyword> keywords = null;
@SerializedName("recordMap")
@Expose
private List<Object> recordMap = null;
@SerializedName("records")
@Expose
private List<Object> records = null;

public List<KeywordMap> getKeywordMap() {
return keywordMap;
}

public void setKeywordMap(List<KeywordMap> keywordMap) {
this.keywordMap = keywordMap;
}

public List<Keyword> getKeywords() {
return keywords;
}

public void setKeywords(List<Keyword> keywords) {
this.keywords = keywords;
}

public List<Object> getRecordMap() {
return recordMap;
}

public void setRecordMap(List<Object> recordMap) {
this.recordMap = recordMap;
}

public List<Object> getRecords() {
return records;
}

public void setRecords(List<Object> records) {
this.records = records;
}

}