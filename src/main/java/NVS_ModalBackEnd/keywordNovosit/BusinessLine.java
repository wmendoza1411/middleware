/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ModalBackEnd.keywordNovosit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BusinessLine {

@SerializedName("id")
@Expose
private Integer id;
@SerializedName("description")
@Expose
private String description;
@SerializedName("defaultWorkflowConfigurationId")
@Expose
private Integer defaultWorkflowConfigurationId;
@SerializedName("status")
@Expose
private String status;
@SerializedName("comments")
@Expose
private String comments;
@SerializedName("type")
@Expose
private String type;

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public String getDescription() {
return description;
}

public void setDescription(String description) {
this.description = description;
}

public Integer getDefaultWorkflowConfigurationId() {
return defaultWorkflowConfigurationId;
}

public void setDefaultWorkflowConfigurationId(Integer defaultWorkflowConfigurationId) {
this.defaultWorkflowConfigurationId = defaultWorkflowConfigurationId;
}

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public String getComments() {
return comments;
}

public void setComments(String comments) {
this.comments = comments;
}

public String getType() {
return type;
}

public void setType(String type) {
this.type = type;
}

}