/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ModalBackEnd.keywordNovosit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Properties {

@SerializedName("name")
@Expose
private String name;
@SerializedName("label")
@Expose
private String label;
@SerializedName("description")
@Expose
private String description;
@SerializedName("instructions")
@Expose
private String instructions;
@SerializedName("order")
@Expose
private Integer order;
@SerializedName("visible")
@Expose
private Boolean visible;
@SerializedName("occurrences")
@Expose
private Occurrences occurrences;
@SerializedName("catalogName")
@Expose
private String catalogName;
@SerializedName("parentCatalogSource")
@Expose
private String parentCatalogSource;
@SerializedName("dataType")
@Expose
private String dataType;
@SerializedName("unique")
@Expose
private Boolean unique;
@SerializedName("lines")
@Expose
private Integer lines;
@SerializedName("isSystemDate")
@Expose
private Boolean isSystemDate;
@SerializedName("defaultValue")
@Expose
private String defaultValue;

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public String getLabel() {
return label;
}

public void setLabel(String label) {
this.label = label;
}

public String getDescription() {
return description;
}

public void setDescription(String description) {
this.description = description;
}

public String getInstructions() {
return instructions;
}

public void setInstructions(String instructions) {
this.instructions = instructions;
}

public Integer getOrder() {
return order;
}

public void setOrder(Integer order) {
this.order = order;
}

public Boolean getVisible() {
return visible;
}

public void setVisible(Boolean visible) {
this.visible = visible;
}

public Occurrences getOccurrences() {
return occurrences;
}

public void setOccurrences(Occurrences occurrences) {
this.occurrences = occurrences;
}

public String getCatalogName() {
return catalogName;
}

public void setCatalogName(String catalogName) {
this.catalogName = catalogName;
}

public String getParentCatalogSource() {
return parentCatalogSource;
}

public void setParentCatalogSource(String parentCatalogSource) {
this.parentCatalogSource = parentCatalogSource;
}

public String getDataType() {
return dataType;
}

public void setDataType(String dataType) {
this.dataType = dataType;
}

public Boolean getUnique() {
return unique;
}

public void setUnique(Boolean unique) {
this.unique = unique;
}

public Integer getLines() {
return lines;
}

public void setLines(Integer lines) {
this.lines = lines;
}

public Boolean getIsSystemDate() {
return isSystemDate;
}

public void setIsSystemDate(Boolean isSystemDate) {
this.isSystemDate = isSystemDate;
}

public String getDefaultValue() {
return defaultValue;
}

public void setDefaultValue(String defaultValue) {
this.defaultValue = defaultValue;
}

}