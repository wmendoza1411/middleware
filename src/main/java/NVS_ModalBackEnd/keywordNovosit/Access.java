/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ModalBackEnd.keywordNovosit;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Access {

@SerializedName("role")
@Expose
private Role role;
@SerializedName("accesses")
@Expose
private List<Integer> accesses = null;

public Role getRole() {
return role;
}

public void setRole(Role role) {
this.role = role;
}

public List<Integer> getAccesses() {
return accesses;
}

public void setAccesses(List<Integer> accesses) {
this.accesses = accesses;
}

}