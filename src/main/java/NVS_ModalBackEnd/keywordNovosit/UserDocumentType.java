/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ModalBackEnd.keywordNovosit;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserDocumentType {

@SerializedName("id")
@Expose
private Integer id;
@SerializedName("name")
@Expose
private String name;
@SerializedName("volume")
@Expose
private Volume volume;
@SerializedName("status")
@Expose
private String status;
@SerializedName("businessLine")
@Expose
private BusinessLine businessLine;
@SerializedName("format")
@Expose
private String format;
@SerializedName("useFullTextIndex")
@Expose
private Boolean useFullTextIndex;
@SerializedName("icon")
@Expose
private Icon icon;
@SerializedName("keywords")
@Expose
private List<Keyword> keywords = null;
@SerializedName("groups")
@Expose
private List<Object> groups = null;
@SerializedName("accesses")
@Expose
private List<Access> accesses = null;
@SerializedName("isTemplate")
@Expose
private Boolean isTemplate;

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public Volume getVolume() {
return volume;
}

public void setVolume(Volume volume) {
this.volume = volume;
}

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public BusinessLine getBusinessLine() {
return businessLine;
}

public void setBusinessLine(BusinessLine businessLine) {
this.businessLine = businessLine;
}

public String getFormat() {
return format;
}

public void setFormat(String format) {
this.format = format;
}

public Boolean getUseFullTextIndex() {
return useFullTextIndex;
}

public void setUseFullTextIndex(Boolean useFullTextIndex) {
this.useFullTextIndex = useFullTextIndex;
}

public Icon getIcon() {
return icon;
}

public void setIcon(Icon icon) {
this.icon = icon;
}

public List<Keyword> getKeywords() {
return keywords;
}

public void setKeywords(List<Keyword> keywords) {
this.keywords = keywords;
}

public List<Object> getGroups() {
return groups;
}

public void setGroups(List<Object> groups) {
this.groups = groups;
}

public List<Access> getAccesses() {
return accesses;
}

public void setAccesses(List<Access> accesses) {
this.accesses = accesses;
}

public Boolean getIsTemplate() {
return isTemplate;
}

public void setIsTemplate(Boolean isTemplate) {
this.isTemplate = isTemplate;
}

}