/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ModalBackEnd.keywordNovosit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Occurrences {

@SerializedName("minimum")
@Expose
private Integer minimum;
@SerializedName("maximum")
@Expose
private Integer maximum;

public Integer getMinimum() {
return minimum;
}

public void setMinimum(Integer minimum) {
this.minimum = minimum;
}

public Integer getMaximum() {
return maximum;
}

public void setMaximum(Integer maximum) {
this.maximum = maximum;
}

}
