/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ModalBackEnd.keywordNovosit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Volume {

@SerializedName("id")
@Expose
private Integer id;
@SerializedName("description")
@Expose
private String description;
@SerializedName("path")
@Expose
private String path;
@SerializedName("filesNamingStrategy")
@Expose
private String filesNamingStrategy;
@SerializedName("encrypted")
@Expose
private Boolean encrypted;
@SerializedName("actualZise")
@Expose
private Integer actualZise;
@SerializedName("maxFilesCount")
@Expose
private Integer maxFilesCount;
@SerializedName("filesCount")
@Expose
private Integer filesCount;
@SerializedName("maxFolderZise")
@Expose
private Integer maxFolderZise;
@SerializedName("isDefault")
@Expose
private Boolean isDefault;

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public String getDescription() {
return description;
}

public void setDescription(String description) {
this.description = description;
}

public String getPath() {
return path;
}

public void setPath(String path) {
this.path = path;
}

public String getFilesNamingStrategy() {
return filesNamingStrategy;
}

public void setFilesNamingStrategy(String filesNamingStrategy) {
this.filesNamingStrategy = filesNamingStrategy;
}

public Boolean getEncrypted() {
return encrypted;
}

public void setEncrypted(Boolean encrypted) {
this.encrypted = encrypted;
}

public Integer getActualZise() {
return actualZise;
}

public void setActualZise(Integer actualZise) {
this.actualZise = actualZise;
}

public Integer getMaxFilesCount() {
return maxFilesCount;
}

public void setMaxFilesCount(Integer maxFilesCount) {
this.maxFilesCount = maxFilesCount;
}

public Integer getFilesCount() {
return filesCount;
}

public void setFilesCount(Integer filesCount) {
this.filesCount = filesCount;
}

public Integer getMaxFolderZise() {
return maxFolderZise;
}

public void setMaxFolderZise(Integer maxFolderZise) {
this.maxFolderZise = maxFolderZise;
}

public Boolean getIsDefault() {
return isDefault;
}

public void setIsDefault(Boolean isDefault) {
this.isDefault = isDefault;
}

}