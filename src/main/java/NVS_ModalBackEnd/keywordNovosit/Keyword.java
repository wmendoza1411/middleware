/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ModalBackEnd.keywordNovosit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Keyword {

@SerializedName("documentTypeId")
@Expose
private Integer documentTypeId;
@SerializedName("definition")
@Expose
private Definition definition;
@SerializedName("properties")
@Expose
private Properties_ properties;

public Integer getDocumentTypeId() {
return documentTypeId;
}

public void setDocumentTypeId(Integer documentTypeId) {
this.documentTypeId = documentTypeId;
}

public Definition getDefinition() {
return definition;
}

public void setDefinition(Definition definition) {
this.definition = definition;
}

public Properties_ getProperties() {
return properties;
}

public void setProperties(Properties_ properties) {
this.properties = properties;
}

}