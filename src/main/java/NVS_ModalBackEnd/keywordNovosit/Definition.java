/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ModalBackEnd.keywordNovosit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Definition {

@SerializedName("id")
@Expose
private Integer id;
@SerializedName("humanName")
@Expose
private String humanName;
@SerializedName("properties")
@Expose
private Properties properties;

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public String getHumanName() {
return humanName;
}

public void setHumanName(String humanName) {
this.humanName = humanName;
}

public Properties getProperties() {
return properties;
}

public void setProperties(Properties properties) {
this.properties = properties;
}

}