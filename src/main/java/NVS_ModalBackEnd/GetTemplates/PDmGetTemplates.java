package NVS_ModalBackEnd.GetTemplates;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PDmGetTemplates {

@SerializedName("templateHandle")
@Expose
private Integer templateHandle;
@SerializedName("name")
@Expose
private String name;
@SerializedName("description")
@Expose
private String description;
@SerializedName("documentHandle")
@Expose
private Integer documentHandle;
@SerializedName("originalDocumentHandle")
@Expose
private Integer originalDocumentHandle;
@SerializedName("documentTypeHandle")
@Expose
private Integer documentTypeHandle;
@SerializedName("businessFunctionName")
@Expose
private String businessFunctionName;
@SerializedName("isPublished")
@Expose
private Boolean isPublished;
@SerializedName("date")
@Expose
private String date;
@SerializedName("generationWorkflowHandle")
@Expose
private Integer generationWorkflowHandle;
@SerializedName("configurationWorkflowHandle")
@Expose
private Integer configurationWorkflowHandle;
@SerializedName("businessFunctionHandle")
@Expose
private Integer businessFunctionHandle;
@SerializedName("businessFunctionType")
@Expose
private Integer businessFunctionType;
@SerializedName("affectedTemplateCount")
@Expose
private Integer affectedTemplateCount;
@SerializedName("binaryParts")
@Expose
private String binaryParts;
@SerializedName("icon")
@Expose
private String icon;
@SerializedName("format")
@Expose
private String format;
@SerializedName("documentsProtectionHandling")
@Expose
private DocumentsProtectionHandling documentsProtectionHandling;
@SerializedName("userId")
@Expose
private Integer userId;
@SerializedName("version")
@Expose
private Integer version;

public Integer getTemplateHandle() {
return templateHandle;
}

public void setTemplateHandle(Integer templateHandle) {
this.templateHandle = templateHandle;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public String getDescription() {
return description;
}

public void setDescription(String description) {
this.description = description;
}

public Integer getDocumentHandle() {
return documentHandle;
}

public void setDocumentHandle(Integer documentHandle) {
this.documentHandle = documentHandle;
}

public Integer getOriginalDocumentHandle() {
return originalDocumentHandle;
}

public void setOriginalDocumentHandle(Integer originalDocumentHandle) {
this.originalDocumentHandle = originalDocumentHandle;
}

public Integer getDocumentTypeHandle() {
return documentTypeHandle;
}

public void setDocumentTypeHandle(Integer documentTypeHandle) {
this.documentTypeHandle = documentTypeHandle;
}

public String getBusinessFunctionName() {
return businessFunctionName;
}

public void setBusinessFunctionName(String businessFunctionName) {
this.businessFunctionName = businessFunctionName;
}

public Boolean getIsPublished() {
return isPublished;
}

public void setIsPublished(Boolean isPublished) {
this.isPublished = isPublished;
}

public String getDate() {
return date;
}

public void setDate(String date) {
this.date = date;
}

public Integer getGenerationWorkflowHandle() {
return generationWorkflowHandle;
}

public void setGenerationWorkflowHandle(Integer generationWorkflowHandle) {
this.generationWorkflowHandle = generationWorkflowHandle;
}

public Integer getConfigurationWorkflowHandle() {
return configurationWorkflowHandle;
}

public void setConfigurationWorkflowHandle(Integer configurationWorkflowHandle) {
this.configurationWorkflowHandle = configurationWorkflowHandle;
}

public Integer getBusinessFunctionHandle() {
return businessFunctionHandle;
}

public void setBusinessFunctionHandle(Integer businessFunctionHandle) {
this.businessFunctionHandle = businessFunctionHandle;
}

public Integer getBusinessFunctionType() {
return businessFunctionType;
}

public void setBusinessFunctionType(Integer businessFunctionType) {
this.businessFunctionType = businessFunctionType;
}

public Integer getAffectedTemplateCount() {
return affectedTemplateCount;
}

public void setAffectedTemplateCount(Integer affectedTemplateCount) {
this.affectedTemplateCount = affectedTemplateCount;
}

public String getBinaryParts() {
return binaryParts;
}

public void setBinaryParts(String binaryParts) {
this.binaryParts = binaryParts;
}

public String getIcon() {
return icon;
}

public void setIcon(String icon) {
this.icon = icon;
}

public String getFormat() {
return format;
}

public void setFormat(String format) {
this.format = format;
}

public DocumentsProtectionHandling getDocumentsProtectionHandling() {
return documentsProtectionHandling;
}

public void setDocumentsProtectionHandling(DocumentsProtectionHandling documentsProtectionHandling) {
this.documentsProtectionHandling = documentsProtectionHandling;
}

public Integer getUserId() {
return userId;
}

public void setUserId(Integer userId) {
this.userId = userId;
}

public Integer getVersion() {
return version;
}

public void setVersion(Integer version) {
this.version = version;
}

}
