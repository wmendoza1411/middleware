package NVS_ModalBackEnd.PostDocument;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class KeywordData {

@SerializedName("documentHandle")
@Expose
private Integer documentHandle;
@SerializedName("originalDocumentHandle")
@Expose
private Integer originalDocumentHandle;
@SerializedName("data")
@Expose
private Data data;
@SerializedName("skipKeywords")
@Expose
private Boolean skipKeywords;

public Integer getDocumentHandle() {
return documentHandle;
}

public void setDocumentHandle(Integer documentHandle) {
this.documentHandle = documentHandle;
}

public Integer getOriginalDocumentHandle() {
return originalDocumentHandle;
}

public void setOriginalDocumentHandle(Integer originalDocumentHandle) {
this.originalDocumentHandle = originalDocumentHandle;
}

public Data getData() {
return data;
}

public void setData(Data data) {
this.data = data;
}

public Boolean getSkipKeywords() {
return skipKeywords;
}

public void setSkipKeywords(Boolean skipKeywords) {
this.skipKeywords = skipKeywords;
}

}