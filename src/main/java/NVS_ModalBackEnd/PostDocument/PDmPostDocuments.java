

package NVS_ModalBackEnd.PostDocument;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PDmPostDocuments {

@SerializedName("documentType")
@Expose
private Integer documentType;
@SerializedName("documentData")
@Expose
private List<String> documentData = null;
@SerializedName("lastVersionDocHandle")
@Expose
private Integer lastVersionDocHandle;
@SerializedName("mimeType")
@Expose
private String mimeType;
@SerializedName("keywordData")
@Expose
private KeywordData keywordData;

public Integer getDocumentType() {
return documentType;
}

public void setDocumentType(Integer documentType) {
this.documentType = documentType;
}

public List<String> getDocumentData() {
return documentData;
}

public void setDocumentData(List<String> documentData) {
this.documentData = documentData;
}

public Integer getLastVersionDocHandle() {
return lastVersionDocHandle;
}

public void setLastVersionDocHandle(Integer lastVersionDocHandle) {
this.lastVersionDocHandle = lastVersionDocHandle;
}

public String getMimeType() {
return mimeType;
}

public void setMimeType(String mimeType) {
this.mimeType = mimeType;
}

public KeywordData getKeywordData() {
return keywordData;
}

public void setKeywordData(KeywordData keywordData) {
this.keywordData = keywordData;
}

}

