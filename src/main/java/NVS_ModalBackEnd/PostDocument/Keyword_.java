package NVS_ModalBackEnd.PostDocument;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Keyword_ {

@SerializedName("keywordHandle")
@Expose
private Integer keywordHandle;
@SerializedName("name")
@Expose
private String name;
@SerializedName("value")
@Expose
private String value;
@SerializedName("dataType")
@Expose
private String dataType;

public Integer getKeywordHandle() {
return keywordHandle;
}

public void setKeywordHandle(Integer keywordHandle) {
this.keywordHandle = keywordHandle;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public String getValue() {
return value;
}

public void setValue(String value) {
this.value = value;
}

public String getDataType() {
return dataType;
}

public void setDataType(String dataType) {
this.dataType = dataType;
}

}
