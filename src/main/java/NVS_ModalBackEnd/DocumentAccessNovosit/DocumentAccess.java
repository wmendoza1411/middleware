/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ModalBackEnd.DocumentAccessNovosit;

/**
 *
 * @author Capas360
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

public class DocumentAccess {


@SerializedName("id")
@Expose
private Integer id;
@SerializedName("code")
@Expose
private String code;
@SerializedName("category")
@Expose
private String category;

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public String getCode() {
return code;
}

public void setCode(String code) {
this.code = code;
}

public String getCategory() {
return category;
}

public void setCategory(String category) {
this.category = category;
}


}