package NVS_ModalBackEnd.GetDocument;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class KeywordData {

@SerializedName("keywordMap")
@Expose
private List<KeywordMap> keywordMap = null;

@SerializedName("keywords")
@Expose
private List<Keyword> keywords = null;

@SerializedName("recordMap")
@Expose
private List<RecordMap> recordMap = null;

@SerializedName("records")
@Expose
private List<Record> records = null;




public List<KeywordMap> getKeywordMap() {
return keywordMap;
}

public void setKeywordMap(List<KeywordMap> keywordMap) {
this.keywordMap = keywordMap;
}

public List<Keyword> getKeywords() {
return keywords;
}

public void setKeywords(List<Keyword> keywords) {
this.keywords = keywords;
}

public List<RecordMap> getRecordMap() {
return recordMap;
}

public void setRecordMap(List<RecordMap> recordMap) {
this.recordMap = recordMap;
}

public List<Record> getRecords() {
return records;
}

public void setRecords(List<Record> records) {
this.records = records;
}

}
