package NVS_ModalBackEnd.GetDocument;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Record {

@SerializedName("keywords")
@Expose
private List<Keyword_> keywords = null;
@SerializedName("order")
@Expose
private Integer order;
@SerializedName("recordHandle")
@Expose
private Integer recordHandle;
@SerializedName("id")
@Expose
private Integer id;
@SerializedName("records")
@Expose
private List<Object> records = null;

public List<Keyword_> getKeywords() {
return keywords;
}

public void setKeywords(List<Keyword_> keywords) {
this.keywords = keywords;
}

public Integer getOrder() {
return order;
}

public void setOrder(Integer order) {
this.order = order;
}

public Integer getRecordHandle() {
return recordHandle;
}

public void setRecordHandle(Integer recordHandle) {
this.recordHandle = recordHandle;
}

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public List<Object> getRecords() {
return records;
}

public void setRecords(List<Object> records) {
this.records = records;
}

}
