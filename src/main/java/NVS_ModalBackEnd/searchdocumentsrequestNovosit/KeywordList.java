/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ModalBackEnd.searchdocumentsrequestNovosit;

/**
 *
 * @author Capas360
 */
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class KeywordList {

@SerializedName("keywordHandle")
@Expose
private Integer keywordHandle;
@SerializedName("keywordName")
@Expose
private String keywordName;
@SerializedName("dataType")
@Expose
private String dataType;
@SerializedName("isGroup")
@Expose
private Boolean isGroup;
@SerializedName("values")
@Expose
private List<Value> values = null;
@SerializedName("groupInstances")
@Expose
private List<GroupInstance> groupInstances = null;
@SerializedName("dictionaryConfig")
@Expose
private String dictionaryConfig;

public Integer getKeywordHandle() {
return keywordHandle;
}

public void setKeywordHandle(Integer keywordHandle) {
this.keywordHandle = keywordHandle;
}

public String getKeywordName() {
return keywordName;
}

public void setKeywordName(String keywordName) {
this.keywordName = keywordName;
}

public String getDataType() {
return dataType;
}

public void setDataType(String dataType) {
this.dataType = dataType;
}

public Boolean getIsGroup() {
return isGroup;
}

public void setIsGroup(Boolean isGroup) {
this.isGroup = isGroup;
}

public List<Value> getValues() {
return values;
}

public void setValues(List<Value> values) {
this.values = values;
}

public List<GroupInstance> getGroupInstances() {
return groupInstances;
}

public void setGroupInstances(List<GroupInstance> groupInstances) {
this.groupInstances = groupInstances;
}

public String getDictionaryConfig() {
return dictionaryConfig;
}

public void setDictionaryConfig(String dictionaryConfig) {
this.dictionaryConfig = dictionaryConfig;
}

}
