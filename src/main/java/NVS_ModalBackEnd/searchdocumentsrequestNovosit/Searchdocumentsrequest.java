/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ModalBackEnd.searchdocumentsrequestNovosit;

/**
 *
 * @author Capas360
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

public class Searchdocumentsrequest {

@SerializedName("referenceMarker")
@Expose
private String referenceMarker;
@SerializedName("documentHandle")
@Expose
private Integer documentHandle;
@SerializedName("documentTypeHandle")
@Expose
private Integer documentTypeHandle;
@SerializedName("documentTypeName")
@Expose
private String documentTypeName;
@SerializedName("keywordList")
@Expose
private List<KeywordList> keywordList = null;
@SerializedName("originalDocumentHandle")
@Expose
private Integer originalDocumentHandle;
@SerializedName("version")
@Expose
private Integer version;
@SerializedName("status")
@Expose
private String status;
@SerializedName("businessFunctionHandle")
@Expose
private Integer businessFunctionHandle;
@SerializedName("businessFunctionName")
@Expose
private String businessFunctionName;
@SerializedName("businessFunctionType")
@Expose
private String businessFunctionType;
@SerializedName("createdBy")
@Expose
private Integer createdBy;
@SerializedName("format")
@Expose
private String format;

public String getReferenceMarker() {
return referenceMarker;
}

public void setReferenceMarker(String referenceMarker) {
this.referenceMarker = referenceMarker;
}

public Integer getDocumentHandle() {
return documentHandle;
}

public void setDocumentHandle(Integer documentHandle) {
this.documentHandle = documentHandle;
}

public Integer getDocumentTypeHandle() {
return documentTypeHandle;
}

public void setDocumentTypeHandle(Integer documentTypeHandle) {
this.documentTypeHandle = documentTypeHandle;
}

public String getDocumentTypeName() {
return documentTypeName;
}

public void setDocumentTypeName(String documentTypeName) {
this.documentTypeName = documentTypeName;
}

public List<KeywordList> getKeywordList() {
return keywordList;
}

public void setKeywordList(List<KeywordList> keywordList) {
this.keywordList = keywordList;
}

public Integer getOriginalDocumentHandle() {
return originalDocumentHandle;
}

public void setOriginalDocumentHandle(Integer originalDocumentHandle) {
this.originalDocumentHandle = originalDocumentHandle;
}

public Integer getVersion() {
return version;
}

public void setVersion(Integer version) {
this.version = version;
}

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public Integer getBusinessFunctionHandle() {
return businessFunctionHandle;
}

public void setBusinessFunctionHandle(Integer businessFunctionHandle) {
this.businessFunctionHandle = businessFunctionHandle;
}

public String getBusinessFunctionName() {
return businessFunctionName;
}

public void setBusinessFunctionName(String businessFunctionName) {
this.businessFunctionName = businessFunctionName;
}

public String getBusinessFunctionType() {
return businessFunctionType;
}

public void setBusinessFunctionType(String businessFunctionType) {
this.businessFunctionType = businessFunctionType;
}

public Integer getCreatedBy() {
return createdBy;
}

public void setCreatedBy(Integer createdBy) {
this.createdBy = createdBy;
}

public String getFormat() {
return format;
}

public void setFormat(String format) {
this.format = format;
}

}