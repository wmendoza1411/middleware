/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ModalBackEnd.GenerationExample;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Context {

@SerializedName("data")
@Expose
private List<Datum> data = null;
@SerializedName("templatesHandleList")
@Expose
private List<Integer> templatesHandleList = null;

public List<Datum> getData() {
return data;
}

public void setData(List<Datum> data) {
this.data = data;
}

public List<Integer> getTemplatesHandleList() {
return templatesHandleList;
}

public void setTemplatesHandleList(List<Integer> templatesHandleList) {
this.templatesHandleList = templatesHandleList;
}

}