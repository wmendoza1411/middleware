/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ModalBackEnd.GenerationExample;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Record {

@SerializedName("id")
@Expose
private String id;
@SerializedName("keywords")
@Expose
private Keywords_ keywords;

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public Keywords_ getKeywords() {
return keywords;
}

public void setKeywords(Keywords_ keywords) {
this.keywords = keywords;
}

}