/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ModalBackEnd.GenerationExample;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GenerationExample {

@SerializedName("contexts")
@Expose
private List<Context> contexts = null;
@SerializedName("templatesHandleList")
@Expose
private List<Integer> templatesHandleList = null;
@SerializedName("version")
@Expose
private String version;
@SerializedName("preview")
@Expose
private Boolean preview;
@SerializedName("resultType")
@Expose
private String resultType;

public List<Context> getContexts() {
return contexts;
}

public void setContexts(List<Context> contexts) {
this.contexts = contexts;
}

public List<Integer> getTemplatesHandleList() {
return templatesHandleList;
}

public void setTemplatesHandleList(List<Integer> templatesHandleList) {
this.templatesHandleList = templatesHandleList;
}

public String getVersion() {
return version;
}

public void setVersion(String version) {
this.version = version;
}

public Boolean getPreview() {
return preview;
}

public void setPreview(Boolean preview) {
this.preview = preview;
}

public String getResultType() {
return resultType;
}

public void setResultType(String resultType) {
this.resultType = resultType;
}

}