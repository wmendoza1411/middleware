/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ModalBackEnd.GenerationExample;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

@SerializedName("keywords")
@Expose
private Keywords keywords;
@SerializedName("records")
@Expose
private List<Record> records = null;

public Keywords getKeywords() {
return keywords;
}

public void setKeywords(Keywords keywords) {
this.keywords = keywords;
}

public List<Record> getRecords() {
return records;
}

public void setRecords(List<Record> records) {
this.records = records;
}

}
