

package NVS_ModalBackEnd.Profile;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PDmGetUserProfile {

@SerializedName("userId")
@Expose
private Integer userId;
@SerializedName("name")
@Expose
private String name;
@SerializedName("lastName")
@Expose
private String lastName;
@SerializedName("fullName")
@Expose
private String fullName;
@SerializedName("locale")
@Expose
private String locale;
@SerializedName("token")
@Expose
private String token;
@SerializedName("company")
@Expose
private String company;
@SerializedName("expires")
@Expose
private String expires;
@SerializedName("specialAccesses")
@Expose
private List<String> specialAccesses = null;
@SerializedName("documentTypeAccesses")
@Expose
private List<DocumentTypeAccess> documentTypeAccesses = null;
@SerializedName("email")
@Expose
private String email;
@SerializedName("licensedResources")
@Expose
private List<String> licensedResources = null;
@SerializedName("firstName")
@Expose
private String firstName;

public Integer getUserId() {
return userId;
}

public void setUserId(Integer userId) {
this.userId = userId;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public String getLastName() {
return lastName;
}

public void setLastName(String lastName) {
this.lastName = lastName;
}

public String getFullName() {
return fullName;
}

public void setFullName(String fullName) {
this.fullName = fullName;
}

public String getLocale() {
return locale;
}

public void setLocale(String locale) {
this.locale = locale;
}

public String getToken() {
return token;
}

public void setToken(String token) {
this.token = token;
}

public String getCompany() {
return company;
}

public void setCompany(String company) {
this.company = company;
}

public String getExpires() {
return expires;
}

public void setExpires(String expires) {
this.expires = expires;
}

public List<String> getSpecialAccesses() {
return specialAccesses;
}

public void setSpecialAccesses(List<String> specialAccesses) {
this.specialAccesses = specialAccesses;
}

public List<DocumentTypeAccess> getDocumentTypeAccesses() {
return documentTypeAccesses;
}

public void setDocumentTypeAccesses(List<DocumentTypeAccess> documentTypeAccesses) {
this.documentTypeAccesses = documentTypeAccesses;
}

public String getEmail() {
return email;
}

public void setEmail(String email) {
this.email = email;
}

public List<String> getLicensedResources() {
return licensedResources;
}

public void setLicensedResources(List<String> licensedResources) {
this.licensedResources = licensedResources;
}

public String getFirstName() {
return firstName;
}

public void setFirstName(String firstName) {
this.firstName = firstName;
}

}

