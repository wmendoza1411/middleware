/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ModalBackEnd.ContextDefinition;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

public class ContextDefinition {

@SerializedName("keywords")
@Expose
private List<Keyword> keywords = null;
@SerializedName("records")
@Expose
private List<Record> records = null;

public List<Keyword> getKeywords() {
return keywords;
}

public void setKeywords(List<Keyword> keywords) {
this.keywords = keywords;
}

public List<Record> getRecords() {
return records;
}

public void setRecords(List<Record> records) {
this.records = records;
}

}