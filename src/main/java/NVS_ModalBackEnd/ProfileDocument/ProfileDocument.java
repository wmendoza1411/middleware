/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ModalBackEnd.ProfileDocument;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileDocument {

@SerializedName("userId")
@Expose
private Integer userId;
@SerializedName("name")
@Expose
private String name;
@SerializedName("fullName")
@Expose
private String fullName;
@SerializedName("locale")
@Expose
private String locale;
@SerializedName("token")
@Expose
private String token;
@SerializedName("company")
@Expose
private Object company;
@SerializedName("expires")
@Expose
private String expires;
@SerializedName("specialAccesses")
@Expose
private List<String> specialAccesses = null;
@SerializedName("documentTypeAccesses")
@Expose
private List<DocumentTypeAccess> documentTypeAccesses = null;
@SerializedName("email")
@Expose
private String email;
@SerializedName("licensedResources")
@Expose
private Object licensedResources;

public Integer getUserId() {
return userId;
}

public void setUserId(Integer userId) {
this.userId = userId;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public String getFullName() {
return fullName;
}

public void setFullName(String fullName) {
this.fullName = fullName;
}

public String getLocale() {
return locale;
}

public void setLocale(String locale) {
this.locale = locale;
}

public String getToken() {
return token;
}

public void setToken(String token) {
this.token = token;
}

public Object getCompany() {
return company;
}

public void setCompany(Object company) {
this.company = company;
}

public String getExpires() {
return expires;
}

public void setExpires(String expires) {
this.expires = expires;
}

public List<String> getSpecialAccesses() {
return specialAccesses;
}

public void setSpecialAccesses(List<String> specialAccesses) {
this.specialAccesses = specialAccesses;
}

public List<DocumentTypeAccess> getDocumentTypeAccesses() {
return documentTypeAccesses;
}

public void setDocumentTypeAccesses(List<DocumentTypeAccess> documentTypeAccesses) {
this.documentTypeAccesses = documentTypeAccesses;
}

public String getEmail() {
return email;
}

public void setEmail(String email) {
this.email = email;
}

public Object getLicensedResources() {
return licensedResources;
}

public void setLicensedResources(Object licensedResources) {
this.licensedResources = licensedResources;
}

}