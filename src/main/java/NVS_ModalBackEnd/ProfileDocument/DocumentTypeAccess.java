/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ModalBackEnd.ProfileDocument;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DocumentTypeAccess {

@SerializedName("documentTypeHandle")
@Expose
private Integer documentTypeHandle;
@SerializedName("documentTypeName")
@Expose
private String documentTypeName;
@SerializedName("permissions")
@Expose
private List<String> permissions = null;

public Integer getDocumentTypeHandle() {
return documentTypeHandle;
}

public void setDocumentTypeHandle(Integer documentTypeHandle) {
this.documentTypeHandle = documentTypeHandle;
}

public String getDocumentTypeName() {
return documentTypeName;
}

public void setDocumentTypeName(String documentTypeName) {
this.documentTypeName = documentTypeName;
}

public List<String> getPermissions() {
return permissions;
}

public void setPermissions(List<String> permissions) {
this.permissions = permissions;
}

}