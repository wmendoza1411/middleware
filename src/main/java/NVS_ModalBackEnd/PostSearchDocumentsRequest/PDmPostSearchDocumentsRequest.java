

package NVS_ModalBackEnd.PostSearchDocumentsRequest;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PDmPostSearchDocumentsRequest {

@SerializedName("arguments")
@Expose
private List<Argument> arguments = null;
@SerializedName("isSqlSearch")
@Expose
private Boolean isSqlSearch;

public List<Argument> getArguments() {
return arguments;
}

public void setArguments(List<Argument> arguments) {
this.arguments = arguments;
}

public Boolean getIsSqlSearch() {
return isSqlSearch;
}

public void setIsSqlSearch(Boolean isSqlSearch) {
this.isSqlSearch = isSqlSearch;
}

}

