package NVS_ModalBackEnd.PostDocumentNovosit;

import NVS_ModalBackEnd.PostDocumentsGenerationRequest.*;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PDM_PostDocumentRequest {

@SerializedName("contentType")
@Expose
private Object contentType;
@SerializedName("documentHandle")
@Expose
private Integer documentHandle;
@SerializedName("documentTypeHandle")
@Expose
private Integer documentTypeHandle;
@SerializedName("lastUpdated")
@Expose
private String lastUpdated;
@SerializedName("version")
@Expose
private Integer version;
@SerializedName("createdBy")
@Expose
private Integer createdBy;
@SerializedName("creationDate")
@Expose
private String creationDate;
@SerializedName("createdByName")
@Expose
private Object createdByName;
@SerializedName("versionDate")
@Expose
private String versionDate;
@SerializedName("documentType")
@Expose
private String documentType;
@SerializedName("pagesBase64")
@Expose
private Object pagesBase64;
@SerializedName("reference")
@Expose
private String reference;
@SerializedName("keywordData")
@Expose
private Object keywordData;
@SerializedName("generationToken")
@Expose
private Object generationToken;
@SerializedName("accesses")
@Expose
private Object accesses;
@SerializedName("templateDocumentHandle")
@Expose
private Object templateDocumentHandle;

public Object getContentType() {
return contentType;
}

public void setContentType(Object contentType) {
this.contentType = contentType;
}

public Integer getDocumentHandle() {
return documentHandle;
}

public void setDocumentHandle(Integer documentHandle) {
this.documentHandle = documentHandle;
}

public Integer getDocumentTypeHandle() {
return documentTypeHandle;
}

public void setDocumentTypeHandle(Integer documentTypeHandle) {
this.documentTypeHandle = documentTypeHandle;
}

public String getLastUpdated() {
return lastUpdated;
}

public void setLastUpdated(String lastUpdated) {
this.lastUpdated = lastUpdated;
}

public Integer getVersion() {
return version;
}

public void setVersion(Integer version) {
this.version = version;
}

public Integer getCreatedBy() {
return createdBy;
}

public void setCreatedBy(Integer createdBy) {
this.createdBy = createdBy;
}

public String getCreationDate() {
return creationDate;
}

public void setCreationDate(String creationDate) {
this.creationDate = creationDate;
}

public Object getCreatedByName() {
return createdByName;
}

public void setCreatedByName(Object createdByName) {
this.createdByName = createdByName;
}

public String getVersionDate() {
return versionDate;
}

public void setVersionDate(String versionDate) {
this.versionDate = versionDate;
}

public String getDocumentType() {
return documentType;
}

public void setDocumentType(String documentType) {
this.documentType = documentType;
}

public Object getPagesBase64() {
return pagesBase64;
}

public void setPagesBase64(Object pagesBase64) {
this.pagesBase64 = pagesBase64;
}

public String getReference() {
return reference;
}

public void setReference(String reference) {
this.reference = reference;
}

public Object getKeywordData() {
return keywordData;
}

public void setKeywordData(Object keywordData) {
this.keywordData = keywordData;
}

public Object getGenerationToken() {
return generationToken;
}

public void setGenerationToken(Object generationToken) {
this.generationToken = generationToken;
}

public Object getAccesses() {
return accesses;
}

public void setAccesses(Object accesses) {
this.accesses = accesses;
}

public Object getTemplateDocumentHandle() {
return templateDocumentHandle;
}

public void setTemplateDocumentHandle(Object templateDocumentHandle) {
this.templateDocumentHandle = templateDocumentHandle;
}

}