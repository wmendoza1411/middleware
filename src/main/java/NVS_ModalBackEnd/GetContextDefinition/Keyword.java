package NVS_ModalBackEnd.GetContextDefinition;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Keyword {

@SerializedName("name")
@Expose
private String name;
@SerializedName("maxLength")
@Expose
private Integer maxLength;
@SerializedName("minOccurs")
@Expose
private Integer minOccurs;
@SerializedName("maxOccurs")
@Expose
private Integer maxOccurs;
@SerializedName("description")
@Expose
private String description;
@SerializedName("instructions")
@Expose
private String instructions;
@SerializedName("label")
@Expose
private String label;
@SerializedName("humanName")
@Expose
private String humanName;
@SerializedName("dataType")
@Expose
private String dataType;
@SerializedName("inputType")
@Expose
private String inputType;
@SerializedName("keywordNum")
@Expose
private Integer keywordNum;
@SerializedName("autoCompleteValues")
@Expose
private Boolean autoCompleteValues;
@SerializedName("lineCount")
@Expose
private Integer lineCount;

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public Integer getMaxLength() {
return maxLength;
}

public void setMaxLength(Integer maxLength) {
this.maxLength = maxLength;
}

public Integer getMinOccurs() {
return minOccurs;
}

public void setMinOccurs(Integer minOccurs) {
this.minOccurs = minOccurs;
}

public Integer getMaxOccurs() {
return maxOccurs;
}

public void setMaxOccurs(Integer maxOccurs) {
this.maxOccurs = maxOccurs;
}

public String getDescription() {
return description;
}

public void setDescription(String description) {
this.description = description;
}

public String getInstructions() {
return instructions;
}

public void setInstructions(String instructions) {
this.instructions = instructions;
}

public String getLabel() {
return label;
}

public void setLabel(String label) {
this.label = label;
}

public String getHumanName() {
return humanName;
}

public void setHumanName(String humanName) {
this.humanName = humanName;
}

public String getDataType() {
return dataType;
}

public void setDataType(String dataType) {
this.dataType = dataType;
}

public String getInputType() {
return inputType;
}

public void setInputType(String inputType) {
this.inputType = inputType;
}

public Integer getKeywordNum() {
return keywordNum;
}

public void setKeywordNum(Integer keywordNum) {
this.keywordNum = keywordNum;
}

public Boolean getAutoCompleteValues() {
return autoCompleteValues;
}

public void setAutoCompleteValues(Boolean autoCompleteValues) {
this.autoCompleteValues = autoCompleteValues;
}

public Integer getLineCount() {
return lineCount;
}

public void setLineCount(Integer lineCount) {
this.lineCount = lineCount;
}

}
