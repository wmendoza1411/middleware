/*
 * Artefacto (clase) para el proceso de las solicitudes del servicio: autentication
 * los recursos atendidos en este servicio son:
 * login, join y forget
 *
 */
package NVS_RestServer;

// Clases modal para el manejo de los recurso del servicio de Autenticación.
// A su vez estas clases son instanciadas por la clase Configurator de la
// se octienen los apuntadores.
import NVS_ModalFrontEnd.Autentication.PDM_Join;
import NVS_ModalFrontEnd.Autentication.PDM_Login;
import NVS_ModalFrontEnd.Autentication.PDM_Forget;

// Clase para el manejo de la seguridad y validacion de los tokens
import NVS_ModalFrontEnd.TokenDriver.PDM_JWT;
//
// Clase utilitaria con la que se puede serializar y desserializar objetos Json
// a clases de java.
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

// Clase Configuradora de los servicio, esta clase abstrae todos los artefactos,
// clases y parametros necesarios para el manejo de los recursos de todas los servicios
// planificados.
import NVS_Configurator.PDM_Configurator;

// Clase que define al cliente para el consumo de los servicios rest de 
// ProDoctivity 
import NVS_ClientBackEnd.CRN_UserProfile;
import NVS_ClientBackEnd.CRN_UserJointNovosit;
import NVS_ClientBackEnd.CRN_UserForgetNovosit;

import NVS_Middleware.NVS_MiddAutentication;
//
// Clases propias de la especificacion de Java que soportan los servicios rest.
import static javax.ws.rs.HttpMethod.POST;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
//
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Request;
import static javax.ws.rs.core.Response.status;
//
import NVS_ClientBackEnd.CRN_UserProfile;
import io.jsonwebtoken.SignatureException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import javax.ws.rs.core.Response.Status;

import static javax.ws.rs.core.Response.status;

//

/**
 * REST Web Service autentication
 *
 * identifica el servicio en este caso Autenticion de usuarios:
 * la url has este momento se compone:
 * * a este nivel el url de invocacion de servicio se compone de:
 * http://dns:puerto/PDT-Movil/services/autentication/
 */
@Path("autentication")
public class PDM_AutenticationServer {

    // Se instancia la clase de configuracion
    PDM_Configurator pdm_configurator   = new PDM_Configurator();
    
    // Se recibe apuntador de la clase que Serializa y DesSerializa de Json 
    // desde el configurador
    private Gson gson                   = this.pdm_configurator.getGson();
    private GsonBuilder gsonBuilder     = this.pdm_configurator.getGsonBuilder();
    
    // Se recibe apuntador de la clase para la validacion del token desde el configurador
    private PDM_JWT pdm_jwt             = this.pdm_configurator.getPdm_jwt();
    
    private PDM_Application             pdm_aplication;
    private NVS_MiddAutentication       nvs_middautenntication;
    /**
     * Construuctor de AutenticationServer
     */
    public PDM_AutenticationServer() {}
    //
    // Autentica usuario..... a traves del recurso login
    // * * a este nivel el url de invocacion de servicio se compone de:
    // http://IP:puerto/PDT-Movil/services/autentication/login
    @POST
    @Path("login")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getLogin(String json) throws UnsupportedEncodingException, SecurityException, IOException {
       
        try{

            // Set of general services variables 
            if (!pdm_configurator.pdm_configurator()) {
                return Response.ok(pdm_configurator.getPdm_paramatermodal().
                        getMensajeserversuspend()).build();}
            
            // It serialicer the Request Json to clases login
            PDM_Login pdm_login = gson.fromJson(json, PDM_Login.class);
            
            this.pdm_configurator.setPdm_login(pdm_login);
            pdm_login.setRespuesta("00");  // indicate if there is any error for login
                                           // 00 Not Error, 50 With Error 99 Fatal Error
    
            // The come in token is validated 
            Boolean TknValido = pdm_jwt.VerifyJWT(pdm_login.getToken(),pdm_configurator);
            
            if (TknValido) { // If IS Valid Token ...
                // Call login web Services in  the Back End
                nvs_middautenntication = this.pdm_configurator.getNvs_middautentication();
                if (!(nvs_middautenntication.login(pdm_configurator))){
                    //String responde = gson.toJson(pdm_login);
                    String responde = gson.toJson(pdm_configurator.getPdm_login());//modo test
                    return Response.ok(responde).build();  // Buil Json Responde
                }
                
                // If login BackEnd is OK 
                // User and key (Fron End) to Configurator
                this.pdm_configurator.setSt_login_key(pdm_login.getClave());
                this.pdm_configurator.setSt_login_user(pdm_login.getUsuario());
                
                // User and key (Back End) to Configurator
                this.pdm_configurator.setSt_login_user_be(nvs_middautenntication.getSt_user());
                this.pdm_configurator.setSt_login_key_be(nvs_middautenntication.getSt_key());
                pdm_login.setClave("");
                pdm_login.setUsuario("");
                
                // Token is will generated
                if (pdm_jwt.CreateJWT(pdm_configurator,"login")) {
                   // pdm_login.resetUSuario();
                    pdm_login.setToken(pdm_jwt.getSt_token()); // Token generator ok....
                    pdm_login.setgeneralerror(pdm_configurator.driver_err(),"00");
                    
                    if(!pdm_login.isBl_statusbackend()){
                   // pdm_login.resetUSuario();
                    //pdm_login.setgeneralerror(pdm_configurator.driver_err("20","20","30","","Error ","internal error"),"50");
                    
                
                }
                    
                }else{  // Error in create the new token, Login is not valid...
                    pdm_login.resetUSuario();
                    pdm_login.setgeneralerror(pdm_configurator.driver_err("10","10","10","SEGU010"," Creation Token Error ",""),"50");
                    
                 
                }

            }else{
                // token that comes front App Movil Not Valid, Login Request is reject
                // Driver java reflexion error
                 pdm_login.resetUSuario();
                 pdm_login.setgeneralerror(pdm_configurator.driver_err("10","20","10",
                         "SEGU020"," Invalid Token Error",""),"50");
               
               
            }
            // Normal Respont with status  corresponding 
             String responde = gson.toJson(pdm_login);
          //test
            //String responde = gson.toJson(pdm_configurator.getUser());
            return Response.ok(responde).build();
        }
        catch (Exception e) { // Fatal error breaks normal operation
            // Register Error
            pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
            pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
            pdm_configurator.DriverError();
            
            // Not respond with Json
            return Response.status(Status.BAD_REQUEST.getStatusCode()," Error Login ").build();
        } 
     
    }
    
    //
    // Autentica usuario..... a treaves del recurso join
    // * * a este nivel el url de invocacion de servicio se compone de:
    // http://IP:puerto/PDT-Movil/services/autentication/join
    @POST
    @Path("join")
    @Produces(MediaType.APPLICATION_JSON) // Produce    Json
    @Consumes(MediaType.APPLICATION_JSON) // Recibe     Json
    public Response postJoin(String json) throws UnsupportedEncodingException, SecurityException, IOException {
        try{
            
           // Set of general services variables 
            if (!pdm_configurator.pdm_configurator()) {
                return Response.ok(pdm_configurator.getPdm_paramatermodal().
                        getMensajeserversuspend()).build();}

            // Se serializa el Json que llega en la peticion hacia la clase join
            PDM_Join pdm_join           = gson.fromJson(json, PDM_Join.class);
            pdm_configurator.setPdm_join(pdm_join);
            
            // If token is valid...?
            Boolean TknValido           = pdm_jwt.VerifyJWT(pdm_join.getToken(),
                                                                pdm_configurator);
           
            if (TknValido) { // If Token ok....
                    
               nvs_middautenntication = this.pdm_configurator.getNvs_middautentication();
                if (nvs_middautenntication.join(pdm_configurator)){
                    // make respond whit the pdm_join
                    String responde = gson.toJson(pdm_join);
                    return Response.ok(responde).build();
                }
               
                // If was successful the join in the Back End then Create New token
                if (pdm_jwt.CreateJWT(pdm_configurator,"join")) {
                                                               // Token ok
                    pdm_join.setgeneralerror(pdm_configurator.driver_err(),"00");
                    pdm_join.restUsuario();
                    pdm_join.setBl_statusbackend(true);
                    pdm_join.setToken(pdm_jwt.getSt_token());
                    
                }else{  // Error in Create New Toke 
                    
                    pdm_join.restUsuario();
                    pdm_join.setgeneralerror(pdm_configurator.driver_err("10","20","10","SEGU010"," Create Token Error",""),"50");
                 
                }
            }else{
              
                // Token With  error The Joint is Reject
                pdm_join.restUsuario();
                pdm_join.setgeneralerror(pdm_configurator.driver_err("10","20","10","SEGU010"," Invalid Token Error",""),"50");
                 
            }
            
            // Arma respuesta con el json
            String responde = gson.toJson(pdm_join);
            return Response.ok(responde).build();
        }
        
        catch (Exception e) { // Fatal Exception
            // 
            System.out.print(" catch (Exception e) " + e.getMessage());
            pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
            pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
            pdm_configurator.DriverError();
         
            return Response.status(Status.BAD_REQUEST.getStatusCode()," Error Join ").build();
        }
    }
    //
    // Autentica usuario..... a treaves del recurso forget
    // * * a este nivel el url de invocacion de servicio se compone de:
    // http://IP:puerto/PDT-Movil/services/autentication/forget
    @POST
    @Path("forget")
    @Produces(MediaType.APPLICATION_JSON) // Produce    Json
    @Consumes(MediaType.APPLICATION_JSON) // Recibe     Json
    public Response postForget(String json) throws UnsupportedEncodingException, SecurityException, IOException {
        
        try {
            // Set of general services variables 
            if (!pdm_configurator.pdm_configurator()) {
                return Response.ok(pdm_configurator.getPdm_paramatermodal().
                        getMensajeserversuspend()).build();}
            
            // Serializa  json to clase java pdm_forget
            PDM_Forget pdm_forget = gson.fromJson(json, PDM_Forget.class);
            pdm_configurator.setPdm_forget(pdm_forget);
            //
            // Validation Tokem 
            Boolean TknValido = pdm_jwt.VerifyJWT(pdm_forget.getToken(),
                                                        pdm_configurator);
            
            if (TknValido) { // If token Valid...???    
                nvs_middautenntication = this.pdm_configurator.getNvs_middautentication();
                if (nvs_middautenntication.forget(pdm_configurator)){
                    String responde = gson.toJson(pdm_forget);
                    return Response.ok(responde).build();}                       
            }else{
                // Error Logico o Sintactico del Token
                // Se rechaza el Forget
                pdm_forget.resetUser();
                pdm_forget.setgeneralerror(pdm_configurator.driver_err("10","20","10","SEGU010"," Invalid Token Error",""),"50");
                
            }
            
              // Arma respuesta con el jason
            String responde = gson.toJson(pdm_forget);
            return Response.ok(responde).build(); // Envia respuesta
        }
    
        catch (Exception e) { // Falla en la resepcion de la petición
            // Se Registra el error.
            pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
            pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
            pdm_configurator.DriverError();
         
            return Response.status(Status.BAD_REQUEST.getStatusCode()," Error Forget ").build();
        }
    
    }
}
