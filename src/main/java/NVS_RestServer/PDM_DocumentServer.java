/*
* Artefacto (clase) que representa el servicio document, dicho servicio 
* atendera todos los recursos de manejo de documento y la gestoria documental
*  consta de los siguientes recursos:
*
*/
package NVS_RestServer;

import NVS_ClientBackEnd.CRN_PostDocument;
import NVS_Configurator.PDM_Configurator;

import NVS_Middleware.NVS_MiddDocument;
import NVS_ModalBackEnd.PostDocumentNovosit.PDM_PostDocumentRequest;
import NVS_ModalFrontEnd.Autentication.PDM_Keyword;
import NVS_ModalFrontEnd.Autentication.PDM_Login;
import NVS_ModalFrontEnd.DocHandleNovosit.PDM_GetDocument;
import NVS_ModalFrontEnd.Document.PDM_Document;

import NVS_ModalFrontEnd.TokenDriver.PDM_JWT;
import NVS_ModalFrontEnd.searchdocumentsrequestNovosit.PDM_Searchdocumentsrequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import java.io.UnsupportedEncodingException;

import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


/**
 * REST Web Service document
 *
 * @author Novatec
 */
@Path("document")
public class PDM_DocumentServer {

   // Se instancia la clase de configuracion
    PDM_Configurator pdm_configurator   = new PDM_Configurator();
    
    // Se recibe apuntador de la clase que Serializa y DesSerializa de Json 
    // desde el configurador
    private Gson gson                   = this.pdm_configurator.getGson();
    private GsonBuilder gsonBuilder     = this.pdm_configurator.getGsonBuilder();
    
    // Se recibe apuntador de la clase para la validacion del token desde el configurador
    private PDM_JWT pdm_jwt = this.pdm_configurator.getPdm_jwt();

    /**
     * Creates a new instance of Document
     */
    public PDM_DocumentServer() {}

    /**
     * 
     * sendDocument es para el envio de documento, to Productivity 
     * Back End
     * prodoctivity http://34.228.64.13:80/
     * CoordinatorFront/api/v1/prodoctivityapi/documents
     * 
     */
    @POST
    @Path("sendDocument")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response sendDocument(String json) throws UnsupportedEncodingException, SecurityException, IOException{
        
        PDM_Document     pdm_document   =  null;
     
        try{// Set of general services variables 
            
            String base64ImageString    =  null;
            pdm_configurator.pdm_configurator();
            
            // Verificate if the server is on.....
            if (!pdm_configurator.pdm_configurator()) {
                return Response.ok(pdm_configurator.getPdm_paramatermodal().
                        getMensajeserversuspend()).build();}
            
            // Se recibe apuntador de la clase manejo recurso login desde el configurador
            pdm_document   = this.pdm_configurator.getPdm_document();
            // Se serializa el Json que llega en la peticion hacia la clase login
            pdm_document = gson.fromJson(json, PDM_Document.class);
            
            Boolean TknValido = pdm_jwt.VerifyJWT(pdm_document.getToken(),pdm_configurator);
            pdm_document.setgeneralerror(pdm_configurator.driver_err(),"00");

            if (TknValido) { // if token is valid....???

                //////////////DM_Document pdm_document=new PDM_Document();
                pdm_configurator.setPdm_document(pdm_document);
                NVS_MiddDocument nvs_midddocument = pdm_configurator.getNvs_midddocument();

                // Call Middleware Document
                if(nvs_midddocument.sendDocument(pdm_configurator)){
                    pdm_document.setCadena(base64ImageString);
                    pdm_document.setgeneralerror(pdm_configurator.driver_err(),"00");
                    //pdm_document.setMensaje(String.valueOf(pdm_configurator.
                                            //getPdm_postdocumentrequest().getDocumentHandle()));
                }
                    // Arma respuesta con el json  
            }else
                {    
                    pdm_document.resetDocument();
                    pdm_document.setgeneralerror(pdm_configurator.
                                            driver_err("10","20","10",""," Invalid Token Error",""),"50");
                }
            
            String responde = gson.toJson(pdm_document);
            return Response.ok(responde).build();
         
        }catch (Exception e) { // Falla en la resepcion de la petición
            // Se Registra el error.
            pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
            pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
            pdm_configurator.DriverError();
            try{
                pdm_document.setgeneralerror(pdm_configurator.
                                            driver_err("10","30","20"," ",e.getMessage(),""),"50");
                pdm_document.resetDocument();
            }
            catch(Exception ex){}
                return Response.status(Response.Status.BAD_REQUEST.getStatusCode()).build();
        }
    }
    
    @POST
    @Path("sendDocument2")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response sendDocument2(String json) throws UnsupportedEncodingException{
        
        PDM_Document     pdm_document   =  null;
     
        try{// Set of general services variables 
            
            
            pdm_configurator.pdm_configurator();
            pdm_document = gson.fromJson(json, PDM_Document.class);
            String cadena="";
            
          
        /////////////////////////se crea un documento final en pdf con un nombre y ruta especifica dentro del directorio
                File  file_final = new File(pdm_document.getIdentificador());
        
                try (FileInputStream imageInFile_do = 
                    new FileInputStream(file_final)) {//byte imageData[]
                   
                // Reading a Image file from file system
                byte imageData[] = new byte[(int) file_final.length()];
                imageInFile_do.read(imageData);
                cadena = Base64.getEncoder().encodeToString(imageData); 
                  
                imageInFile_do.close();
                 
                        
                } catch (FileNotFoundException e) {
			System.out.println("Image not found" + e);
                } catch (IOException ioe) {
			System.out.println("Exception while reading the Image " + ioe);
                }
                /////////////////////////////////////////fin crea documento 
            pdm_document.setCadena(cadena);
            String responde = gson.toJson(pdm_document);
            return Response.ok(responde).build();
         
        }catch (Exception e) { // Falla en la resepcion de la petición
            // Se Registra el error.
            
           // pdm_configurator.DriverError();
            try{
                pdm_document.setgeneralerror(pdm_configurator.
                                            driver_err("10","30","20"," ",e.getMessage(),""),"50");
                pdm_document.resetDocument();
            }
            catch(Exception ex){}
                return Response.status(Response.Status.BAD_REQUEST.getStatusCode()).build();
        }
    }
    @POST
    @Path("sendDocumentt")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response sendDocumentt(String json) throws UnsupportedEncodingException{
        
        PDM_Document     pdm_document   =  null;
     
        try{// Set of general services variables 
            
            String base64ImageString    =  null;
            pdm_configurator.pdm_configurator();
            
            // Verificate if the server is on.....
            if (!pdm_configurator.pdm_configurator()) {
                return Response.ok(pdm_configurator.getPdm_paramatermodal().
                        getMensajeserversuspend()).build();}
            
            // Se recibe apuntador de la clase manejo recurso login desde el configurador
            pdm_document   = this.pdm_configurator.getPdm_document();
            // Se serializa el Json que llega en la peticion hacia la clase login
            pdm_document = gson.fromJson(json, PDM_Document.class);
            
            Boolean TknValido = pdm_jwt.VerifyJWT(pdm_document.getToken(),pdm_configurator);
            pdm_document.setgeneralerror(pdm_configurator.driver_err(),"00");

            if (TknValido) { // if token is valid....???

                //////////////DM_Document pdm_document=new PDM_Document();
                pdm_configurator.setPdm_document(pdm_document);
                NVS_MiddDocument nvs_midddocument = pdm_configurator.getNvs_midddocument();

                // Call Middleware Document
                if(nvs_midddocument.sendDocument(pdm_configurator)){
                    pdm_document.setCadena(base64ImageString);
                    pdm_document.setgeneralerror(pdm_configurator.driver_err(),"00");
                    //pdm_document.setMensaje(String.valueOf(pdm_configurator.
                                            //getPdm_postdocumentrequest().getDocumentHandle()));
                }
                    // Arma respuesta con el json  
            }else
                {    
                    pdm_document.resetDocument();
                    pdm_document.setgeneralerror(pdm_configurator.
                                            driver_err("10","20","10",""," Invalid Token Error",""),"50");
                }
            
            String responde = gson.toJson(pdm_document);
            return Response.ok(responde).build();
         
        }catch (Exception e) { try {
            // Falla en la resepcion de la petición
            // Se Registra el error.
            pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
            } catch (SecurityException ex) {
                Logger.getLogger(PDM_DocumentServer.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(PDM_DocumentServer.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
            } catch (SecurityException ex) {
                Logger.getLogger(PDM_DocumentServer.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(PDM_DocumentServer.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                pdm_configurator.DriverError();
            } catch (SecurityException ex) {
                Logger.getLogger(PDM_DocumentServer.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(PDM_DocumentServer.class.getName()).log(Level.SEVERE, null, ex);
            }
            try{
                pdm_document.setgeneralerror(pdm_configurator.
                                            driver_err("10","30","20"," ",e.getMessage(),""),"50");
                pdm_document.resetDocument();
            }
            catch(Exception ex){}
                return Response.status(Response.Status.BAD_REQUEST.getStatusCode()).build();
        }
    }
}
