/*
* Artefacto (clase) que entrega y valida token de seguridad JWT
* El primer evento de una conversación iniciada por el front End
* es la solicitud del token de seguridad ademas se verifica el 
* servicio en el Back End
* 
*/
package NVS_RestServer;

// Clases para la gestion del servicio Rest
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


// Clase para la gestion del JWT
import NVS_ModalFrontEnd.TokenDriver.PDM_JWT;

// Class for the json management
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

// Apuntador Clase configuradora.
import NVS_Configurator.PDM_Configurator;

// Apuntador Clase consumo servicio "service/info" Back End
import NVS_ClientBackEnd.CRN_GetInfo;
import NVS_Middleware.NVS_MiddJwtServer;
import NVS_Middleware.NVS_MiddInfo;

//import NVS_ModalBackEnd.GetInfo.PDmGetServiceInfo;

// Apuntador PDM_INfo
import  NVS_ModalFrontEnd.Info.PDM_Info;
import java.io.IOException;

/**
 * REST Web Service document
 *
 * @author Novatec
 * http://IP:puerto/PDT-Movil/services/JWToken
 */
@Path("JWToken")
public class PDM_JwtServer {
    
    // Se instancia la clase configuradora.
    PDM_Configurator pdm_configurator   = new PDM_Configurator();
    
    // se instancian desde el configurador las clases para Serializar y 
    // de los Json.
    private Gson gson                   = this.pdm_configurator.getGson();
    private GsonBuilder gsonBuilder     = this.pdm_configurator.getGsonBuilder();
        
    /* Constructor.....*/
    public PDM_JwtServer() {}
    
    // Set Up class for verificate the ProDoctivity dispobibility 
    CRN_GetInfo crn_getinfo             = this.pdm_configurator.getCrn_getinfo();
    String responde = "";

    /**
     * Requens token
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON) // Parameter: Phone Number
    public Response getJson(String json) throws SecurityException, IOException {
        // Is validate the prodoctiviy status is (On Line or Off Line)
        try {
            
            // Set of general services variables 
            if (!pdm_configurator.pdm_configurator()) {
                return Response.ok(pdm_configurator.getPdm_paramatermodal().
                        getMensajeserversuspend()).build();}
            //
            // Se des serializa el json con la solicitud de token hacia la clase que lo 
            PDM_JWT pdm_jwt = gson.fromJson(json, PDM_JWT.class);  // Generated class
            pdm_configurator.setPdm_jwt(pdm_jwt); // set PDM_JWT pointer class to configurator
            
            // Token Generation.
            if (pdm_jwt.CreateJWT(pdm_configurator,"token")) 
                                                {   // Si fue positiva la creación del
                                                    // Token ya generado y se pasa a crear el Json de         
                    // Include status ProDoctivity (on / Off Line) front configurator class
                    pdm_jwt.setBl_statusbackend(pdm_configurator.isBl_onlineProd());
                    // make the json
                    pdm_jwt.setgeneralerror(); // Reset error
                    responde = gson.toJson(pdm_jwt); 
                    // response
                    return Response.ok(responde).build();
                }             
            else
                {   // Response NO Ok
                    pdm_jwt.setBl_statusbackend(pdm_configurator.isBl_onlineProd());
                    pdm_jwt.setgeneralerror("10","30","10",""," Create Token Error","","50");
                    responde = gson.toJson(pdm_jwt);
                    return Response.ok(responde).build();
            }
        }
        catch (Exception e) { // Fatal Error
            // Se Registra el error.
            pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
            pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
            pdm_configurator.DriverError();
            // fatal exception
            return Response.status(Response.Status.BAD_REQUEST.getStatusCode(),
                    " Fatal Error token request ").build();
        }
    }
    
    // Request for status Productivity
    @POST
    @Path("info")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getStatus(String json) throws SecurityException, IOException {
           
        try {
     
            pdm_configurator.pdm_configurator();
            
            // Verificate if BackEnd is Online or OffLine
            NVS_MiddInfo  nvs_middinfo = pdm_configurator.getNvs_middinfo();
            nvs_middinfo.isOnlineBackEnd(pdm_configurator);
                
            // Respont to Front End the prodoctivity status with json
            String responde = gson.toJson(nvs_middinfo.getPdm_info());
            
            return Response.ok(responde).build();
           
        }
        
        catch (Exception e) { // Fatal Exception Return error....
            // Se Registra el error.
            pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
            pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
            pdm_configurator.DriverError();
            
            // fatal exception
            return Response.status(Response.Status.BAD_REQUEST.getStatusCode(),
                    " Fatal Error Info request ").build();
        }
           
    }

}
