/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ModalFrondEnd.DocumentAccessNovosit;

/**
 *
 * @author Capas360
 */
import NVS_Error.PDM_HandlerError;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;


public class PDM_DocumentAccess {



private Integer id;
private String code;
private String category;
private String internal_error= "";
private String respuesta;
private boolean bl_statusbackend;
private PDM_HandlerError handlererror;

    public String getInternal_error() {
        return internal_error;
    }

    public void setInternal_error(String internal_error) {
        this.internal_error = internal_error;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public boolean isBl_statusbackend() {
        return bl_statusbackend;
    }

    public void setBl_statusbackend(boolean bl_statusbackend) {
        this.bl_statusbackend = bl_statusbackend;
    }

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public String getCode() {
return code;
}

public void setCode(String code) {
this.code = code;
}

public String getCategory() {
return category;
}

public void setCategory(String category) {
this.category = category;
}

public void setgeneralerror(PDM_HandlerError Par_handlererror, String par_repuesta){
        
        if (handlererror==null){PDM_HandlerError handlererror = new PDM_HandlerError();}
        
        getHandlererror().setOrigen(Par_handlererror.getOrigen());
        getHandlererror().setSclass(Par_handlererror.getSclass());
        getHandlererror().setTip(Par_handlererror.getTip());
        getHandlererror().setCod(Par_handlererror.getCod());
        getHandlererror().setUsermessage(Par_handlererror.getUsermessage());
        getHandlererror().setIntmessage(Par_handlererror.getIntmessage());
        setRespuesta(par_repuesta);
        
        return;
        
    }
    
    public  void setgeneralerror(String par_origen, 
                                            String par_class,
                                                String par_tipo,
                                                    String par_cod,
                                                        String par_messuser,
                                                            String par_messinter,
                                                                String par_repuesta){
        getHandlererror().setOrigen(par_origen);
        getHandlererror().setSclass(par_class);
        getHandlererror().setTip(par_tipo);
        getHandlererror().setCod(par_cod);
        getHandlererror().setUsermessage(par_messuser);
        getHandlererror().setIntmessage(par_messinter);
        setRespuesta(par_repuesta);
        
        return;
    }
    
    public  void setgeneralerror(){
        
        getHandlererror().setOrigen("00");
        getHandlererror().setSclass("");
        getHandlererror().setTip("");
        getHandlererror().setCod("");
        getHandlererror().setUsermessage("");
        getHandlererror().setIntmessage("");
        
        return;
         
    }

    public  PDM_HandlerError getHandlererror() {
        if (handlererror==null){handlererror = new PDM_HandlerError();}
        return handlererror;
    }

    public void setHandlererror(PDM_HandlerError handlererror) {
        this.handlererror = handlererror;
    }


}