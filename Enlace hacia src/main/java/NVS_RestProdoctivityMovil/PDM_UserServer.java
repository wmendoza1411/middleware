/*
* Artefacto (clase) que representa el servicios relacionados a la configuracion 
* de los servicios y usuarios  
* atendera todos los recursos de manejo de documento y la gestoria documental
*  consta de los siguientes recursos:
*
*/
package NVS_RestProdoctivityMovil;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author wmendoza
 */
@Path("user")
public class PDM_UserServer {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of Services
     */
    public PDM_UserServer() {}

    /**
     * Retrieves representation of an instance of server.prodoctivitymovil.PDM_UserServer
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String getJson() {
        return " Test User ";}

    /**
     * PUT method for updating or creating an instance of PDM_UserServer
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {}
}
