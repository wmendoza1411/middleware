/*
 * Artefacto (clase) para el proceso de las solicitudes del servicio: autentication
 * los recursos atendidos en este servicio son:
 * login, join y forget
 *
 */
package NVS_RestProdoctivityMovil;

// Clases modal para el manejo de los recurso del servicio de Autenticación.
// A su vez estas clases son instanciadas por la clase Configurator de la
// se octienen los apuntadores.
import NVS_ModalFrontEnd.Autentication.PDM_Join;
import NVS_ModalFrontEnd.Autentication.PDM_Login;
import NVS_ModalFrontEnd.Autentication.PDM_Forget;

// Clase para el manejo de la seguridad y validacion de los tokens
import NVS_ModalFrontEnd.TokenDriver.PDM_JWT;
//
// Clase utilitaria con la que se puede serializar y desserializar objetos Json
// a clases de java.
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

// Clase Configuradora de los servicio, esta clase abstrae todos los artefactos,
// clases y parametros necesarios para el manejo de los recursos de todas los servicios
// planificados.
import NVS_Configurator.PDM_Configurator;

// Clase que define al cliente para el consumo de los servicios rest de 
// ProDoctivity 
import NVS_ClientRestNovosit.CRN_UserProfile;
import NVS_ClientRestNovosit.CRN_UserJointNovosit;
import NVS_ClientRestNovosit.CRN_UserForgetNovosit;
//
// Clases propias de la especificacion de Java que soportan los servicios rest.
import static javax.ws.rs.HttpMethod.POST;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
//
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Request;
import static javax.ws.rs.core.Response.status;
//
import NVS_ClientRestNovosit.CRN_UserProfile;
import io.jsonwebtoken.SignatureException;
import java.io.UnsupportedEncodingException;
import javax.ws.rs.core.Response.Status;

import static javax.ws.rs.core.Response.status;

//

/**
 * REST Web Service autentication
 *
 * identifica el servicio en este caso Autenticion de usuarios:
 * la url has este momento se compone:
 * * a este nivel el url de invocacion de servicio se compone de:
 * http://IP:puerto/PDT-Movil/services/autentication/
 */
@Path("autentication")
public class PDM_AutenticationServer {

    // Se instancia la clase de configuracion
    PDM_Configurator pdm_configurator   = new PDM_Configurator();
    
    // Se recibe apuntador de la clase que Serializa y DesSerializa de Json 
    // desde el configurador
    private Gson gson                   = this.pdm_configurator.getGson();
    private GsonBuilder gsonBuilder     = this.pdm_configurator.getGsonBuilder();
    
    // Se recibe apuntador de la clase para la validacion del token desde el configurador
    private PDM_JWT pdm_jwt             = this.pdm_configurator.getPdm_jwt();
    
    // 
    private PDM_Application pdm_aplication;
    /**
     * Construuctor de AutenticationServer
     */
    public PDM_AutenticationServer() {}
    //
    // Autentica usuario..... a treaves del recurso login
    // * * a este nivel el url de invocacion de servicio se compone de:
    // http://IP:puerto/PDT-Movil/services/autentication/login
    @POST
    @Path("login")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getLogin(String json) throws UnsupportedEncodingException {
       
        try{
            System.out.println(" Inicio login ");
            // Metodo de Set de las variables generales del servicio
            pdm_configurator.pdm_configurator();
            
            // Se recibe apuntador de la clase manejo recurso login desde el configurador
            PDM_Login   pdm_login   = this.pdm_configurator.getPdm_login();

            // Se serializa el Json que llega en la peticion hacia la clase login
            pdm_login = gson.fromJson(json, PDM_Login.class);

            // Se Valida el token que viene en el Json 
            // y que fue serializado en la clase PDM_Login
            Boolean TknValido = pdm_jwt.VerifyJWT(pdm_login.getToken(),
                                                        pdm_configurator);

            if (TknValido) { // Si el token es valido...se invoca al recurso de login
                             // de ProDoctivity con el que se valida al usuario que viene
                             // desde la peticion movil
                             
                CRN_UserProfile crn_userProfile = this.pdm_configurator.getPdm_userprofile();
                
                if (!crn_userProfile.getUserProfile(1,pdm_login)) { // Error login BackEnd Not OK
                                                                    
                    // Arma respuesta con el json seteado
                    String responde = gson.toJson(pdm_login);
                    return Response.ok(responde).build();  // Responde
                }

                this.pdm_configurator.setSt_login_user(crn_userProfile.getSt_user());
                this.pdm_configurator.setSt_login_key(crn_userProfile.getSt_key());
                
                pdm_login.setClave("");
                pdm_login.setUsuario("");
                
                // if Login Ok...
                // Token is generated
                if (pdm_jwt.CreateJWT(pdm_configurator,
                        "login",
                                crn_userProfile.getSt_user(),       
                                    crn_userProfile.getSt_key())) {
                    
                    pdm_login.setToken(pdm_jwt.getSt_token());
                    pdm_login.setRespuesta("1");
                    
                }else{  // Error in create the new token, Login is not valid...
                    
                    pdm_login.resetUSuario();
                    pdm_login.setMensaje(pdm_jwt.getSt_msgtokennok());
                    pdm_login.setInternal_error(pdm_jwt.getSt_internal_error());
                    pdm_login.setRespuesta("11");
                    
                }

             }else{     // token that comes front App Movil has error Login Request is reject
                
                pdm_login.resetUSuario();
                pdm_login.setMensaje(pdm_jwt.getSt_msgtokennok() + " Favor iniciar de nuevo ");
                pdm_login.setInternal_error(pdm_jwt.getSt_internal_error());
                pdm_login.setRespuesta("11");  // error Token
               
            }
            // Normal Respont with status  corresponding 
            String responde = gson.toJson(pdm_login);
            return Response.ok(responde).build();
        }
        catch (Exception e) { // Fatal error breaks normal operation
            // Register Error
            pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
            pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
            pdm_configurator.DriverError();
         
            return Response.status(Status.BAD_REQUEST.getStatusCode()," Error Login ").build();
        }
     
    }
    
    //
    // Autentica usuario..... a treaves del recurso join
    // * * a este nivel el url de invocacion de servicio se compone de:
    // http://IP:puerto/PDT-Movil/services/autentication/join
    @POST
    @Path("join")
    @Produces(MediaType.APPLICATION_JSON) // Produce    Json
    @Consumes(MediaType.APPLICATION_JSON) // Recibe     Json
    public Response postJoin(String json) throws UnsupportedEncodingException {
        try{
            // Metodo de Set de las variables generales del servicio
            pdm_configurator.pdm_configurator();
            // Se recibe apuntador de la clase manejo recurso login desde el configurador
            PDM_Join    pdm_join       = this.pdm_configurator.getPdm_join();

            // Se serializa el Json que llega en la peticion hacia la clase join
            pdm_join                    = gson.fromJson(json, PDM_Join.class);
            
            // If token is valid...?
            Boolean TknValido = pdm_jwt.VerifyJWT(pdm_join.getToken(),
                                                        pdm_configurator);
            if (TknValido) { // If Token ok....
                    
                // Process Joint into Back End 
                CRN_UserJointNovosit userProfile = pdm_configurator.getPdm_userjointnovosit();
                if (!userProfile.getUserProfile(1,pdm_join)) { // Joint in BackEnd is NOT Valid
                    
                    // Arma respuesta con el json
                    String responde = gson.toJson(pdm_join);
                    return Response.ok(responde).build();
                }
                
                // If was successful the join in the Back End then Create New token
                if (pdm_jwt.CreateJWT(pdm_configurator,
                        "join",
                                userProfile.getSt_user(),
                                  userProfile.getSt_key())) {
                    pdm_join.setToken(pdm_jwt.getSt_token()); 
                }else{  // Error in Create New Toke 
                    
                    pdm_join.setToken(""); 
                    pdm_join.setRespuesta("11");
                    pdm_join.setPerfil(""); 
                    pdm_join.setMensaje(pdm_jwt.getSt_msgtokennok());
                    pdm_join.setUsuario("");
                    pdm_join.setClave("");
                    pdm_join.setInternal_error(pdm_jwt.getSt_internal_error());
                 
                }
            }else{
                
                // Token With  error The Joint is Reject
                
                pdm_join.setRespuesta("11");
                pdm_join.setPerfil("");
                pdm_join.setMensaje(pdm_jwt.getSt_msgtokennok() + " Favor iniciar de nuevo");
                     
                pdm_join.setUsuario("");   // viene como el email
                pdm_join.setToken("");
                pdm_join.setClave("");
                pdm_join.setInternal_error(pdm_jwt.getSt_internal_error());
                
            }
            
            // Arma respuesta con el json
            String responde = gson.toJson(pdm_join);
            return Response.ok(responde).build();
        }
        
        catch (Exception e) { // Fatal Exception
            // 
            System.out.print(" catch (Exception e) " + e.getMessage());
            pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
            pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
            pdm_configurator.DriverError();
         
            return Response.status(Status.BAD_REQUEST.getStatusCode()," Error Join ").build();
        }
    }
    //
    // Autentica usuario..... a treaves del recurso forget
    // * * a este nivel el url de invocacion de servicio se compone de:
    // http://IP:puerto/PDT-Movil/services/autentication/forget
    @POST
    @Path("forget")
    @Produces(MediaType.APPLICATION_JSON) // Produce    Json
    @Consumes(MediaType.APPLICATION_JSON) // Recibe     Json
    public Response postForget(String json) throws UnsupportedEncodingException {
        
        try {
             // Metodo de Set de las variables generales del servicio
            pdm_configurator.pdm_configurator();
            
            // Se instancia la clase modal del forget
            PDM_Forget  pdm_forget  = pdm_configurator.getPdm_forget();
            // Serializa el json  a la clase java pdm_forget
            pdm_forget              = gson.fromJson(json, PDM_Forget.class);
            //
            // Tokem Validation 
            Boolean TknValido = pdm_jwt.VerifyJWT(pdm_forget.getToken(),
                                                        pdm_configurator);
            
            if (TknValido) { // If token Valid...???
                             
                CRN_UserForgetNovosit userProfile = pdm_configurator.getPdm_userforgetnovisit();
                if (!userProfile.getUserProfile(1,pdm_forget)) {
                    // Error in Back End Forget Services
                    // Arma respuesta con el jason
                    String responde = gson.toJson(pdm_forget);
                    return Response.ok(responde).build();}
                             
                // Back End Forget Services is Ok
                if (pdm_jwt.CreateJWT(pdm_configurator, // Create new token
                        "forget",
                               "",
                                "")) {  // En este caso la generacion de token sera sin usuario 
                                        // y sin clave, por lo tanto el aplicativo movil debe 
                                        // disparar un login luego que el usuario haya recibido 
                                        // la clave por otro medio digtal.
                    pdm_forget.setToken(pdm_jwt.getSt_token()); 
                }else{  // Error en la creacion del nuevo token
                    
                    pdm_forget.setToken(""); 
                    pdm_forget.setRespuesta("11");
                    pdm_forget.setPerfil(""); 
                    pdm_forget.setMensaje(pdm_jwt.getSt_msgtokennok());
                    pdm_forget.setUsuario("");
                    pdm_forget.setInternal_error(pdm_jwt.getSt_internal_error());
                   
                }             
                                   
            
             }else{
                
                // Error Logico o Sintactico del Token
                        // Se rechaza el Forget
                
                pdm_forget.setRespuesta("11");
                pdm_forget.setPerfil("");
                pdm_forget.setMensaje(pdm_jwt.getSt_msgtokennok() + " Favor iniciar de nuevo");
               
                pdm_forget.setUsuario("");   // viene como el email
                pdm_forget.setToken("");
                pdm_forget.setInternal_error(pdm_jwt.getSt_internal_error());
                
            }
            
              // Arma respuesta con el jason
            String responde = gson.toJson(pdm_forget);
            return Response.ok(responde).build(); // Envia respuesta
            
        }
    
        catch (Exception e) { // Falla en la resepcion de la petición
            // Se Registra el error.
            pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
            pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
            pdm_configurator.DriverError();
         
            return Response.status(Status.BAD_REQUEST.getStatusCode()," Error Forget ").build();
        }
    
    }
}
