/*
 * Clase "Aplication" la misma define el ambito de los servicios rest
 * atenderan las peticiones provenientes del App Movil
 * el tab "services" formara parte del url y se considera el ambito
 */
package NVS_RestProdoctivityMovil;

import NVS_RestProdoctivityMovil.PDM_AutenticationServer;
import NVS_RestProdoctivityMovil.PDM_DocumentServer;
import NVS_RestProdoctivityMovil.PDM_JwtServer;
import NVS_RestProdoctivityMovil.PDM_UserServer;
import java.util.Set;
import java.util.HashSet;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
* a este nivel el url de invocacion de servicio se compone de:
 * http://IP:puerto/PDT-Movil/services
 */
@ApplicationPath("services")
public class PDM_Application extends Application {
    
    // Se definen los servicios dentro del ambito SERVICES :
    // Servicio de Autenticacion.
    // Servicio de Documento.
    // Servicio de Seguridad
    // Servicio de Server.
   
    @Override
    public Set<Class<?>> getClasses() {
          Set<Class<?>> classes = new HashSet<>();
          classes.add(PDM_AutenticationServer.class);
          classes.add(PDM_DocumentServer.class);
          classes.add(PDM_JwtServer.class);
          classes.add(PDM_UserServer.class);
      return classes;
    }
}
