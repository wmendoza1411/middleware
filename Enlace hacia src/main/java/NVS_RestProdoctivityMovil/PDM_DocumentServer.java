/*
* Artefacto (clase) que representa el servicio document, dicho servicio 
* atendera todos los recursos de manejo de documento y la gestoria documental
*  consta de los siguientes recursos:
*
*/
package NVS_RestProdoctivityMovil;

import NVS_Configurator.PDM_Configurator;
import NVS_ModalFrontEnd.Autentication.PDM_Login;
import NVS_ModalFrontEnd.Document.PDM_Document;
import NVS_ModalFrontEnd.TokenDriver.PDM_JWT;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Base64;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web Service document
 *
 * @author Novatec
 */
@Path("document")
public class PDM_DocumentServer {

   // Se instancia la clase de configuracion
    PDM_Configurator pdm_configurator   = new PDM_Configurator();
    
    // Se recibe apuntador de la clase que Serializa y DesSerializa de Json 
    // desde el configurador
    private Gson gson                   = this.pdm_configurator.getGson();
    private GsonBuilder gsonBuilder     = this.pdm_configurator.getGsonBuilder();
    
    // Se recibe apuntador de la clase para la validacion del token desde el configurador
    private PDM_JWT pdm_jwt = this.pdm_configurator.getPdm_jwt();

    /**
     * Creates a new instance of Document
     */
    public PDM_DocumentServer() {
    }

    /**
     * Retrieves representation of an instance of server.prodoctivitymovil.PDM_DocumentServer
     * @return an instance of java.lang.String
     */
    @POST
    @Path("sendDocument")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response sendDocument(String json) throws UnsupportedEncodingException{
     PDM_Document   pdm_document=null;
      try{
         
        String base64ImageString = null;
        
         pdm_configurator.pdm_configurator();
            // Se recibe apuntador de la clase manejo recurso login desde el configurador
               pdm_document   = this.pdm_configurator.getPdm_document();
          
            // Se serializa el Json que llega en la peticion hacia la clase login
            pdm_document = gson.fromJson(json, PDM_Document.class);
            
            Boolean TknValido = pdm_jwt.VerifyJWT(pdm_document.getToken(),
                                                    pdm_configurator.getKey(),
                                                      pdm_document.getTelefono(),
                                                        pdm_configurator);
             
            if (TknValido) { 
                
                System.out.println("el Json es = " + json+" y el token valido es ="+TknValido);
                System.out.println("*********************** la cadena enconde es="+pdm_document.getCadena());
            
                try{
                 ///////////////////////////////
        
                    String pathFile=pdm_document.getIdentificador()+"."+pdm_document.getTipo();
                   //  String pathFile="C:\\Users\\Capas360\\Desktop\\PDF\\ejemplo\\"+"Archivo_test_."+pdm_document.getTipo();
                    String base64Image =pdm_document.getCadena();
                    
                    //proceso de validacion de campos de seguridad va en esta parte
                
                    try (FileOutputStream imageOutFile = new FileOutputStream(pathFile)) {  
			// Converting a Base64 String into Image byte array
			byte[] imageByteArray = Base64.getDecoder().decode(base64Image);
			imageOutFile.write(imageByteArray);
                        File file2=new File(pathFile);
                        
                        pdm_document.setPeso(String.valueOf((file2.length())/1024)+"kb");
                        
                     
                    } catch (FileNotFoundException e) {
                        
			System.out.println("Image not found" + e);
                      } catch (IOException ioe) {
			System.out.println("Exception while reading the Image " + ioe);
                        }
                     System.out.println("DONE!");
                     pdm_document.setCadena(base64ImageString);
                     pdm_document.setRespuesta("1");
                     pdm_document.setMensaje("Correcto");
                    // Arma respuesta con el json 
                    
                }
                    catch(Exception e){System.out.print("error");}
            }
            
            else{    // Error Logico o Sintactico del Token
                     // Se rechaza el login
                    pdm_document.setIdentificador("");
                    pdm_document.setInternal_error("");
                    pdm_document.setAutor("");
                    pdm_document.setCadena("");
                    pdm_document.setFecha("");
                    pdm_document.setInternal_error(pdm_jwt.getSt_internal_error());
                    pdm_document.setPeso("");
                    pdm_document.setTelefono("");
                    pdm_document.setTipo("");
                    pdm_document.setToken("");
                    pdm_document.setRespuesta("0");
                    pdm_document.setMensaje(pdm_jwt.getSt_msgtokennok() + " Favor iniciar de nuevo");      
            }
            
             String responde = gson.toJson(pdm_document);
             return Response.ok(responde).build();
         
      }catch (Exception e) { // Falla en la resepcion de la petici�n
            // Se Registra el error.
            pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
            pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
            pdm_configurator.DriverError();
         
            return Response.status(Response.Status.BAD_REQUEST.getStatusCode()).build();
        }
     
    }
    
    @POST
    @Path("visibleDocument")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response visibleDocument(String json) throws UnsupportedEncodingException{
     PDM_Document pdm_document=null;
      try{
         
        String base64ImageString = null;
        
         pdm_configurator.pdm_configurator();
         // Se recibe apuntador de la clase manejo recurso login desde el configurador
         pdm_document   = this.pdm_configurator.getPdm_document();
          
            // Se serializa el Json que llega en la peticion hacia la clase login
            pdm_document = gson.fromJson(json, PDM_Document.class);
            
            Boolean TknValido = pdm_jwt.VerifyJWT(pdm_document.getToken(),
                                                    pdm_configurator.getKey(),
                                                      pdm_document.getTelefono(),
                                                        pdm_configurator);
             
            if (TknValido) { 
                
                 System.out.println("el Json es = " + json+" y el token valido es ="+TknValido);
            
                try{
                 ///////////////////////////////
        
                    //String pathFile="Instructivo_Ingreso_OpenKm.pdf";
                  //  String imagePath = "C:\\Users\\Capas360\\Desktop\\PDF\\Instructivo_Ingreso_OpenKm.pdf";
                   // pathFile=imagePath;
                   String pathFile=pdm_document.getIdentificador()+"."+pdm_document.getTipo();
                    String base64Image ="";
                    String imagePath = "C:\\Users\\Capas360\\Desktop\\PDF\\Instructivo_Ingreso_OpenKm.pdf";
               pathFile=imagePath;
               //proceso de validacion de campos de seguridad va en esta parte
                
		File file = new File(pathFile);
		try (FileInputStream imageInFile = new FileInputStream(file)) {//byte imageData[]
			// Reading a Image file from file system
			byte imageData[] = new byte[(int) file.length()];
                         pdm_document.setPeso(String.valueOf((file.length())/1024)+"kb");
			imageInFile.read(imageData);
			base64ImageString = Base64.getEncoder().encodeToString(imageData);
                        pdm_document.setCadena(base64ImageString);
                        pdm_document.setRespuesta("1");
                        pdm_document.setMensaje("Correcto");
                        
		} catch (FileNotFoundException e) {
			System.out.println("Image not found" + e);
		} catch (IOException ioe) {
			System.out.println("Exception while reading the Image " + ioe);
		}
                    // Arma respuesta con el jason 
                    
                }
                    catch(Exception e){System.out.print("error");}
            }
            
            else{    // Error Logico o Sintactico del Token
                     // Se rechaza el login
                    pdm_document.setInternal_error("");
                    pdm_document.setIdentificador("");
                    pdm_document.setAutor("");
                    pdm_document.setCadena("");
                    pdm_document.setFecha("");
                    pdm_document.setInternal_error(pdm_jwt.getSt_internal_error());
                    pdm_document.setPeso("");
                    pdm_document.setTelefono("");
                    pdm_document.setTipo("");
                    pdm_document.setRespuesta("1");
                    pdm_document.setToken("");
                    pdm_document.setMensaje(pdm_jwt.getSt_msgtokennok() + " Favor iniciar de nuevo");      
            }
            
              
              String responde = gson.toJson(pdm_document);
            
             return Response.ok(responde).build();
         
      }catch (Exception e) { // Falla en la resepcion de la petici�n
            // Se Registra el error.
            pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
            pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
            pdm_configurator.DriverError();
         
            return Response.status(Response.Status.BAD_REQUEST.getStatusCode()).build();
        }
     
    }

    /**
     * PUT method for updating or creating an instance of PDM_DocumentServer
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
    
     
	
}
