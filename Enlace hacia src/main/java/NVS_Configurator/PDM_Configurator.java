/*
 * Clase encargada de mantener la configuración general del servicio
 * Se instancian las clases que aplican en el servicio 
 * cualquier valor general del servicio se incluye o modifica en esta clase
 */


package NVS_Configurator;

// Clases que soportan los recursos del  Servicio de Autenticación
import NVS_ClientRestNovosit.CRN_GetInfo;
import NVS_ModalFrontEnd.Autentication.PDM_Join;
import NVS_ModalFrontEnd.Autentication.PDM_Login;
import NVS_ModalFrontEnd.Autentication.PDM_Forget;
import NVS_ModalFrontEnd.Autentication.PDM_Autentication;

// Clase que configura el cliente rest para el consumo de los servicios
// de ProDoctivity.
import NVS_ClientRestNovosit.CRN_UserProfile;
import NVS_ClientRestNovosit.CRN_UserJointNovosit;
import NVS_ClientRestNovosit.CRN_UserForgetNovosit;

// Clase de generacion y validación del token de seguridad.
import NVS_ModalFrontEnd.TokenDriver.PDM_JWT;

// Clase de manejo de errores, el cofigurador hara el manejo de los errores
// de manera que se aprovechara su apuntador para dicha tarea
import NVS_Error.PDM_Error;

import NVS_ModalFrontEnd.Info.PDM_Info;

// Clases utilitarias de consumo general en el servicio.
import io.jsonwebtoken.*;
import io.jsonwebtoken.impl.crypto.MacProvider;
import java.security.Key;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

// Clases para la Serializacion / Deserializacion 
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


/**
 *
 * @author wmendoza
 */
public class PDM_Configurator {
    Gson                    gson;
    GsonBuilder             gsonBuilder;
    
    PDM_Login               pdm_login;
    PDM_Join                pdm_join;
    PDM_Forget              pdm_forget;
    
    PDM_JWT                 pdm_jwt;
    PDM_Info                pdm_info;
    CRN_UserProfile         pdm_userprofile;
    CRN_UserForgetNovosit   pdm_userforgetnovisit;
    CRN_UserJointNovosit    pdm_userjointnovosit;
    
    CRN_GetInfo             crn_getinfo;
    
    PDM_Error               pdm_error;
    
    // Valores generales
    String                  key;
    
    String                  st_inline;
    String                  st_id;
    String                  st_subject;
    String                  st_mensajetokenok;
    String                  st_mensajetokeok;
    String                  st_time1;
    String                  st_time2;
    String                  st_error;
    String                  st_versionProDoc;
    int                     in_timeOut;
    
    // Configuracion de los URL de Back End
    String                  st_urldominio;
    String                  st_urlport;
    String                  st_url1;
    String                  st_urlversion;
    String                  st_url2;
    String                  st_urlBackEnd;
    
    // Configuracion URL del servicio info (Priero que se consume y dicta las
    //                                      pautas de los demas ervicios
    String                  st_urlinfo;
    String                  st_verInfo;
    String                  st_dnsInfo;
    String                  st_portInfo;
    
    // Credenciales
    String                  st_credencialesInfo;
    
    // On Line Back End
    boolean                 bl_onlineProd;
    
    // User and Key
    String                  st_login_user;
    String                  st_login_key;
    
    
    // Constructor con el set de los valores generales de configuración
    // del servicio, falta solo definir la forma digital de tomar estos valores.
    // Por ahora y para efecto de esta fase del proyecto se manejan asi
    // 04/03/2018
    public void pdm_configurator() {
       
         
        // Se establece la clave de encriptación 
        this.key = "secret";
        
        //The JWT signature algorithm we will be using to sign the token
        //SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS512;
        
        this.st_mensajetokeok   = "Token Valido";
        this.st_mensajetokenok  = "Token No Valido";
        this.st_id              = "Novosit";
        this.st_subject         = "PDM";
        // Se manejaran 2 valores de vencimiento para la transaccion
        // Desde la App movil se definira que lapso de tiempo se utilizara 
        this.st_time1           = "300000";
        this.st_time2           = "600000";
        
        this.setSt_urldominio("http://localhost:");
        this.setSt_urlport("8080");
        this.setSt_url1("/api");
        this.setSt_urlversion("v1");
        this.setSt_url2("/prodoctivityapi/users/profile");
        
        st_urlBackEnd = this.getSt_urldominio() +
                               this.st_urlport +
                                     this.st_url1 +
                                        this.st_urlversion + 
                                          this.st_url2;
                                            
    }
    
    public Gson getGson() {
        if (this.gson == null) {this.gson = new Gson ();}
        return gson;
    }

    public void setGson(Gson gson) {this.gson = gson;}

    public GsonBuilder getGsonBuilder() {
        if (this.gsonBuilder == null) {this.gsonBuilder = new GsonBuilder();}
        return gsonBuilder;
    }

    public void setGsonBuilder(GsonBuilder gsonBuilder) {this.gsonBuilder = gsonBuilder;}

    // Set Get Clases servicio Autenticación
    public PDM_Login getPdm_login() {
        if (this.pdm_login == null){this.pdm_login = new PDM_Login();}
        return pdm_login;
    }

    public void setPdm_login(PDM_Login pdm_login) {this.pdm_login = pdm_login;}

    public PDM_Join getPdm_join() {
        if (this.pdm_join == null){
            this.pdm_join = new PDM_Join();
        }
        return pdm_join;
    }

    public CRN_GetInfo getCrn_getinfo() {
        if (this.crn_getinfo == null) {
            crn_getinfo = new CRN_GetInfo();
        }
        return crn_getinfo;
    }

    public void setCrn_getinfo(CRN_GetInfo crn_getinfo) {
        this.crn_getinfo = crn_getinfo;
    }

    public void setPdm_join(PDM_Join pdm_join) {this.pdm_join = pdm_join;}

    public PDM_Forget getPdm_forget() {
        if (this.pdm_forget == null){
            this.pdm_forget = new PDM_Forget();
        }
        return pdm_forget;
    }

    public void setPdm_forget(PDM_Forget pdm_forget) {this.pdm_forget = pdm_forget;}

    // Se instancia los valores generales de la clase de seguridad
    public PDM_JWT getPdm_jwt() {
        if (pdm_jwt==null) {   
            this.pdm_jwt = new PDM_JWT();
        }
        return pdm_jwt;
    }

    public void setPdm_jwt(PDM_JWT pdm_jwt) {this.pdm_jwt = pdm_jwt;}

    public String getKey() {return key;}

    public void setKey(String key) {this.key = key;}

    public String getSt_mensajetokenok() {return st_mensajetokenok;}

    public void setSt_mensajetokenok(String st_mensajetokenok) {
        this.st_mensajetokenok = st_mensajetokenok;}

    public String getSt_mensajetokeon() {return st_mensajetokeok;}

    public void setSt_mensajetokeon(String st_mensajetokeok) {
        this.st_mensajetokeok = st_mensajetokeok;}

    public String getSt_mensajetokeok() {return st_mensajetokeok;}

    public void setSt_mensajetokeok(String st_mensajetokeok) {
        this.st_mensajetokeok = st_mensajetokeok;}

    public String getSt_time1() {return st_time1;}

    public void setSt_time1(String st_time1) {this.st_time1 = st_time1;}

    public String getSt_time2() {return st_time2;}
    
    public String getSt_inline() {return st_inline;}

    public void setSt_inline(String st_inline) {this.st_inline = st_inline;}

    public String getSt_id() {return st_id;}

    public void setSt_id(String st_id) {this.st_id = st_id;}

    public String getSt_subject() {return st_subject;}

    public void setSt_subject(String st_subject) {this.st_subject = st_subject;}

    public void setSt_time2(String st_time2) {this.st_time2 = st_time2;}

    public CRN_UserProfile getPdm_userprofile() {
        if (pdm_userprofile == null){
            pdm_userprofile = new CRN_UserProfile();
        }
        
        return pdm_userprofile;}

    public void setPdm_userprofile(CRN_UserProfile pdm_userprofile) {
        this.pdm_userprofile = pdm_userprofile;
    }

    public PDM_Error getPdm_error() {
        if (this.pdm_error == null){
            this.pdm_error = new PDM_Error();
        }
        return pdm_error;
    }

    public void setPdm_error(PDM_Error pdm_error) {this.pdm_error = pdm_error;}
    
    // Metodo para el manejo de errore
    public void DriverError (){}

    public CRN_UserForgetNovosit getPdm_userforgetnovisit() {
        if (pdm_userforgetnovisit == null){
            pdm_userforgetnovisit = new CRN_UserForgetNovosit();
        }
        return pdm_userforgetnovisit;
    }

    public void setPdm_userforgetnovisit(CRN_UserForgetNovosit pdm_userforgetnovisit) {
        this.pdm_userforgetnovisit = pdm_userforgetnovisit;
    }

    public CRN_UserJointNovosit getPdm_userjointnovosit() {
        if (pdm_userjointnovosit == null){
            pdm_userjointnovosit = new CRN_UserJointNovosit();
        }
        return pdm_userjointnovosit;
    }

    public void setPdm_userjointnovosit(CRN_UserJointNovosit pdm_userjointnovosit) {
        this.pdm_userjointnovosit = pdm_userjointnovosit;
    }

    public String getSt_urldominio() {
        return st_urldominio;
    }

    public void setSt_urldominio(String st_urldominio) {
        this.st_urldominio = st_urldominio;
    }

    public String getSt_url1() {
        return st_url1;
    }

    public void setSt_url1(String st_url1) {
        this.st_url1 = st_url1;
    }

    public String getSt_urlversion() {
        return st_urlversion;
    }

    public void setSt_urlversion(String st_urlversion) {
        this.st_urlversion = st_urlversion;
    }

    public String getSt_url2() {
        return st_url2;
    }

    public void setSt_url2(String st_url2) {
        this.st_url2 = st_url2;
    }

    public String getSt_urlport() {
        return st_urlport;
    }

    public void setSt_urlport(String st_urlport) {
        this.st_urlport = st_urlport;
    }

    public String getSt_urlBackEnd() {
        return st_urlBackEnd;
    }

    public void setSt_urlBackEnd(String st_urlBackEnd) {
        this.st_urlBackEnd = st_urlBackEnd;
    }
    
    public void pdm_SetLogin(String st_parametro){
        
        if (st_parametro == "login"){
            
            this.setSt_urldominio("http://localhost:");
            this.setSt_urlport("8080");
            this.setSt_url1("/api");
            this.setSt_urlversion(this.getSt_verInfo());
            this.setSt_url2("/prodoctivityapi/users/profile");

            st_urlBackEnd = this.getSt_urldominio() +
                                   this.st_urlport +
                                         this.st_url1 +
                                            this.st_urlversion + 
                                              this.st_url2;
        }
        
    }

    public String getSt_urlinfo() {
        st_urlinfo = getSt_dnsInfo();
        st_urlinfo = st_urlinfo + getSt_portInfo(); 
        st_urlinfo = st_urlinfo + "/api";
        st_urlinfo = st_urlinfo + "/" + getSt_verInfo(); // Prodoctivity Version
        st_urlinfo = st_urlinfo + "/prodoctivityapi/service/info";
        System.out.println(st_urlinfo);
        return st_urlinfo;
    }

    public void setSt_urlinfo(String st_urlinfo) {
        this.st_urlinfo = st_urlinfo;
    }

    public String getSt_verInfo() {
        st_verInfo = "1";
        return st_verInfo;
    }

    public void setSt_verInfo(String st_verInfo) {
        this.st_verInfo = st_verInfo;
    }

    public String getSt_dnsInfo() {
        st_dnsInfo = "https://localhost:";
        return st_dnsInfo;
    }

    public void setSt_dnsInfo(String st_dnsInfo) {
        this.st_dnsInfo = st_dnsInfo;
    }

    public String getSt_portInfo() {
        this.st_portInfo = "8080";
        return st_portInfo;
    }

    public void setSt_portInfo(String st_portInfo) {
        this.st_portInfo = st_portInfo;
    }

    public String getSt_credencialesInfo() {
        st_credencialesInfo = "manager@ProDoctivity Mobile:AAbb11";
        return st_credencialesInfo;
    }

    public void setSt_credencialesInfo(String st_credenciales) {
        this.st_credencialesInfo = st_credenciales;
    }

    public String getSt_error() {
        return st_error;
    }

    public void setSt_error(String st_error) {
        this.st_error = st_error;
    }

    public boolean isBl_onlineProd() {
        return bl_onlineProd;
    }

    public void setBl_onlineProd(boolean bl_onlineProd) {
        this.bl_onlineProd = bl_onlineProd;
    }

    public String getSt_versionProDoc() {
        return st_versionProDoc;
    }

    public void setSt_versionProDoc(String st_versionProDoc) {
        this.st_versionProDoc = st_versionProDoc;
    }

    public int getIn_timeOut() {
        in_timeOut = 100;
        return in_timeOut;
    }

    public void setIn_timeOut(int in_timeOut) {
        this.in_timeOut = in_timeOut;
    }

    public PDM_Info getPdm_info() {
        if (pdm_info == null){
            pdm_info = new PDM_Info();
        }
        return pdm_info;
    }

    public void setPdm_info(PDM_Info pdm_info) {
        this.pdm_info = pdm_info;
    }

    public String getSt_login_user() {
        return st_login_user;
    }

    public void setSt_login_user(String st_login_user) {
        this.st_login_user = st_login_user;
    }

    public String getSt_login_key() {
        return st_login_key;
    }

    public void setSt_login_key(String st_login_key) {
        this.st_login_key = st_login_key;
    }

    
    
    
    
    
}
