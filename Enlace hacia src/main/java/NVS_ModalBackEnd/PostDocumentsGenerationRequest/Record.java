
package NVS_ModalBackEnd.PostDocumentsGenerationRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Record {

@SerializedName("id")
@Expose
private String id;
@SerializedName("keywords")
@Expose
private Keywords_ keywords;

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public Keywords_ getKeywords() {
return keywords;
}

public void setKeywords(Keywords_ keywords) {
this.keywords = keywords;
}

}

