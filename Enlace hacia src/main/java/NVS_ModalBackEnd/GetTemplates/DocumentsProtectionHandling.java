package NVS_ModalBackEnd.GetTemplates;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DocumentsProtectionHandling {

@SerializedName("protectionType")
@Expose
private String protectionType;
@SerializedName("password")
@Expose
private String password;

public String getProtectionType() {
return protectionType;
}

public void setProtectionType(String protectionType) {
this.protectionType = protectionType;
}

public String getPassword() {
return password;
}

public void setPassword(String password) {
this.password = password;
}

}
