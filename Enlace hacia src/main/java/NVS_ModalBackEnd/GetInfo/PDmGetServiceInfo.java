
package NVS_ModalBackEnd.GetInfo;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PDmGetServiceInfo {

@SerializedName("name")
@Expose
private String name = "";
@SerializedName("url")
@Expose
private String url = "";
@SerializedName("description")
@Expose
private String description = "";
@SerializedName("version")
@Expose
private String version = "";
@SerializedName("active")
@Expose
private Boolean active = false;
@SerializedName("dependencies")
@Expose
private List<Object> dependencies = null;
@SerializedName("features")
@Expose
private List<Feature> features = null;
@SerializedName("parameters")
@Expose
private List<Parameter> parameters = null;
@SerializedName("tests")
@Expose
private List<Test> tests = null;

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public String getUrl() {
return url;
}

public void setUrl(String url) {
this.url = url;
}

public String getDescription() {
return description;
}

public void setDescription(String description) {
this.description = description;
}

public String getVersion() {
return version;
}

public void setVersion(String version) {
this.version = version;
}

public Boolean getActive() {
return active;
}

public void setActive(Boolean active) {
this.active = active;
}

public List<Object> getDependencies() {
return dependencies;
}

public void setDependencies(List<Object> dependencies) {
this.dependencies = dependencies;
}

public List<Feature> getFeatures() {
return features;
}

public void setFeatures(List<Feature> features) {
this.features = features;
}

public List<Parameter> getParameters() {
return parameters;
}

public void setParameters(List<Parameter> parameters) {
this.parameters = parameters;
}

public List<Test> getTests() {
return tests;
}

public void setTests(List<Test> tests) {
this.tests = tests;
}

}
