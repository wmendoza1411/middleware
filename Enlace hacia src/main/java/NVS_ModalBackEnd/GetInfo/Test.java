package NVS_ModalBackEnd.GetInfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Test {

@SerializedName("name")
@Expose
private String name;
@SerializedName("result")
@Expose
private Boolean result;
@SerializedName("message")
@Expose
private String message;

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public Boolean getResult() {
return result;
}

public void setResult(Boolean result) {
this.result = result;
}

public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

}

