package NVS_ModalBackEnd.PostDocument;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Record {

@SerializedName("name")
@Expose
private String name;
@SerializedName("recordHandle")
@Expose
private Integer recordHandle;
@SerializedName("keywords")
@Expose
private List<Keyword_> keywords = null;
@SerializedName("records")
@Expose
private List<Object> records = null;

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public Integer getRecordHandle() {
return recordHandle;
}

public void setRecordHandle(Integer recordHandle) {
this.recordHandle = recordHandle;
}

public List<Keyword_> getKeywords() {
return keywords;
}

public void setKeywords(List<Keyword_> keywords) {
this.keywords = keywords;
}

public List<Object> getRecords() {
return records;
}

public void setRecords(List<Object> records) {
this.records = records;
}

}