package NVS_ModalBackEnd.GetDocumentAccess;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PDmGetDocumentAccess {

@SerializedName("id")
@Expose
private Integer id;
@SerializedName("code")
@Expose
private String code;
@SerializedName("category")
@Expose
private String category;

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public String getCode() {
return code;
}

public void setCode(String code) {
this.code = code;
}

public String getCategory() {
return category;
}

public void setCategory(String category) {
this.category = category;
}

}
