
package NVS_ModalBackEnd.GetDocument;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class KeywordMap {

@SerializedName("dataType")
@Expose
private Integer dataType;
@SerializedName("handle")
@Expose
private Integer handle;
@SerializedName("name")
@Expose
private String name;

public Integer getDataType() {
return dataType;
}

public void setDataType(Integer dataType) {
this.dataType = dataType;
}

public Integer getHandle() {
return handle;
}

public void setHandle(Integer handle) {
this.handle = handle;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

}