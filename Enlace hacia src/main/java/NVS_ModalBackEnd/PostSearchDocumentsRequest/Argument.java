package NVS_ModalBackEnd.PostSearchDocumentsRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Argument {

@SerializedName("isNegative")
@Expose
private Boolean isNegative;
@SerializedName("source")
@Expose
private String source;
@SerializedName("operator")
@Expose
private String operator;
@SerializedName("target")
@Expose
private String target;
@SerializedName("targetFixedValue")
@Expose
private String targetFixedValue;
@SerializedName("summaryFunction")
@Expose
private String summaryFunction;

public Boolean getIsNegative() {
return isNegative;
}

public void setIsNegative(Boolean isNegative) {
this.isNegative = isNegative;
}

public String getSource() {
return source;
}

public void setSource(String source) {
this.source = source;
}

public String getOperator() {
return operator;
}

public void setOperator(String operator) {
this.operator = operator;
}

public String getTarget() {
return target;
}

public void setTarget(String target) {
this.target = target;
}

public String getTargetFixedValue() {
return targetFixedValue;
}

public void setTargetFixedValue(String targetFixedValue) {
this.targetFixedValue = targetFixedValue;
}

public String getSummaryFunction() {
return summaryFunction;
}

public void setSummaryFunction(String summaryFunction) {
this.summaryFunction = summaryFunction;
}

}
