

package NVS_ModalBackEnd.GetContextDefinition;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PDmGetContextDefinition {

@SerializedName("keywords")
@Expose
private List<Keyword> keywords = null;
@SerializedName("records")
@Expose
private List<Record> records = null;

public List<Keyword> getKeywords() {
return keywords;
}

public void setKeywords(List<Keyword> keywords) {
this.keywords = keywords;
}

public List<Record> getRecords() {
return records;
}

public void setRecords(List<Record> records) {
this.records = records;
}

}

