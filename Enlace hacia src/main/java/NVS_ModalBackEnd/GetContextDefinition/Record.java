
package NVS_ModalBackEnd.GetContextDefinition;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Record {

@SerializedName("name")
@Expose
private String name;
@SerializedName("label")
@Expose
private String label;
@SerializedName("humanName")
@Expose
private String humanName;
@SerializedName("dataType")
@Expose
private String dataType;
@SerializedName("inputType")
@Expose
private String inputType;
@SerializedName("keywords")
@Expose
private List<Keyword_> keywords = null;

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public String getLabel() {
return label;
}

public void setLabel(String label) {
this.label = label;
}

public String getHumanName() {
return humanName;
}

public void setHumanName(String humanName) {
this.humanName = humanName;
}

public String getDataType() {
return dataType;
}

public void setDataType(String dataType) {
this.dataType = dataType;
}

public String getInputType() {
return inputType;
}

public void setInputType(String inputType) {
this.inputType = inputType;
}

public List<Keyword_> getKeywords() {
return keywords;
}

public void setKeywords(List<Keyword_> keywords) {
this.keywords = keywords;
}

}