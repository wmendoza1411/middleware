/*
*
* Clase que materializa al json del recurso Forget.....
* la misma mantendra los campos que objeto 
*/
package NVS_ModalFrontEnd.Autentication;

/**
 *
 * @author wmendoza
 */
public class PDM_Forget {
    
    private String usuario          = "";
    private String telefono         = "";
    private String token            = "";
    private String respuesta        = "";
    private String mensaje          = "";
    private String perfil           = "";
    private String email            = "";
    private String internal_error   = "";
    private boolean bl_statusbackend;
    private String idd              = "";

   
    
    public PDM_Forget(){}


    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    public String getInternal_error() {
        return internal_error;
    }

    public void setInternal_error(String internal_error) {
        this.internal_error = internal_error;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isBl_statusbackend() {
        return bl_statusbackend;
    }

    public void setBl_statusbackend(boolean bl_statusbackend) {
        this.bl_statusbackend = bl_statusbackend;
    }

    public String getIdd() {
        return idd;
    }

    public void setIdd(String idd) {
        this.idd = idd;
    }
    
    
    
    
}
