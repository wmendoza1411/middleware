/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ModalFrontEnd.Info;

/**
 *
 * @author wmendoza
 */
public class PDM_Info {
    
    String      st_version = "";
    boolean     bl_statusbackend = false;

    public String getSt_version() {
        return st_version;
    }

    public void setSt_version(String st_version) {
        this.st_version = st_version;
    }

    public boolean isBl_statusbackend() {
        return bl_statusbackend;
    }

    public void setBl_statusbackend(boolean bl_statusbackend) {
        this.bl_statusbackend = bl_statusbackend;
    }

    
    
    
}
