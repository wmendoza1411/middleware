/*
 * Clase para la creacion y validacion del token JWT
 * Para los efectos la solicitud del token es con el nro. de telefono
 * Usurio y clave del usuario involucrado.
 * la clase devuelve el nuevo token, o una respuesta afirmativa si el token
 * pasa la verificación 03/10/2018
 
 */
package NVS_ModalFrontEnd.TokenDriver;

// 
import java.security.Key;
import io.jsonwebtoken.*;
import io.jsonwebtoken.impl.crypto.MacProvider;
import java.util.Date;

// 
import NVS_Configurator.PDM_Configurator;
import java.io.UnsupportedEncodingException;

/**
 *
 * @author Novatec
 */
public class PDM_JWT {
    
    String  st_token;           
    String  st_respond;
    String  st_internal_error;
    String  st_msgtokenok;
    String  st_msgtokennok;
    boolean bl_statusbackend;
    String  st_idd;
    
    String  key;
    
    
   
    // Constructor
    public void PDM_JWT() {}

    
    // Creacion de Token inicio de conversación y por cada peticion para proteger los tiempos 
    // de respuesta y darle tiempo al usuario en sus ejecuciones en el App Movil
    // La crecion del token involucra el usuario y la clave por que cada operacion
    // que se vaya a realizar contra el Back End requiere usuario y clave.
    public boolean CreateJWT(PDM_Configurator pdm_configurator_p,
                                String par_llamada,
                                        String par_user,
                                            String par_key) {
        
        // Genera un nuevo token con el Inicio de una conversacion Movil to Back End
        // se utilizaran  los valores generales del servicio que provee el configurador esto es:
        // Clave de encriptamiento general
        // Lapso de tiempo de vencimiento general
        // Configurador y Subject generales.
        // Nro. de telefono donde esta la app movil operando.
        
        PDM_Configurator pdm_configurator = pdm_configurator_p;
        try {
           
            long nowMillis = System.currentTimeMillis();
            // Se indican los lapsos de tiempo de vencimiento de la transacción.
            Date now = new Date(nowMillis);
            Date exp = new Date(nowMillis + Integer.parseInt(pdm_configurator.getSt_time1()));

            // Se genera el Token
            String compactJws = Jwts.builder()
                                    .setId(pdm_configurator.getSt_id())
                                    .setSubject(pdm_configurator.getSt_subject())
                                    .setIssuedAt(now)
                                    .setExpiration(exp)
                                    .claim("llamada",par_llamada)
                                    .claim("user",par_user)
                                    .claim("key",par_key)
                                    .signWith(SignatureAlgorithm.HS256, 
                                            pdm_configurator.getKey().getBytes("UTF-8"))
                                    .compact();
            
            // Almacena el token generado y demas valores en propia clase con los metodos "set"
            setSt_token(compactJws);
            setSt_respond("1");
            setSt_internal_error("");
            this.setSt_msgtokenok(pdm_configurator.getSt_mensajetokeok());
            this.setSt_msgtokennok("");
            //
            // Token Generado.
            return true;
        }
   
         catch (Exception e) {  // Error en la generacion del token
                                // Se le avisa al usuario 
                                
                        
            setSt_respond("11");
            setSt_internal_error(e.getMessage());
            setSt_token("");
            setSt_msgtokennok(pdm_configurator.getSt_mensajetokenok());
            setSt_msgtokenok("");
           
            
            // Acciones para el registro de errores.
            pdm_configurator.getPdm_error().setSt_error(e.getMessage());
            pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
            pdm_configurator.DriverError();
            
            return false;
        }
    }
    
    // Metodo de Verificación del Token
    // por cada peticion que llega desde la App Movil, se verifica el token
    // y se comprueba su validez y vigencia.
    public boolean VerifyJWT(String compactJwt, 
                                        PDM_Configurator pdm_configurator_p) 
                                                throws UnsupportedEncodingException {
        // Configurador pasado como parametro
        PDM_Configurator pdm_configurator = pdm_configurator_p;
        key = pdm_configurator.getKey();
        
        try {
            Boolean response;
            
            // Valida sintacticamente el token que viene como parametro
            if (compactJwt == null || compactJwt.isEmpty()){
                setSt_respond(pdm_configurator.getSt_mensajetokenok());
                setSt_internal_error("Token Sintaxis Error");
                
                return false;
            }
            
            // Se desencripta el Token
            Claims claims = Jwts.parser()         
                .setSigningKey(key.getBytes("UTF-8")) // Clave que viene como parametro
                    .parseClaimsJws(compactJwt) // Token Parametro
                        .getBody();
            
            // Se hacen las validaciones lógicas correpondientes
           
            String st_compare = (String)claims.getId();
            if (!(st_compare.equals(pdm_configurator.getSt_id()))){      // ID
                    this.setSt_respond(pdm_configurator.getSt_mensajetokenok() + " ID != ");
                    this.setSt_internal_error("Token Sintaxis Error");
                    return false;}
             
            
            st_compare = (String)claims.getSubject();
            if (!(st_compare.equals(pdm_configurator.getSt_subject()))){        // SUBJECT
                    this.setSt_respond(pdm_configurator.getSt_mensajetokenok() + " SUBJECT != ");
                    this.setSt_internal_error("Token Sintaxis Error");
                    return false;}
            
            this.setSt_respond("1");
           
            return true; 
        } catch (SignatureException e) {
            // Se captura y controla la invalides del token
            this.setSt_respond("11");
            this.setSt_respond("Error Interno del Token");
            this.setSt_internal_error(e.hashCode() + "  " + e.getMessage());
            
            // Acciones para el registro de errores.
            pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
            pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
            pdm_configurator.DriverError();
            
            return false;
        }
        catch (Exception e) {
            
            // Se captura y controla la invalides del token
            
            this.setSt_respond("Error Interno del Token");
            this.setSt_internal_error(e.hashCode() + "  " + e.getMessage());
            
            // Acciones para el registro de errores.
            pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
            pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
            pdm_configurator.DriverError();
            
            this.setSt_respond("11");
            
            return false;
            
        }
        
    }
    
    
    // Set y Get para a generacion del Json con el Token
    public String getSt_token() {return st_token;}

    public void setSt_token(String st_token) {this.st_token = st_token;}

    public String getSt_respond() {return st_respond;}

    public void setSt_respond(String st_respond) {this.st_respond = st_respond;}

    public String getSt_internal_error() {return st_internal_error;}

    public void setSt_internal_error(String st_internal_error) {
        this.st_internal_error = st_internal_error;}

    public String getSt_msgtokenok() {return st_msgtokenok;}

    public void setSt_msgtokenok(String st_msgtokenok) {this.st_msgtokenok = st_msgtokenok;}

    public String getSt_msgtokennok() {return st_msgtokennok;}

    public void setSt_msgtokennok(String st_msgtokennok) {
        this.st_msgtokennok = st_msgtokennok;}

    public boolean isBl_statusbackend() {
        return bl_statusbackend;
    }

    public void setBl_statusbackend(boolean bl_statusbackend) {
        this.bl_statusbackend = bl_statusbackend;
    }

    public String getSt_idd() {
        return st_idd;
    }

    public void setSt_idd(String st_idd) {
        this.st_idd = st_idd;
    }
    
    
    
    
    
}
