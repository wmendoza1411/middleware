/*
 */
package NVS_ModalFrontEnd.Document;

import java.util.Objects;


import java.util.Objects;

/**
 *
 * @author Novatec wmendoza
 */
public class PDM_Document {
    
    String identificador;
    String tipo;
    String cadena;
    String peso;
    String fecha;
    String autor;
    String telefono;
    String mensaje;
    String respuesta;

    public String getRespuesta() {
        return respuesta;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
    private String token= "";
    private String internal_error= "";
    private boolean bl_statusbackend;
    

    public PDM_Document() {
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String Tipo) {
        this.tipo = Tipo;
    }

    public String getCadena() {
        return cadena;
    }

    public void setCadena(String cadena) {
        this.cadena = cadena;
    }

    public String getPeso() {
        return peso;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }
    
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String Telefono) {
        this.telefono = Telefono;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getInternal_error() {
        return internal_error;
    }

    public void setInternal_error(String internal_error) {
        this.internal_error = internal_error;
    }

    public boolean isBl_statusbackend() {
        return bl_statusbackend;
    }

    public void setBl_statusbackend(boolean bl_statusbackend) {
        this.bl_statusbackend = bl_statusbackend;
    }

    
    
}

