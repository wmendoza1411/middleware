/*
* Clase para el manejo de los errores
* Esta clase debe desarrollarse, por ahora solamente acetara los valores de los 
* diferentes Catch que produscan errores.
*/
package NVS_Error;

/**
 *
 * @author wmendoza
 */
public class PDM_Error {
    
    String st_error         = "";
    String st_localizacion  = "";
    String st_servicio      = "";
    String st_fecha         = "";
    String st_hora          = "";
    String st_telefono      = "";

    public PDM_Error() {
    }

    public String getSt_error() {
        return st_error;
    }

    public void setSt_error(String st_error) {
        this.st_error = st_error;
    }

    public String getSt_localizacion() {
        return st_localizacion;
    }

    public void setSt_localizacion(String st_localizacion) {
        this.st_localizacion = st_localizacion;
    }

    public String getSt_servicio() {
        return st_servicio;
    }

    public void setSt_servicio(String st_servicio) {
        this.st_servicio = st_servicio;
    }

    public String getSt_fecha() {
        return st_fecha;
    }

    public void setSt_fecha(String st_fecha) {
        this.st_fecha = st_fecha;
    }

    public String getSt_hora() {
        return st_hora;
    }

    public void setSt_hora(String st_hora) {
        this.st_hora = st_hora;
    }

    public String getSt_telefono() {
        return st_telefono;
    }

    public void setSt_telefono(String st_telefono) {
        this.st_telefono = st_telefono;
    }
    
    // Metodo para la captura de errores.
    public void GetError () {
        
    }
            
    
}

