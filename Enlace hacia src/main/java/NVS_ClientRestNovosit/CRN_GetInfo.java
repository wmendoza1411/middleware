/*
 * -This class has the mediation funtion betwen the app movril request and the ProDoctivity Services (Rest)  (Back End).
 * -Instance the modal class "GetServiceDescription" and 
 *  make the Serialization for the Json component front ProDoctivity.
 * -Receive as parameter the pointer  for the clase Configurator whit the general values  for  the 
 * Middleware.
 */
package NVS_ClientRestNovosit;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import sun.misc.BASE64Encoder;
//
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
//
// Serializacion / DesSerializacion Json Component
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

// Instance the just Back End modal clase
import NVS_ModalBackEnd.GetInfo.PDmGetServiceInfo;

// Instance the just Front End modal class
import NVS_ModalFrontEnd.Info.PDM_Info;

// Configurator class whit all parameter and condition for the middleware  services
import NVS_Configurator.PDM_Configurator;

        

/**
 *
 * @author Novatec wmendoza
 */
public class CRN_GetInfo {
    
    // Clase para el manejo del servicio de ServiceDescription
    // expuesto por Prodoctivity
    // Por ser un servicio que no es solicitado explicitamente desde la 
    // aplicacion movil los datos que provengan del Back End
    // se matendran en la clase modal
    
    PDM_Configurator pdm_configurator;
    Gson gson;
    GsonBuilder gsonbuilder;
    PDmGetServiceInfo  pdmGetServiceInfo;
    PDM_Info    pdm_info;
     
    // Constructor Class
    public void PDM_GetServiceDescription(){     }
    
    public boolean getServiceRS(PDM_Configurator pdm_configurator){
        
        this.pdm_configurator       = pdm_configurator;
        // this.gson                   = this.pdm_configurator.getGson();
        this.pdm_info               = this.pdm_configurator.getPdm_info();
        
        pdmGetServiceInfo           = new PDmGetServiceInfo();
        
        //Client cliente = ClientBuilder.newClient();
        //pdmGetServiceInfo = cliente.target("http://localhost:3000/mifactura")
        //.request(MediaType.APPLICATION_JSON_TYPE).get(PDmGetServiceInfo.class);
        
        this.pdm_info.setSt_version("V1.10");
        this.pdm_info.setBl_statusbackend(true);
        
        return true;
    }
    
    public boolean getService(PDM_Configurator pdm_configurator){
        // This code shoult be change
        // The correct code should cosume the prodoctivity services
        try {

            // se recibe instancia del configurador
            // se recibe instancia para serializar/desserializar
            this.pdm_configurator       = pdm_configurator;
            this.gson                   = this.pdm_configurator.getGson();
            pdmGetServiceInfo           = new PDmGetServiceInfo();

            // Se optiene el url de este servicio que es el primero a consumir
            // en el servicio.
            String st_url = this.pdm_configurator.getSt_urlinfo();
            System.out.println("URL     " + st_url);
            URL url = new URL(st_url);
           
            BASE64Encoder enc           = new sun.misc.BASE64Encoder();

            String userCredentials      = this.pdm_configurator.getSt_credencialesInfo();
            String encodedAuthorization = enc.encode( userCredentials.getBytes() );

            String basicAuth            = "Basic " + encodedAuthorization;

            HttpURLConnection conn      = (HttpURLConnection) url.openConnection();

            // it's indicate the timeout value
            conn.setConnectTimeout(this.pdm_configurator.getIn_timeOut());
            
            conn.setRequestMethod("GET");
            conn.setRequestProperty ("Authorization", basicAuth);
            conn.setRequestProperty("X-API-KEY", "1234237");
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            conn.setRequestProperty("Accept", "application/json");
            
            conn.setUseCaches(false);
            conn.setDoInput(true);
            conn.setDoOutput(true);

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode() + ". " +conn.getResponseMessage());
            }

           
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
            while ((output = br.readLine()) != null) {
                System.out.println(output);
            }
            conn.disconnect();

            // is deserialized the json to modal class "pdmGetServiceInfo"
            pdmGetServiceInfo = gson.fromJson(output,PDmGetServiceInfo.class);
            // is obtained the prodoctivity version Client
            this.pdm_configurator.setSt_versionProDoc(pdmGetServiceInfo.getVersion());
            // The Prodoctivity is On Line  
            this.pdm_configurator.setBl_onlineProd(true);
         
            
            return true;

        } catch (Exception e) {
             // Se Registra el error.
            pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
            pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
            pdm_configurator.DriverError();
            this.pdmGetServiceInfo.setActive(false);
            this.pdm_configurator.setBl_onlineProd(false);  // prodoctivity is Off Line
            return false;
        }

    }

    // Analiza el Json de Back End y Genera Jason de Respuesta para el
    // Front End
    // Como hoy (03/16/2018) no se tiene claro la logica de operacion contra
    // los servicios del Back End esta clase este metodo estara vacio.
    // Hay que aclarar si con este servicio
    // '/api/v{version}/prodoctivityapi/service/info': se debe utilizar con
    // la interaccion del Front End
    private void ProdService (){

    }

    public PDmGetServiceInfo getPdmGetServiceInfo() {
        return pdmGetServiceInfo;
    }

    public void setPdmGetServiceInfo(PDmGetServiceInfo pdmGetServiceInfo) {
        this.pdmGetServiceInfo = pdmGetServiceInfo;
    }
    
    
    
}
