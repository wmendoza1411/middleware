/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ClientRestNovosit;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import sun.misc.BASE64Encoder;

import NVS_ModalFrontEnd.Autentication.PDM_Join;

/**
 *
 * @author cesar3r2
 */
public class CRN_UserJointNovosit {
    
    String st_mensaje = "";
    String st_internal_error = "";
    String st_user  = "";
    String st_key   = "";
    String st_dns   = "";
    
    PDM_Join pdm_join;
    
    
    public boolean getUserProfile (int it_par,PDM_Join pdm_join){
        this.setPdm_join(pdm_join);
        
        this.pdm_join.setUsuario("");
        this.pdm_join.setClave("");
        this.pdm_join.setPerfil("Datos enviados por el Join de ProDoctivity");
        this.pdm_join.setInternal_error("");
        this.pdm_join.setRespuesta("1");
        this.pdm_join.setMensaje(" Joint OK ");
        this.pdm_join.setBl_statusbackend(true);
        
        return true;
    }
    
    public boolean getUserProfile() {
        try {
            URL url = new URL("http://localhost:8389/api/v1/prodoctivityapi/users/profile");
            BASE64Encoder enc = new sun.misc.BASE64Encoder();
            
            String userCredentials = "manager@ProDoctivity Mobile:AAbb11";
            String encodedAuthorization = enc.encode( userCredentials.getBytes() );
            
            String basicAuth = "Basic "+ encodedAuthorization;
            
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty ("Authorization", basicAuth);
            conn.setRequestProperty("X-API-KEY", "1234237");
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            conn.setRequestProperty("Accept", "application/json");
            conn.setUseCaches(false);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            
            System.out.println("\nSending 'GET' request to URL: " + url);

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode() + ". " +conn.getResponseMessage());
            }
            System.out.println("Response Code: " + conn.getResponseCode() + ". " +conn.getResponseMessage());
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
            while ((output = br.readLine()) != null) {
                System.out.println(output);
            }
            conn.disconnect();
            return true;

        } catch (Exception e) {
            System.out.println("Exception in getUserProfile:- " + e);
            return false;
        }
    }

    public String getSt_mensaje() {
        return st_mensaje;
    }

    public void setSt_mensaje(String st_mensaje) {
        this.st_mensaje = st_mensaje;
    }

    public String getSt_internal_error() {
        return st_internal_error;
    }

    public void setSt_internal_error(String st_internal_error) {
        this.st_internal_error = st_internal_error;
    }

    public String getSt_user() {
        return st_user;
    }

    public void setSt_user(String st_user) {
        this.st_user = st_user;
    }

    public String getSt_key() {
        return st_key;
    }

    public void setSt_key(String st_key) {
        this.st_key = st_key;
    }

    public PDM_Join getPdm_join() {
        return pdm_join;
    }

    public void setPdm_join(PDM_Join pdm_join) {
        this.pdm_join = pdm_join;
    }

    public String getSt_dns() {
        return st_dns;
    }

    public void setSt_dns(String st_dns) {
        this.st_dns = st_dns;
    }
    
}
