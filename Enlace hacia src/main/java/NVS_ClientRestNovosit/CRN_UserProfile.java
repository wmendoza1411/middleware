/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NVS_ClientRestNovosit;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import sun.misc.BASE64Encoder;

import NVS_ModalFrontEnd.Autentication.PDM_Login;
import NVS_ModalBackEnd.Profile.PDmGetUserProfile;

// Serializacion / DesSerializacion Json Component
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

// Configurator class whit all parameter and condition for the middleware  services
import NVS_Configurator.PDM_Configurator;
import NVS_ModalBackEnd.GetInfo.PDmGetServiceInfo;

/**
 *
 * @author cesar3r2
 */
public class CRN_UserProfile {
    
    
    PDM_Configurator pdm_configurator;
    Gson gson;
    GsonBuilder gsonbuilder;
    PDmGetUserProfile pdmgetUseprofile;
    
    String st_mensaje           = "";
    String st_internal_error    = "";
    String st_user              = "";
    String st_key               = "";
    String st_dns               = "";
    String output               = "";
    
    PDM_Login pdm_login;
    
    public CRN_UserProfile() {
        
    }
    
    public boolean getUserProfile (int par_i, PDM_Login pdm_login){
        
        // Recibe apuntador de la clase modal que viene activada
        this.setPdm_login(pdm_login);
       
        this.pdm_login.setRespuesta("1");
        this.pdm_login.setMensaje(" Login Aceptado ");
        this.pdm_login.setPerfil(" Usuario Operario ");
        
        return true;
    }
    
    // Consume Back End Profile Back End Services 
    public boolean getUserProfile(PDM_Configurator 
                                        pdm_configurator) {
        try {
            
            this.pdm_configurator   = pdm_configurator;
            
            this.pdm_login          = this.pdm_configurator.getPdm_login();
            
            this.gson               = this.pdm_configurator.getGson();
            
            URL url                 = new URL(this.pdm_configurator.getSt_urlBackEnd());
            
            BASE64Encoder enc       = new sun.misc.BASE64Encoder();
            
            String userCredentials = "manager@ProDoctivity Mobile:AAbb11";
            String encodedAuthorization = enc.encode( userCredentials.getBytes() );
            
            String basicAuth = "Basic "+ encodedAuthorization;
            
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty ("Authorization", basicAuth);
            conn.setRequestProperty("X-API-KEY", "1234237");
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            conn.setRequestProperty("Accept", "application/json");
            conn.setUseCaches(false);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            
            System.out.println("\nSending 'GET' request to URL: " + url);

            if (conn.getResponseCode() != 200) {
                 pdm_login.setToken(""); 
                 pdm_login.setRespuesta("0");
                 pdm_login.setPerfil(""); 
                 pdm_login.setMensaje(conn.getResponseMessage());
                 pdm_login.setClave("");     
                 pdm_login.setUsuario("");
                 pdm_login.setInternal_error("");
                 pdm_login.setBl_statusbackend(false);
                
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode() + ". " +conn.getResponseMessage());
            }
            
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            
            while ((output = br.readLine()) != null) {
                System.out.println(output);
            }
            
            conn.disconnect();
            
            // Process login y media json Back End Json Front
            this.ProcLogin(output);
            return true;

        } catch (Exception e) {
            
             // Se Registra el error.
            pdm_configurator.getPdm_error().setSt_error(e.hashCode() + "  " + e.getMessage());
            pdm_configurator.getPdm_error().setSt_localizacion(this.getClass().getSimpleName());
            pdm_configurator.DriverError();
            pdm_login.setInternal_error(e.getMessage());
            return false;
        }
    }
    
    // Procesa el login que llego del back end y lo transforma al 
    // login modal requerido por el front end
    private boolean ProcLogin(String output){
        
        // Des Serializa hacia la clase modal el json  que llega en la peticion 
        // desde el Back End
        this.pdmgetUseprofile = gson.fromJson(output,
                                            PDmGetUserProfile.class);
        
        String st_nomber = this.pdmgetUseprofile.getName();
        
        pdm_login.setRespuesta("1");
        pdm_login.setPerfil("Datos enviados por login de ProDoctivity");  // Est eperfil lo entrga Back End
        pdm_login.setMensaje("Login Aceptado");
        pdm_login.setInternal_error("");
        pdm_login.setBl_statusbackend(true);
        
        return true;
        
    }
    

    public String getSt_mensaje() {
        return st_mensaje;
    }

    public void setSt_mensaje(String st_mensaje) {
        this.st_mensaje = st_mensaje;
    }

    public String getSt_internal_error() {
        return st_internal_error;
    }

    public void setSt_internal_error(String st_internal_error) {
        this.st_internal_error = st_internal_error;
    }

    public String getSt_user() {
        return st_user;
    }

    public void setSt_user(String st_user) {
        this.st_user = st_user;
    }

    public String getSt_key() {
        return st_key;
    }

    public void setSt_key(String st_key) {
        this.st_key = st_key;
    }

    public PDM_Login getPdm_login() {
        return pdm_login;
    }

    public void setPdm_login(PDM_Login pdm_login) {
        this.pdm_login = pdm_login;
    }

    public String getSt_dns() {
        return st_dns;
    }

    public void setSt_dns(String st_dns) {
        this.st_dns = st_dns;
    }
    
}
